export default {
  state: {
    inputFiles: new Map(),
  },
  mutations: {
    addInputFile(state, { name, valueWrapper }) {
      state.inputFiles.set(name, valueWrapper);
    },
  },
  actions: {
    addInputFile({ commit }, { name, valueWrapper }) {
      commit('addInputFile', { name, valueWrapper });
    },
  },
  getters: {
    getInputValues: (state, name) => state.inputFiles.get(name),
    getAllInputFiles: (state) => state.inputFiles,
  },
};
