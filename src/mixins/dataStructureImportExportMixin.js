import ValueType from '../utils/ValueType';

export default {
  created() {
    this.$store.commit('setValueType', ValueType.INTEGERS);
  },
  methods: {
    pushValueToStore(value) {
      this.$store.commit('addValue', value);
    },
    removeValueFromStore(value) {
      this.$store.commit('removeValue', value);
    },
    setK(value) {
      this.$store.commit('setK', value);
    },
    setIndex(value) {
      this.$store.commit('setIndex', value);
    },
    setBucket(value) {
      this.$store.commit('setBucket', value);
    },
    setEntry(value) {
      this.$store.commit('setEntry', value);
    },
    setCellar(value){
      this.$store.commit('setCellar',value);
    },
    setType(value) {
      this.$store.commit('setValueType', value);
    },
    clearState() {
      this.$store.commit('clearCurrentValues');
    },
  },
};
