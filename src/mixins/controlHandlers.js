import {cloneDeep} from "lodash";
import EventBus from "../services/EventBus";
import StepGroup from "../utils/StepGroup";
import StepGroupType from "../utils/StepGroupType";
import HashStructure from "../algorithms/hashes/HashStructure";
import TreeUtils from "../utils/TreeUtils";
import Heap from "../algorithms/trekhleb/src/data-structures/heap/Heap";
import TreeStructure from "../algorithms/trees/TreeStructure";

const Direction = {
  FORWARD: 1,
  BACK: -1
};

export default {
  data: () => ({
    stepGroups: [],
    currentStepGroup: -1,
    nextStep: 0,
    componentKey: 0 // incremented by dataStructure watcher to trigger component reload
  }),
  watch: {
    dataStructure() {
      this.componentKey += 1;
    }
  },
  computed: {
    animationSpeed() {
      return this.$store.getters.animationSpeed;
    }
  },
  methods: {
    registerListeners() {
      EventBus.$on("controlRandom", this.random);
      EventBus.$on("controlReset", this.reset);
      EventBus.$on("controlGoToStart", this.goToStart);
      EventBus.$on("controlStepBack", this.stepBack);
      EventBus.$on("controlPause", this.pause);
      EventBus.$on("controlPlay", this.play);
      EventBus.$on("controlStepForward", this.stepForward);
      EventBus.$on("controlGoToEnd", this.goToEnd);
    },
    unregisterListeners() {
      EventBus.$off("controlRandom", this.random);
      EventBus.$off("controlReset", this.reset);
      EventBus.$off("controlGoToStart", this.goToStart);
      EventBus.$off("controlStepBack", this.stepBack);
      EventBus.$off("controlPause", this.pause);
      EventBus.$off("controlPlay", this.play);
      EventBus.$off("controlStepForward", this.stepForward);
      EventBus.$off("controlGoToEnd", this.goToEnd);
      this.clearAnimation();
    },
    isAnimationFinished() {
      const atStart = this.currentStepGroup === 0 && this.isAtStepGroupStart();
      const atEnd = this.currentStepGroup === this.stepGroups.length - 1 && this.isAtStepGroupEnd();
      return atStart || atEnd;
    },
    isStepGroupMutation(stepGroup) {
      switch (stepGroup.type) {
        case StepGroupType.ALGORITHM:
          return false;
        case StepGroupType.CREATE:
        case StepGroupType.INSERT:
        case StepGroupType.REMOVE:
          return true;
        default:
          throw new Error(`Unknown StepGroupType: ${stepGroup.type}`);
      }
    },
    isAtStepGroupStart() {
      return this.nextStep === 0;
    },
    isAtStepGroupStartPoint() {
      return this.nextStep === -1;
    },
    isAtStepGroupEnd() {
      return this.nextStep === this.stepGroups[this.currentStepGroup].steps.length;
    },
    isStepGroupFinished(direction) {
      let finishedAtStart;
      if (
        direction === Direction.BACK &&
        this.stepGroups[this.currentStepGroup].type === StepGroupType.ALGORITHM
      ) {
        finishedAtStart = this.isAtStepGroupStartPoint();
      } else {
        finishedAtStart = direction === Direction.BACK && this.isAtStepGroupStartPoint();
      }
      const finishedAtEnd = direction === Direction.FORWARD && this.isAtStepGroupEnd();

      return finishedAtStart || finishedAtEnd;
    },
    random() {
      // todo: implement
    },
    reset() {
      clearInterval(this.$store.getters.animationIntervalId); // Clear animation interval if it exists
      this.stepGroups = []; // Reset step groups to an empty array
      this.currentStepGroup = -1; // Reset the current step group index
      this.nextStep = 0; // Reset the next step index
      this.componentKey += 1; // Increment componentKey to trigger component reload (if necessary)
      EventBus.$emit("animationStopped"); // Emit an event indicating that the animation has stopped
    },
    goToStart() {
      clearInterval(this.$store.getters.animationIntervalId);
      const intervalId = setInterval(this.stepBack, 10);
      this.$store.commit("setAnimationIntervalId", intervalId);
    },
    clearAnimation() {
      clearInterval(this.$store.getters.animationIntervalId);
      EventBus.$emit("animationFinished");
      EventBus.$emit("animationStopped");
      EventBus.$emit("animationProgress", 0);
      this.stepGroups = [];
      this.currentStepGroup = -1;
      this.nextStep = 0;
    },
    updateStorage(stepGroup, forward = true) {
      switch (stepGroup.type) {
        case StepGroupType.INSERT:
          forward
            ? this.pushValueToStore(stepGroup.value)
            : this.removeValueFromStore(stepGroup.value);
          return;
        case StepGroupType.REMOVE:
          forward
            ? this.removeValueFromStore(stepGroup.value)
            : this.pushValueToStore(stepGroup.value);

        default:
      }
    },
    setAnimationProgress() {
      EventBus.$emit(
        "animationProgress",
        (this.getCurrentPositionInAnimation() * 100) / this.getTotalLength()
      );
    },
    goBackInStepGroups() {
      if (!(this.currentStepGroup === 0)) {
        this.currentStepGroup -= 1;
        this.nextStep = this.stepGroups[this.currentStepGroup].steps.length;
      } else {
        this.nextStep = -1;
      }
    },
    stepBack() {
      // at first step of first step group, cannot go back further, stop
      if ((this.isAnimationFinished() && this.isAtStepGroupStart()) || this.nextStep === -1) {
        if (
          this.isAtStepGroupStart() &&
          this.stepGroups[this.currentStepGroup].type === StepGroupType.ALGORITHM &&
          this.nextStep !== -1
        ) {
          this.step(Direction.BACK);
        }
        // We need to go back in step group after algorithm step
        if (
          (this.dataStructure instanceof HashStructure ||
            this.dataStructure instanceof TreeStructure) &&
          this.currentStepGroup !== 0
        ) {
          this.currentStepGroup -= 1;
          this.nextStep = this.stepGroups[this.currentStepGroup].steps.length - 1;
        } else {
          clearInterval(this.$store.getters.animationIntervalId);
          EventBus.$emit("animationFinished");
          EventBus.$emit("animationStopped");
          return;
        }
      }

      if (
        this.isStepGroupMutation(this.stepGroups[this.currentStepGroup]) &&
        !this.isStepGroupFinished(Direction.FORWARD) &&
        !this.stepGroups[this.currentStepGroup]
      ) {
        // step group is MUTATION and NOT FINISHED
        // -> no step back allowed because mutations must go forward and not be interrupted
        return;
      }

      // now we know that IF we have a mutation,
      // we are at the beginning or end, so we can safely undo.
      if (this.isStepGroupMutation(this.stepGroups[this.currentStepGroup])) {
        if (this.dataStructure instanceof HashStructure) {
          this.updateStorage(this.stepGroups[this.currentStepGroup], false);
          this.setDataStructure(this.stepGroups[this.currentStepGroup].oldDataStructure);
          while (this.nextStep >= 0) {
            this.step(Direction.BACK);
          }
          this.goBackInStepGroups();
          this.setAnimationProgress();
          return;
        }
        if (this.dataStructure instanceof TreeStructure) {
          this.setDataStructure(this.stepGroups[this.currentStepGroup].oldDataStructure);
          this.updateStorage(this.stepGroups[this.currentStepGroup], false);
          const step = {
            action: TreeUtils.UPDATE_TREE,
            elements:
              this.dataStructure instanceof Heap
                ? this.dataStructure.buildTree()
                : this.dataStructure.root,
            dataStructure: this.stepGroups[this.currentStepGroup].oldDataStructure
          };
          this.goBackInStepGroups();
          EventBus.$emit("animationStep", step);
          this.setAnimationProgress();
          return;
        }
        // is MUTATION -> undo entire mutation in one step

        // replace entire data structure with the one given in the undo step

        do {
          if (this.currentStepGroup === 0) {
            break;
          }
          // skip non-mutation step groups for data structures (search, traverse)
          this.currentStepGroup -= 1;
        } while (!this.isStepGroupMutation(this.stepGroups[this.currentStepGroup]));

        // found a non-mutation step group, set next step and dataStructure accordingly
        this.nextStep = this.stepGroups[this.currentStepGroup].steps.length;
        this.setDataStructure(this.stepGroups[this.currentStepGroup].dataStructure);
        return;
      }

      // now we know that we have a NON-mutation step group
      // algo: sort, graph / ds: search, traverse
      // -> go back freely step-by-step

      if (
        this.dataStructure instanceof TreeStructure ||
        this.dataStructure instanceof HashStructure
      ) {
        while (this.nextStep >= 0) {
          this.step(Direction.BACK);
        }
        return;
      }
      this.step(Direction.BACK);
    },
    pause() {
      // mutations should not be interrupted (create, insert, delete)
      if (this.isStepGroupMutation(this.stepGroups[this.currentStepGroup]) === false) {
        clearInterval(this.$store.getters.animationIntervalId);
        EventBus.$emit("animationStopped");
      }
    },
    play(input = null) {
      if (this.currentStepGroup === -1 || this.isAnimationFinished()) {
        // we are in a clean state at the beginning or end of a step sequence of a mutation
        // we can start a new animation
        if (input instanceof StepGroup) {
          // DS mutation (create, insert, remove)
          this.stepGroups.push(input);
        } else if (Array.isArray(input) && input.length > 0 && input[0] instanceof StepGroup) {
          this.stepGroups.push(...input);
        } else if (Array.isArray(input)) {
          // DS algorithm (traverse or search)
          const vsm = new StepGroup(StepGroupType.ALGORITHM);
          vsm.setSteps(input);
          this.stepGroups.push(vsm);
        } else if (input === null) {
          // sort or graph algorithm - try to this.getSteps()

          try {
            const steps = this.getSteps();
            // steps can have a do array of actions or just one do action.
            // should not matter here, as we emit an entire step, listener should deal with that.
            const stepGroup = new StepGroup(StepGroupType.ALGORITHM);
            stepGroup.setSteps(steps);
            // todo: append new algorithm step groups instead of overwriting
            //  but only if not empty and different from last existing step group.
            //  Requires storing the input with the step group.
            //  Requires decoupling between view and DS - like sort tables,
            //  unlike bar charts, networks - start state also has to come from actions,
            //  not from data binding.
            this.stepGroups.length = 0;
            this.stepGroups.push(stepGroup);
          } catch (e) {
            if (e instanceof TypeError) {
              // function getSteps does not exist
              console.log(e.message);
            }
            EventBus.$emit("animationStopped");
            return;
          }
        } else {
          EventBus.$emit("animationStopped");
          return;
        }
        // start the new step group from the beginning
        this.currentStepGroup += 1;
        if (this.currentStepGroup >= this.stepGroups.length) {
          this.currentStepGroup = this.stepGroups.length - 1;
          this.updateStorage(this.stepGroups[this.currentStepGroup], true);
        } else {
          this.updateStorage(this.stepGroups[this.currentStepGroup], true);
        }

        this.nextStep = 0;
      } else {
        // console.log(this.currentStepGroup);
        // console.log(this.nextStep);
        // console.log(this.stepGroups.length);
      }

      const intervalId = setInterval(this.stepForward, 1000 / this.animationSpeed);
      this.$store.commit("setAnimationIntervalId", intervalId);
      EventBus.$emit("animationStarted");
    },
    stepForward() {
      // at last step of last step group, cannot go forward further, stop
      if (this.isAnimationFinished() && this.isAtStepGroupEnd()) {
        clearInterval(this.$store.getters.animationIntervalId);
        EventBus.$emit("animationFinished");
        EventBus.$emit("animationStopped");
        return;
      }
      this.step(Direction.FORWARD);
    },
    goToEnd() {
      clearInterval(this.$store.getters.animationIntervalId);
      const intervalId = setInterval(this.stepForward, 10);
      this.$store.commit("setAnimationIntervalId", intervalId);
    },
    step(direction) {
      // no need to cover stepping back over mutations because this was handled already in stepBack!
      if (this.isStepGroupFinished(direction)) {
        if (typeof this.stepGroups[this.currentStepGroup + direction] === "undefined") {
          // there is no previous/next step group
          return;
        }
        // stepping across stepGroups, increment/decrement currentStepGroup
        this.currentStepGroup += direction;
        if (this.isStepGroupMutation(this.stepGroups[this.currentStepGroup])) {
          direction === Direction.FORWARD
            ? this.updateStorage(this.stepGroups[this.currentStepGroup], true)
            : this.updateStorage(this.stepGroups[this.currentStepGroup], false);
        }

        // reset nextStep accordingly
        if (direction === Direction.FORWARD) {
          this.nextStep = 0;
        } else {
          this.nextStep = this.stepGroups[this.currentStepGroup].steps.length - 1;
        }
        // since we moved across step groups, we have to set the new data structure
        this.setDataStructure(this.stepGroups[this.currentStepGroup].dataStructure);

        // this.setDataStructure(this.stepGroups[this.currentStepGroup].dataStructure);
      } else {
        if (
          direction === Direction.BACK
        ) {
          this.nextStep -= 1;
          if (this.nextStep === -1) {
            return;
          }
        }
        if (direction === Direction.FORWARD && this.nextStep === -1) {
          this.nextStep += 1;
        }
      }

      // get next step
      let step = this.stepGroups[this.currentStepGroup].steps[this.nextStep];

      // step is either array of actions or single action
      const key = direction === Direction.FORWARD ? "do" : "undo";
      if (Array.isArray(step)) {
        step = step.map(item => item[key]);
      } else {
        // step is a single action
        // todo: all steps should be array of actions, not single action
        step = step[key];
      }

      EventBus.$emit("animationStep", step);

      // increment/decrement nextStep
      if (direction === Direction.FORWARD) {
        this.nextStep += direction;
      }
      EventBus.$emit(
        "animationProgress",
        (this.getCurrentPositionInAnimation() * 100) / this.getTotalLength()
      );
    },
    setDataStructure(dataStructure) {
      this.dataStructure = cloneDeep(dataStructure);
    },
    getCurrentDataStructure() {
      if (this.currentStepGroup === 0) {
        return this.stepGroups[this.currentStepGroup].oldDataStructure;
      }
      return this.stepGroups[this.currentStepGroup].dataStructure;
    },
    cutStepGroup() {
      if (this.currentStepGroup === 0 && this.nextStep === -1) {
        this.stepGroups = [];
        this.nextStep = 0;
        this.currentStepGroup = -1;
        return;
      }
      this.stepGroups.splice(this.currentStepGroup + 1);
      this.setDataStructure(this.stepGroups[this.currentStepGroup].dataStructure);
      // this.setAnimationProgress();
    },
    isatStepsGroupEnd() {
      return (
        this.stepGroups === undefined ||
        (this.currentStepGroup === this.stepGroups.length - 1 && this.nextStep !== -1) ||
        this.stepGroups.length === 0
      );
    },
    isAtStepsGroupStart() {
      return this.currentStepGroup === 0 && this.nextStep === -1;
    },
    loadStructure(steps) {
      steps.forEach(step => {
        let steptoDo;
        if (Array.isArray(step)) {
          steptoDo = step.map(item => item.do);
          steptoDo.forEach(step => {
            EventBus.$emit("animationStep", step);
          });
        } else {
          steptoDo = step.do;
          EventBus.$emit("animationStep", [steptoDo]);
        }
      });
    },
    getTotalLength() {
      let total = 0;
      this.stepGroups.forEach(stepGroup => {
        total += stepGroup.steps.length;
      });
      return total;
    },
    getCurrentPositionInAnimation() {
      let current = 0;
      for (let i = 0; i < this.currentStepGroup; i++) {
        current += this.stepGroups[i].steps.length;
      }
      current += this.nextStep;
      return current;
    }
  }
};
