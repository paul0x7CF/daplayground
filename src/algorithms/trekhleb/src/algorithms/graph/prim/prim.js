import Graph from '../../../data-structures/graph/Graph';
import PriorityQueue from '../../../data-structures/priority-queue/PriorityQueue';
import GraphAction from '../../../../../../utils/GraphAction';
import Color from '../../../../../../utils/Color';

/**
 * @param {Graph} graph
 * @return {[]}
 */
export default function prim(graph) {
  const actions = [];
  // It should fire error if graph is directed since the algorithm works only
  // for undirected graphs.
  if (graph.isDirected) {
    throw new Error('Prim\'s algorithms works only for undirected graphs');
  }

  // Init new graph that will contain minimum spanning tree of original graph.
  const minimumSpanningTree = new Graph();

  // This priority queue will contain all the edges that are starting from
  // visited nodes and they will be ranked by edge weight - so that on each step
  // we would always pick the edge with minimal edge weight.
  const edgesQueue = new PriorityQueue();

  // Set of vertices that has been already visited.
  const visitedVertices = {};

  // Vertex from which we will start graph traversal.
  const startVertex = graph.getAllVertices()[0];

  // Add start vertex to the set of visited ones.
  visitedVertices[startVertex.getKey()] = startVertex;

  // Add all edges of start vertex to the queue.
  startVertex.getEdges().forEach((graphEdge) => {
    edgesQueue.add(graphEdge, graphEdge.weight);
  });

  // Now let's explore all queued edges.
  while (!edgesQueue.isEmpty()) {
    // Fetch next queued edge with minimal weight.
    /** @var {GraphEdge} currentEdge */
    const currentMinEdge = edgesQueue.poll();

    // Find out the next unvisited minimal vertex to traverse.
    let nextMinVertex = null;
    if (!visitedVertices[currentMinEdge.source.getKey()]) {
      nextMinVertex = currentMinEdge.source;
    } else if (!visitedVertices[currentMinEdge.target.getKey()]) {
      nextMinVertex = currentMinEdge.target;
    }

    // If all vertices of current edge has been already visited then skip this round.
    if (nextMinVertex) {
      // Add current min edge to MST.
      minimumSpanningTree.addEdge(currentMinEdge);
      actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_LINK,
          color: Color.GRAPH_HIGHLIGHTED_LINK,
          elements: currentMinEdge.getKey(),
        },
        undo: {
          action: GraphAction.HIGHLIGHT_LINK,
          color: Color.GRAPH_LINK_BASE_COLOR,
          elements: currentMinEdge.getKey(),
        },
      });

      // Add vertex to the set of visited ones.
      visitedVertices[nextMinVertex.getKey()] = nextMinVertex;

      // Add all current vertex's edges to the queue.
      nextMinVertex.getEdges().forEach((graphEdge) => {
        // Add only vertices that link to unvisited nodes.
        if (
          !visitedVertices[graphEdge.source.getKey()]
          || !visitedVertices[graphEdge.target.getKey()]
        ) {
          edgesQueue.add(graphEdge, graphEdge.weight);
        }
      });
    }
  }

  return actions;
}
