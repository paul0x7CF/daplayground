import HashTable from '../hash-table/HashTable';
import { v4 as uuidv4 } from 'uuid';
import GraphAction from '../../../../../utils/GraphAction';
import TreeUtils from '../../../../../utils/TreeUtils';

export default class TrieNode {
  /**
   * @param {string} character
   * @param {boolean} isCompleteWord
   */
  constructor(character, isCompleteWord = false) {
    this.character = character;
    this.isCompleteWord = isCompleteWord;
    this.children = new HashTable();
    this.id = 'N' + uuidv4();
  }

  /**
   * @param {string} character
   * @return {TrieNode}
   */
  getChild(character) {
    return this.children.get(character);
  }

  /**
   * @param {string} character
   * @param {boolean} isCompleteWord
   * @param actions
   * @return {TrieNode}
   */
  addChild(character, isCompleteWord = false, actions = 0, dataStructure) {
    if (!this.children.has(character)) {
      const newNode = new TrieNode(character, isCompleteWord);
      this.children.set(character, newNode);
      actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
          dataStructure: dataStructure,
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });

    }

    const childNode = this.children.get(character);

    // actions.push({
    //   do: {
    //     action: GraphAction.HIGHLIGHT_NODE,
    //     color: 'yellow',
    //     elements: childNode.getId(),
    //   },
    //   undo: {
    //     action: GraphAction.HIGHLIGHT_NODE,
    //     color: '69b3a2',
    //     elements: childNode.getId(),
    //   },
    // });

    // In cases similar to adding "car" after "carpet" we need to mark "r" character as complete.
    childNode.isCompleteWord = childNode.isCompleteWord || isCompleteWord;

    // if (childNode.isCompleteWord) {
    //   const endNode = new TrieNode('$', true);
    //   childNode.children.set('$', endNode);
    // }

    return childNode;
  }

  /**
   * @param {string} character
   * @return {TrieNode}
   */
  removeChild(character) {
    const childNode = this.getChild(character);

    // Delete childNode only if:
    // - childNode has NO children,
    // - childNode.isCompleteWord === false.
    if (
      childNode
      && !childNode.isCompleteWord
      && !childNode.hasChildren()
    ) {
      this.children.delete(character);
    }

    return this;
  }

  /**
   * @param {string} character
   * @return {boolean}
   */
  hasChild(character) {
    return this.children.has(character);
  }

  /**
   * Check whether current TrieNode has children or not.
   * @return {boolean}
   */
  hasChildren() {
    return this.children.getKeys().length !== 0;
  }

  /**
   * @return {string[]}
   */
  suggestChildren() {
    return [...this.children.getKeys()];
  }

  /**
   * @return {TrieNode[]}
   */
  getAllChildren() {
    const children = [];
    this.children.getKeys().forEach((item) => children.push(this.children.get(item)));
    return children;
  }

  /**
   * @return {string}
   */
  toString() {
    let childrenAsString = this.suggestChildren().toString();
    childrenAsString = childrenAsString ? `:${childrenAsString}` : '';
    const isCompleteString = this.isCompleteWord ? '*' : '';

    return `${this.character}${isCompleteString}${childrenAsString}`;
  }

  getId() {
    return this.id;
  }
}
