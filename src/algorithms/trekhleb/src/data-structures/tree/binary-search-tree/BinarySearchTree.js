import BinarySearchTreeNode from './BinarySearchTreeNode';
import GraphAction from '../../../../../../utils/GraphAction';
import Color from "../../../../../../utils/Color";
import TreeStructure from '../../../../../trees/TreeStructure';

export default class BinarySearchTree extends TreeStructure {
  /**
   * @param {function} [nodeValueCompareFunction]
   */
  constructor(nodeValueCompareFunction) {
    super();
    this.root = new BinarySearchTreeNode(null, nodeValueCompareFunction);
    this.actions = [];

    // Steal node comparator from the root.
    this.nodeComparator = this.root.nodeComparator;
  }

  /**
   * @param {*} value
   * @return {BinarySearchTreeNode}
   */
  insert(value) {
    this.resetActions();
    return this.root.insert(value, this.actions);
  }

  /**
   * @param {*} value
   * @return {boolean}
   */
  contains(value) {
    this.resetActions();
    return this.root.contains(value, this.actions);
  }

  /**
   * @param {*} value
   * @return {boolean}
   */
  remove(value) {
    this.resetActions();
    return this.root.remove(value, this.actions);
  }

  /**
   * @return {string}
   */
  toString() {
    return this.root.toString();
  }

  resetActions() {
    this.actions = [];
  }

  getActions() {
    return this.actions;
  }

  preOrder() {
    const actions = [];

    function _preOrder(node){
      if (node !== null) {
        actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.TREE_CURRENT_NODE_SELECTED,
            elements: node.value,
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.BASE_COLOR_NODE,
            elements: node.value,
          },
        });
        _preOrder(node.left);
        _preOrder(node.right);
      }
    }

    _preOrder(this.root);
    return actions;
  }

  postOrder() {
    const actions = [];

    function _postOrder(node) {
      if (node !== null) {
        _postOrder(node.left);
        _postOrder(node.right);
        actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.TREE_CURRENT_NODE_SELECTED,
            elements: node.value,
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.BASE_COLOR_NODE,
            elements: node.value,
          },
        });
      }
    }

    _postOrder(this.root);
    return actions;
  }

  inOrder() {
    const actions = [];
    function _inOrder(node) {
      if (node !== null) {
        _inOrder(node.left);
        actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.TREE_CURRENT_NODE_SELECTED,
            elements: node.value,
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.BASE_COLOR_NODE,
            elements: node.value,
          },
        });
        _inOrder(node.right);
      }
    }

    _inOrder(this.root);
    return actions;
  }
}
