import BinaryTreeNode from '../BinaryTreeNode';
import Comparator from '../../../utils/comparator/Comparator';
import GraphAction from '../../../../../../utils/GraphAction';
import TreeUtils from '../../../../../../utils/TreeUtils';
import Color from "../../../../../../utils/Color";

export default class BinarySearchTreeNode extends BinaryTreeNode {
  /**
   * @param {*} [value] - node value.
   * @param {function} [compareFunction] - comparator function for node values.
   */
  constructor(value = null, compareFunction = undefined) {
    super(value);

    // This comparator is used to compare node values with each other.
    this.compareFunction = compareFunction;
    this.nodeValueComparator = new Comparator(compareFunction);
  }

  /**
   * @param {*} value
   * @param actions
   * @return {BinarySearchTreeNode}
   */
  insert(value, actions) {
    actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.TREE_CURRENT_NODE_SELECTED,
        elements: this.value,
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.BASE_COLOR_NODE,
        elements: this.value,
      },
    });
    // console.log(this.value);
    if (this.nodeValueComparator.equal(this.value, null)) {
      this.value = value;

      actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });

      return this;
    }

    if (this.nodeValueComparator.lessThan(value, this.value)) {
      // Insert to the left.
      if (this.left) {
        return this.left.insert(value, actions);
      }

      const newNode = new BinarySearchTreeNode(value, this.compareFunction);
      this.setLeft(newNode);

      actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });

      return newNode;
    }

    if (this.nodeValueComparator.greaterThan(value, this.value)) {
      // Insert to the right.
      if (this.right) {
        return this.right.insert(value, actions);
      }

      const newNode = new BinarySearchTreeNode(value, this.compareFunction);
      this.setRight(newNode);

      actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });

      return newNode;
    }

    return this;
  }

  /**
   * @param {*} value
   * @param actions
   * @return {BinarySearchTreeNode}
   */
  find(value, actions) {
    actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.TREE_CURRENT_NODE_SELECTED,
        elements: this.value,
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.BASE_COLOR_NODE,
        elements: this.value,
      },
    });
    // Check the root.
    if (this.nodeValueComparator.equal(this.value, value)) {
      actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.TREE_CURRENT_NODE_RESULT_TRUE,
          elements: this.value,
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.TREE_CURRENT_NODE_SELECTED,
          elements: this.value,
        },
      });
      return this;
    }

    if (this.nodeValueComparator.lessThan(value, this.value) && this.left) {
      // Check left nodes.
      return this.left.find(value, actions);
    }

    if (this.nodeValueComparator.greaterThan(value, this.value) && this.right) {
      // Check right nodes.
      return this.right.find(value, actions);
    }

    return null;
  }

  /**
   * @param {*} value
   * @param actions
   * @return {boolean}
   */
  contains(value, actions) {
    return !!this.find(value, actions);
  }

  /**
   * @param {*} value
   * @param actions
   * @return {boolean}
   */
  remove(value, actions) {
    const nodeToRemove = this.find(value , []); // actions not needed here

    if (!nodeToRemove) {
      throw new Error('Item not found in the tree');
    }

    // actions.push({
    //   do: {
    //     action: GraphAction.HIGHLIGHT_NODE,
    //     color: Color.TREE_CURRENT_NODE_SELECTED,
    //     elements: this.value,
    //   },
    //   undo: {
    //     action: GraphAction.HIGHLIGHT_NODE,
    //     color: Color.BASE_COLOR_NODE,
    //     elements: this.value,
    //   },
    // });

    const { parent } = nodeToRemove;

    if (!nodeToRemove.left && !nodeToRemove.right) {
      // Node is a leaf and thus has no children.
      if (parent) {
        // Node has a parent. Just remove the pointer to this node from the parent.
        // actions.push({
        //   do: {
        //     action: GraphAction.HIGHLIGHT_NODE,
        //     color: Color.TREE_CURRENT_NODE_SELECTED,
        //     elements: parent.value,
        //   },
        //   undo: {
        //     action: GraphAction.HIGHLIGHT_NODE,
        //     color: Color.BASE_COLOR_NODE,
        //     elements: parent.value,
        //   },
        // });

        actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.TREE_CURRENT_NODE_REMOVE,
            elements: nodeToRemove.value,
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.BASE_COLOR_NODE,
            elements: nodeToRemove.value,
          },
        });

        actions.push({
          do: {
            action: TreeUtils.UPDATE_TREE,
          },
          undo: {
            action: TreeUtils.UPDATE_TREE,
          },
        });
        parent.removeChild(nodeToRemove);
      } else {
        actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: 'yellow',
            elements: nodeToRemove.value,
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.BASE_COLOR_NODE,
            elements: nodeToRemove.value,
          },
        });
        actions.push({
          do: {
            action: TreeUtils.UPDATE_TREE,
          },
          undo: {
            action: TreeUtils.UPDATE_TREE,
          },
        });
        // Node has no parent. Just erase current node value.
        nodeToRemove.setValue(undefined);
      }
    } else if (nodeToRemove.left && nodeToRemove.right) {
      // Node has two children.
      // Find the next biggest value (minimum value in the right branch)
      // and replace current value node with that next biggest value.

      // if (parent) {
      //   actions.push({
      //     do: {
      //       action: GraphAction.HIGHLIGHT_NODE,
      //       color: Color.TREE_CURRENT_NODE_SELECTED,
      //       elements: parent.value,
      //     },
      //     undo: {
      //       action: GraphAction.HIGHLIGHT_NODE,
      //       color: Color.BASE_COLOR_NODE,
      //       elements: parent.value,
      //     },
      //   });
      // }

      actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: 'yellow',
          elements: nodeToRemove.value,
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.BASE_COLOR_NODE,
          elements: nodeToRemove.value,
        },
      });

      const nextBiggerNode = nodeToRemove.right.findMin();

      actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.TREE_CURRENT_NODE_SELECTED,
          elements: nextBiggerNode.value,
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.BASE_COLOR_NODE,
          elements: nextBiggerNode.value,
        },
      });

      if (!this.nodeComparator.equal(nextBiggerNode, nodeToRemove.right)) {
        this.remove(nextBiggerNode.value, []);
        nodeToRemove.setValue(nextBiggerNode.value);
      } else {
        // In case if next right value is the next bigger one and it doesn't have left child
        // then just replace node that is going to be deleted with the right node.
        nodeToRemove.setValue(nodeToRemove.right.value);
        nodeToRemove.setRight(nodeToRemove.right.right);
      }
      actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });
    } else {
      // Node has only one child.
      // Make this child to be a direct child of current node's parent.
      /** @var BinarySearchTreeNode */

      actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: 'yellow',
          elements: nodeToRemove.value,
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.BASE_COLOR_NODE,
          elements: nodeToRemove.value,
        },
      });

      const childNode = nodeToRemove.left || nodeToRemove.right;

      if (parent) {
        parent.replaceChild(nodeToRemove, childNode);
        actions.push({
          do: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.TREE_CURRENT_NODE_SELECTED,
            elements: childNode.value,
          },
          undo: {
            action: GraphAction.HIGHLIGHT_NODE,
            color: Color.BASE_COLOR_NODE,
            elements: childNode.value,
          },
        });

      } else {
        BinaryTreeNode.copyNode(childNode, nodeToRemove);
      }

      actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });
    }

    // Clear the parent of removed node.
    nodeToRemove.parent = null;

    return true;
  }

  /**
   * @return {BinarySearchTreeNode}
   */
  findMin() {
    if (!this.left) {
      return this;
    }

    return this.left.findMin();
  }
}
