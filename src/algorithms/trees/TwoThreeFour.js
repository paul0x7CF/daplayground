/* eslint-disable */

import Node from './TwoThreeFourNode';
import GraphAction from '../../utils/GraphAction';
import TreeUtils from '../../utils/TreeUtils';
import { cloneDeep } from 'lodash';
import TreeStructure from './TreeStructure';
import Color from '../../utils/Color';

export default class TwoThreeFour extends TreeStructure {
  root;
  actions;

  constructor() {
    super();
    this.root = new Node();
    this.actions = [];
  }

  getActions() {
    return this.actions;
  }

  insert(value) {
    this.actions = [];
    const node = new Node();
    node.value = value;

    // this.importHistory += `a${val}`;
    if (this.root === undefined) {
      this.root = node;
    }
    // this.pushToHistory('major', `Begin insert ${val}`, this.root);
    this.insertNode(this.root, value);
    // this.pushToHistory('major', `End insert ${val}`, this.root);
  }

  insertNode(node, value) {
    this.actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.TREE_CURRENT_NODE_SELECTED,
        elements: node.id,
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.BASE_COLOR_TREE,
        elements: node.id,
      },
    });

    let newNode = null;
    for (let i = 0; i < node.values.length; i += 1) {
      if (node.values[i] === value) return;
    } // value exists.
    for (let i = 0; i < node.children.length; i += 1) {
      if (i === node.values.length || node.values[i] > value) {
        newNode = this.insertNode(node.children[i], value);
        break;
      }
    }
    if (node.children.length === 0) { // unterste ebene
      // this.pushToHistory('minor', 'found node', this.root);
      node.insertIndex(value);
      this.actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
          elements: this.root,
          dataStructure: cloneDeep(this),
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });
      // this.pushToHistory('minor', 'insert value', this.root);
      if (node.values.length >= 4) { // overflow->split
        newNode = node.split();
        this.actions.push({
          do: {
            action: TreeUtils.UPDATE_TREE,
            elements: this.root,
            dataStructure: cloneDeep(this),
          },
          undo: {
            action: TreeUtils.UPDATE_TREE,
          },
        });
        if (newNode !== null && node === this.root) {
          this.root = newNode;
          this.actions.push({
            do: {
              action: TreeUtils.UPDATE_TREE,
              elements: this.root,
              dataStructure: cloneDeep(this),
            },
            undo: {
              action: TreeUtils.UPDATE_TREE,
            },
          });
        }
        // eslint-disable-next-line consistent-return
        return newNode;
      }
      // eslint-disable-next-line consistent-return
      return null;
    }

    if (newNode === null) {
      // eslint-disable-next-line consistent-return
      return newNode;
    }
    node.insertIndex(value, newNode);
    this.actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        elements: this.root,
        dataStructure: cloneDeep(this),
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });
    // this.pushToHistory('minor', 'node overflow, split', this.root);
    if (node.values.length >= 4) { // overflow
      // this.pushToHistory('minor', 'root overflow, split', this.root);
      const retNode = node.split();
      this.actions.push({
        do: {
          action: TreeUtils.UPDATE_TREE,
          elements: this.root,
          dataStructure: cloneDeep(this),
        },
        undo: {
          action: TreeUtils.UPDATE_TREE,
        },
      });
      if (node === this.root) {
        this.root = retNode;
      }
      // this.pushToHistory('minor', 'new root', this.root);
      // eslint-disable-next-line consistent-return
      return retNode;
    }
  }

  search(value) {
    this.actions = [];
    const tree = this;
    if (tree.root === undefined) {
      return;
    }

    const currentNode = this.root;

    // eslint-disable-next-line consistent-return,no-shadow
    function whileLoop(tree, actNode) {
      tree.actions.push({
        do: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.TREE_CURRENT_NODE_SELECTED,
          elements: actNode.id,
        },
        undo: {
          action: GraphAction.HIGHLIGHT_NODE,
          color: Color.BASE_COLOR_TREE,
          elements: actNode.id,
        },
      });
      // find index of pointer leading to target:
      // assume its on first place
      let index = 0;
      // if not, iterate
      if (value >= actNode.values[0]) {
        for (let i = 0; i < actNode.values.length; i++) {
          // eslint-disable-next-line max-len
          if (value >= actNode.values[i] && (actNode.values[i + 1] === undefined || value < actNode.values[i + 1])) {
            index = i + 1; // because pointer.length+1 == keys.length
            break;
          }
        }
      }
      actNode.neededKid = index;
      actNode = actNode.children[index];
      actNode.parent.neededKid = undefined;

      for (let i = 0; i < actNode.values.length; i += 1) {
        if (actNode.values[i] === value) {
          tree.actions.push({
            do: {
              action: GraphAction.HIGHLIGHT_NODE,
              color: Color.TREE_CURRENT_NODE_RESULT_TRUE,
              elements: actNode.id,
            },
            undo: {
              action: GraphAction.HIGHLIGHT_NODE,
              color: Color.TREE_CURRENT_NODE_SELECTED,
              elements: actNode.id,
            },
          });
          return true;
        }
      }
      if (actNode.children.length > 0) {
        whileLoop(tree, actNode);
      }
    }

    if (!currentNode.isLeaf()) {
      whileLoop(tree, currentNode);
    } else {
      return false;
    }
  }

  remove(removeValue) {
    this.actions = [];
    if (this.root === undefined) {
      return;
    }
    const val = removeValue;
    // this.pushToHistory('major', `Begin remove ${val}`, this.root);
    const res = this.removeIndex(this.root, val);
    if (res !== undefined) {
      this.removeIndex(this.root, res);
      // this.pushToHistory('minor', `removed next value ${res}`, this.root);
      this.replaceValue(this.root, val, res);
      // this.pushToHistory('minor', `replace value ${val} with ${res}`, this.root);
    }
    this.actions.push({
      do: {
        action: TreeUtils.UPDATE_TREE,
        elements: this.root,
        dataStructure: cloneDeep(this),
      },
      undo: {
        action: TreeUtils.UPDATE_TREE,
      },
    });
    // this.pushToHistory('major', `End remove ${val}`, this.root);
  }

  searchNext(node) {
    if (node.children.length > 0) {
      return this.searchNext(node.children[0]);
    }
    return node.values[0];
  }

  replaceValue(node, value, repWith) {
    let idx = 0;
    while (value > node.values[idx] && idx < node.values.length) idx++;
    if (node.values[idx] === value) {
      node.values[idx] = repWith;
    } else if (node.children.length > 0) {
      this.replaceValue(node.children[idx], value, repWith);
    }
  }

  removeIndex(node, value) {
    this.actions.push({
      do: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.TREE_CURRENT_NODE_SELECTED,
        elements: node.id,
      },
      undo: {
        action: GraphAction.HIGHLIGHT_NODE,
        color: Color.TREE_CURRENT_NODE_SELECTED,
        elements: node.id,
      },
    });
    let res;
    let idx = 0;
    while (value > node.values[idx] && idx < node.values.length) idx++;
    const left = node.getLeft();
    const right = node.getRight();

    if (node.values[idx] === value) {
      if (node.children.length > 0) {
        return this.searchNext(node.children[idx + 1]);
        // dont remove inner index elements, return next element
        // this.pushToHistory('minor', `found next value: ${res}`, this.root);
      }
      node.values.splice(idx, 1);
    }

    if (node.children.length > 0) {
      res = this.removeIndex(node.children[idx], value);
    }

    if (node.values.length === 0) { // underflow
      // console.log(`left: ${(left != undefined) ? left.values : '-'}`);
      // console.log(`right: ${(right != undefined) ? right.values : '-'}`);
      // this.pushToHistory('minor', 'underflow', this.root);

      if (node === this.root) {
        this.root = node;
        this.parent = undefined;
        // this.pushToHistory('minor', 'underflow: swap root', this.root);
      } else if ((left === undefined
        || left.values.length === 1)
        && (right === undefined
          || right.values.length === 1)) {
        // case 1:
        // Bedingung: Alle adjazenten Knoten (benachbarte Knoten auf derselben Tiefe) zum unterlaufenden Knoten v sind 2-Knoten
        // Man verschmilzt v mit einem/dem adjazenten Nachbarn w und verschiebt den nicht mehr benötigten
        // Schlüssel vom Elternknoten u zu dem verschmolzenen Knoten v´
        if (left !== undefined) {
          if (node.children.length > 0) {
            left.children.splice(left.children.length, 0, node.children[0]);
          }
          const pos = node.parent.findIdxPos(value);
          left.values.push(left.parent.values[pos - 1]);
          node.parent.values.splice(pos - 1, 1);
          node.parent.children.splice(pos, 1);
          if (node.parent === this.root && node.parent.values.length === 0) {
            left.parent = undefined;
            this.root = left;
          }
          // this.pushToHistory('minor', 'case 1 underflow, merge left', this.root);
        } else if (right !== undefined) {
          if (node.children.length > 0) {
            right.children.splice(0, 0, node.children[0]);
          }
          const pos = right.parent.findIdxPos(value);
          right.values.splice(0, 0, node.parent.values[pos]);
          node.parent.values.splice(pos, 1);
          node.parent.children.splice(pos, 1);
          if (node.parent === this.root && node.parent.values.length === 0) {
            right.parent = undefined;
            this.root = right;
          }
          // this.pushToHistory('minor', 'case 1 underflow, merge right', this.root);
        }
      } else {
        // console.log('case 2, verschieben');
        // case 2:
        // Verschieben von Schlüsseln
        // Bedingung: Ein adjazenter Knoten (benachbarter Knoten auf derselben Tiefe) w zum unterlaufenden
        // Knoten v ist ein 3-Knoten oder 4-Knoten
        // Man verschiebt ein Kind von w nach v
        // Man verschiebt einen Schlüssel von u nach v
        // Man verschiebt einen Schlüssel von w nach u
        // Nach dem Verschieben ist der Unterlauf behoben
        const v = node;
        if (left !== undefined && (left.values.length === 2 || left.values.length === 3)) {
          // console.log('left');
          // Man verschiebt ein Kind von w nach v
          if (left.children.length > 0) {
            node.children.splice(0, 0, left.children[left.children.length - 1]);
            left.children.splice(-1, 1);
          }
          // Man verschiebt einen Schlüssel von u nach v
          const pos = left.parent.findIdxPos(value);
          node.values.splice(0, 0, left.parent.values[pos - 1]);
          left.parent.values.splice(pos - 1, 1);
          // Man verschiebt einen Schlüssel von w nach u
          left.parent.values.splice(pos - 1, 0, left.values[left.values.length - 1]);
          left.values.splice(-1, 1);
          // this.pushToHistory('minor', 'case 2 underflow, balance left', this.root);
        } else {
          // console.log('right');
          // Man verschiebt ein Kind von w nach v
          if (right.children.length > 0) {
            node.children.splice(0, 0, right.children[0]);
            node.children.splice(0, 1);
          }
          // Man verschiebt einen Schlüssel von u nach v
          const pos = right.parent.findIdxPos(value);
          node.values.splice(0, 0, right.parent.values[pos]);
          right.parent.values.splice(pos, 1);
          // Man verschiebt einen Schlüssel von w nach u
          right.parent.values.splice(pos, 0, right.values[0]);
          right.values.splice(0, 1);
          // this.pushToHistory('minor', 'case 2 underflow, balance right', this.root);
        }
      }
    }
    return res;
  }
}

// function Node(node) {
//
//   if (node !== undefined) {
//     for (let i = 0; i < node.children.length; i++) {
//       this.children.push(new Node(node.children[i]));
//       this.children[i].parent = this;
//     }
//     this.parent = node.parent;
//
//     this.values = node.values;
//   }
// }


// Node.prototype.isLeaf = function () {
//   return this.children.length === 0;
// };

// Node.prototype.split = function () {
//   const parent = new Node();
//   this.parent = parent;
//   const right = new Node();
//   right.parent = parent;
//   right.values = this.values.slice(3);
//   right.children = this.children.slice(3);
//   for (let i = 0; i < right.children.length; i++) right.children[i].parent = right;
//   parent.children.push(this, right);
//   parent.values.push(this.values[2]);
//   this.values = this.values.slice(0, 2);
//   this.children = this.children.slice(0, 3);
//   return parent;
// };

// Node.prototype.findIdxPos = function (val) {
//   let idx = 0;
//   while (val >= this.values[idx] && idx <= this.values.length) idx++;
//   return idx;
// };

// Node.prototype.getLeft = function () {
//   if (this.parent === undefined) return undefined;
//   const value = this.values[0];
//   const idx = this.parent.findIdxPos(value);
//   if (idx > 0) return this.parent.children[idx - 1];
//   return undefined;
// };

// Node.prototype.getRight = function () {
//   if (this.parent == undefined) return undefined;
//   const value = this.values[this.values.length - 1];
//   const idx = this.parent.findIdxPos(value);
//   if (idx < this.parent.children.length - 1) return this.parent.children[idx + 1];
//   return undefined;
// };

// Node.prototype.insertIndex = function (value, node) {
//   let idx = 0;
//   while (value > this.values[idx] && idx < this.values.length) idx++;
//   if (this.values[idx] == value) return; // value existiert bereits
//
//   if (node != null) {
//     this.values.splice(idx, 0, node.values[0]);
//     this.children.splice(idx + 1, 0, node.children[1]);
//     for (let i = 0; i < this.children.length; i++) this.children[i].parent = this;
//     node.parent = this.parent;
//   } else {
//     this.values.splice(idx, 0, value);
//   }
// };

// Node.prototype.print = function () {
//   let txt = ' ';
//   for (let i = 0; i < this.values.length; i++) txt += `,${this.values[i]}`;
//   console.log(txt);
// };
//
// Node.prototype.getSize = function () {
//   return this.values.length + 1;
// };

// function TwoThreeFour() {
//   this.view = new TwoThreeFourView(this);
//   this.history = [];
//   this.root = undefined;
//   this.actStateID = -1;
//   this.importHistory = '';
// }

// TwoThreeFour.prototype.init = function () {
//   this.pushToHistory('init', '', new Node());
//   this.importHistory = '';
//   this.currentVersion = 0;
// };

// TwoThreeFour.prototype.pushToHistory = function (type, text, node) {
//   this.history.push([type, text,
//     JSON.retrocycle( // Zyklische Referenzen wiederherstellen
//       JSON.parse( // Aus serialisierter Darstellung Objekte erstellen
//         JSON.stringify( // Objekte in JSON-Format wandeln
//           JSON.decycle(node),
//         ),
//       ),
//     )]); // Zyklische Referenzen auflösen
// };

// TwoThreeFour.prototype.loadVersion = function (id) {
//   console.log(this.history[id]);
//   this.root = new Node(this.history[id][2]);
//   this.currentVersion = id;
//   this.draw();
// };

// TwoThreeFour.prototype.insertNode = function (node, value) {
//   let newNode = null;
//   for (var i = 0; i < node.values.length; i++) if (node.values[i] == value) return; // value exists.
//   for (var i = 0; i < node.children.length; i++) {
//     if (i == node.values.length || node.values[i] > value) {
//       newNode = this.insertNode(node.children[i], value);
//       break;
//     }
//   }
//   if (node.children.length == 0) { // unterste ebene
//     node.color = this.view.colActive;
//     this.pushToHistory('minor', 'found node', this.root);
//     node.insertIndex(value);
//     this.pushToHistory('minor', 'insert value', this.root);
//     if (node.values.length >= 4) { // overflow->split
//       node.color = this.view.colInvalid;
//       newNode = node.split();
//       newNode.color = this.view.colInvalid;
//       if (newNode != null && node == this.root) {
//         this.root = newNode;
//         this.pushToHistory('minor', 'new root', this.root);
//       }
//       return newNode;
//     }
//     return null;
//   }
//
//   if (newNode == null) return newNode;
//   node.insertIndex(value, newNode);
//   node.color = this.view.colInvalid;
//   newNode.color = this.view.colInvalid;
//   this.pushToHistory('minor', 'node overflow, split', this.root);
//   if (node.values.length >= 4) { // overflow
//     this.pushToHistory('minor', 'root overflow, split', this.root);
//     const retNode = node.split();
//     if (node == this.root) this.root = retNode;
//     node.color = this.view.colInvalid;
//     newNode.color = this.view.colActive;
//     this.pushToHistory('minor', 'new root', this.root);
//     return retNode;
//   }
// };

// TwoThreeFour.prototype.addInt = function (val) {
//   if (isNaN(val)) return;
//   const node = new Node();
//   node.value = val;
//   this.importHistory += `a${val}`;
//   if (this.root == undefined) {
//     this.root = node;
//   }
//   this.pushToHistory('major', `Begin insert ${val}`, this.root);
//   this.insertNode(this.root, val);
//   this.resetColor(this.root);
//   this.pushToHistory('major', `End insert ${val}`, this.root);
//   this.currentVersion = this.history.length - 1;
// };
//
// TwoThreeFour.prototype.add = function () {
//   let strings = validInput('Add:');
//   if (strings == null) return;
//
//   strings = strings.split(' ');
//   for (let i = 0; i < strings.length; i++) {
//     this.addInt(parseInt(strings[i]));
//   }
// };

// TwoThreeFour.prototype.resetColor = function (node) {
//   node.color = '#FFFFFF';
//   for (let i = 0; i < node.children.length; i++) {
//     this.resetColor(node.children[i]);
//   }
// };

// TwoThreeFour.prototype.search = function () {
//   // var val = parseInt(prompt("Search for:"));
//   // if (isNaN(val))
//   // return;
//   const val = validInput('Search  for:');
//   if (val === null) {
//     return;
//   }
//
//   const tree = this;
//   if (tree.root == undefined) return;
//
//   const actNode = this.root;
//   actNode.color = this.view.colInvalid;
//   this.draw();
//
//   function whileLoop(tree, actNode) {
//     tree.resetColor(tree.root);
//     setTimeout(() => {
//       // find index of pointer leading to target:
//       // assume its on first place
//       let index = 0;
//       // if not, iterate
//       if (val >= actNode.values[0]) {
//         for (let i = 0; i < actNode.values.length; i++) {
//           if (val >= actNode.values[i] && (actNode.values[i + 1] == undefined || val < actNode.values[i + 1])) {
//             index = i + 1; // because pointer.length+1 == keys.length
//             break;
//           }
//         }
//       }
//       actNode.neededKid = index;
//       actNode = actNode.children[index];
//       actNode.color = tree.view.colInvalid;
//
//       actNode.parent.color = tree.view.colActive;
//       tree.draw();
//       actNode.parent.neededKid = undefined;
//
//       setTimeout(() => {
//         for (let i = 0; i < actNode.values.length; i++) {
//           if (actNode.values[i] == val) {
//             actNode.color = tree.view.colActive;
//             tree.draw();
//             return;
//           }
//         }
//         if (actNode.children.length > 0) whileLoop(tree, actNode);
//         else {
//           actNode.color = tree.view.colNotFound;
//           tree.draw();
//         }
//       }, 1000);
//     }, 1000);
//   }
//
//   if (!actNode.is_leaf) whileLoop(tree, actNode);
//   else notFound(this);
// };

// TwoThreeFour.prototype.remove = function (removeValue) {
//   if (this.root == undefined) return;
//   let strings;
//   if (removeValue === undefined) strings = validInput('Remove:');
//   else strings = [removeValue];
//   if (strings === null) return;
//
//   strings = strings.split(' ');
//   for (let i = 0; i < strings.length; i++) {
//     const val = parseInt(strings[i]);
//     this.pushToHistory('major', `Begin remove ${val}`, this.root);
//     const res = this.removeIndex(this.root, val);
//     if (res != undefined) {
//       this.removeIndex(this.root, res);
//       this.pushToHistory('minor', `removed next value ${res}`, this.root);
//       this.replaceValue(this.root, val, res);
//       this.pushToHistory('minor', `replace value ${val} with ${res}`, this.root);
//     }
//     this.resetColor(this.root);
//     this.pushToHistory('major', `End remove ${val}`, this.root);
//     this.importHistory += `r${val}`;
//   }
// };
//
// TwoThreeFour.prototype.searchNext = function (node) {
//   if (node.children.length > 0) return this.searchNext(node.children[0]);
//   return node.values[0];
// };
//
// TwoThreeFour.prototype.replaceValue = function (node, value, repWith) {
//   let idx = 0;
//   while (value > node.values[idx] && idx < node.values.length) idx++;
//   if (node.values[idx] == value) {
//     node.values[idx] = repWith;
//   } else if (node.children.length > 0) this.replaceValue(node.children[idx], value, repWith);
// };
//
// TwoThreeFour.prototype.removeIndex = function (node, value) {
//   let res;
//   let idx = 0;
//   while (value > node.values[idx] && idx < node.values.length) idx++;
//   const left = node.getLeft();
//   const right = node.getRight();
//
//   if (node.values[idx] == value) {
//     if (node.children.length > 0) {
//       return this.searchNext(node.children[idx + 1]); // dont remove inner index elements, return next element
//       node.color = this.tree.view.colActive;
//       this.pushToHistory('minor', `found next value: ${res}`, this.root);
//     }
//
//     node.values.splice(idx, 1);
//   }
//
//   if (node.children.length > 0) res = this.removeIndex(node.children[idx], value);
//
//   if (node.values.length == 0) // underflow
//   {
//     console.log(`left: ${(left != undefined) ? left.values : '-'}`);
//     console.log(`right: ${(right != undefined) ? right.values : '-'}`);
//     this.pushToHistory('minor', 'underflow', this.root);
//
//     if (node == this.root) {
//       node.color = this.view.colInvalid;
//       this.root = node;
//       this.parent = undefined;
//       node.color = this.view.colInvalid;
//       this.pushToHistory('minor', 'underflow: swap root', this.root);
//     } else if ((left == undefined
//       || left.values.length == 1)
//       && (right == undefined
//         || right.values.length == 1)) {
//       // case 1:
//       // Bedingung: Alle adjazenten Knoten (benachbarte Knoten auf derselben Tiefe) zum unterlaufenden Knoten v sind 2-Knoten
//       // Man verschmilzt v mit einem/dem adjazenten Nachbarn w und verschiebt den nicht mehr benötigten
//       // Schlüssel vom Elternknoten u zu dem verschmolzenen Knoten v´
//       if (left != undefined) {
//         left.color = this.view.colActive;
//         node.color = this.view.colInvalid;
//         if (node.children.length > 0) {
//           left.children.splice(left.children.length, 0, node.children[0]);
//         }
//         var pos = node.parent.findIdxPos(value);
//         left.values.push(left.parent.values[pos - 1]);
//         node.parent.values.splice(pos - 1, 1);
//         node.parent.children.splice(pos, 1);
//         if (node.parent == this.root && node.parent.values.length == 0) {
//           left.parent = undefined;
//           this.root = left;
//         }
//         node.color = this.view.colActive;
//         this.pushToHistory('minor', 'case 1 underflow, merge left', this.root);
//       } else if (right != undefined) {
//         right.color = this.view.colActive;
//         node.color = this.view.colInvalid;
//         if (node.children.length > 0) {
//           right.children.splice(0, 0, node.children[0]);
//         }
//         var pos = right.parent.findIdxPos(value);
//         right.values.splice(0, 0, node.parent.values[pos]);
//         node.parent.values.splice(pos, 1);
//         node.parent.children.splice(pos, 1);
//         if (node.parent == this.root && node.parent.values.length == 0) {
//           right.parent = undefined;
//           this.root = right;
//         }
//         node.color = this.view.colActive;
//         this.pushToHistory('minor', 'case 1 underflow, merge right', this.root);
//       }
//     } else {
//       console.log('case 2, verschieben');
//       // case 2:
//       // Verschieben von Schlüsseln
//       // Bedingung: Ein adjazenter Knoten (benachbarter Knoten auf derselben Tiefe) w zum unterlaufenden
//       // Knoten v ist ein 3-Knoten oder 4-Knoten
//       // Man verschiebt ein Kind von w nach v
//       // Man verschiebt einen Schlüssel von u nach v
//       // Man verschiebt einen Schlüssel von w nach u
//       // Nach dem Verschieben ist der Unterlauf behoben
//       const v = node;
//       if (left != undefined && (left.values.length == 2 || left.values.length == 3)) {
//         left.color = this.view.colActive;
//         node.color = this.view.colInvalid;
//         console.log('left');
//         // Man verschiebt ein Kind von w nach v
//         if (left.children.length > 0) {
//           node.children.splice(0, 0, left.children[left.children.length - 1]);
//           left.children.splice(-1, 1);
//         }
//         // Man verschiebt einen Schlüssel von u nach v
//         var pos = left.parent.findIdxPos(value);
//         node.values.splice(0, 0, left.parent.values[pos - 1]);
//         left.parent.values.splice(pos - 1, 1);
//         // Man verschiebt einen Schlüssel von w nach u
//         left.parent.values.splice(pos - 1, 0, left.values[left.values.length - 1]);
//         left.values.splice(-1, 1);
//         left.color = this.view.colActive;
//         node.color = this.view.colActive;
//         this.pushToHistory('minor', 'case 2 underflow, balance left', this.root);
//       } else {
//         right.color = this.view.colActive;
//         node.color = this.view.colInvalid;
//         console.log('right');
//         // Man verschiebt ein Kind von w nach v
//         if (right.children.length > 0) {
//           node.children.splice(0, 0, right.children[0]);
//           node.children.splice(0, 1);
//         }
//         // Man verschiebt einen Schlüssel von u nach v
//         var pos = right.parent.findIdxPos(value);
//         node.values.splice(0, 0, right.parent.values[pos]);
//         right.parent.values.splice(pos, 1);
//         // Man verschiebt einen Schlüssel von w nach u
//         right.parent.values.splice(pos, 0, right.values[0]);
//         right.values.splice(0, 1);
//         right.color = this.view.colActive;
//         node.color = this.view.colActive;
//         this.pushToHistory('minor', 'case 2 underflow, balance right', this.root);
//       }
//     }
//   }
//   return res;
// };
