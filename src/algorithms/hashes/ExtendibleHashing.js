import HashStructure from '@/algorithms/hashes/HashStructure';
import Action from '@/components/hashes/HashTableVerticalActions';
import Color from '@/utils/Color';

/**
 * Bucket
 * @param {number} localDepth
 * @param {number} bucketSize
 * @param {number} directoryKey
 * @constructor
 */
function Bucket(localDepth, bucketSize, directoryKey) {
  this.localDepth = localDepth;
  this.elements = new Array(bucketSize);
  this.key = directoryKey;
}

export default class ExtendibleHashing extends HashStructure {
  /**
   * ?
   * @type {number}
   */
  globalDepth;

  /**
   * The Hash Table consists of a list of DataBlocks that hold the values.
   * @type {Bucket[]}
   */
  directory;

  /**
   * Number of elements within a Bucket
   * @type {number}
   */
  MAX_BUCKET_SIZE;

  /**
   * Initialize an empty hash table.
   * @param {number} bucketSize
   */
  init(bucketSize) {
    const steps = [];
    this.globalDepth = 1;
    this.MAX_BUCKET_SIZE = bucketSize;
    this.directory = [];


    // Initially, every index shall reference its own personal Bucket, thus
    // localDepth = globalDepth for all DataBlocks.
    for (let address = 0; address < 2 ** this.globalDepth; address += 1) {
      this.directory[address] = new Bucket(this.globalDepth, this.MAX_BUCKET_SIZE, address);

    }

    this.visualizeCreateTable(steps);
    this.visualizeUpdateInfoBoxes(steps, `Table initialized with bucket size: ${this.MAX_BUCKET_SIZE}`);
    return steps;
  }

  /**
   * Generates a hash value for a given key using a bitmask.
   * @param {number} key - The key to be hashed.
   * @param {number} globalDepth - The global depth for creating the bitmask.
   * @returns {number} - The hashed value.
   */
  hash(key, globalDepth) {
    // The bitmask is created to consist of <globalDepth> 1s.
    const bitMask = (2 ** globalDepth) - 1; // Sum of 2^x series: 1 + 2 + 4 ...
    // Retain only the rightmost <globalDepth> bits using bitwise AND.
    // eslint-disable-next-line no-bitwise
    const result = key & bitMask;
    return result;
  }

  /**
   * Insert a value into the hash table.
   * @param value
   * @returns {steps[]} - The steps of the visualization.
   */
  insert(value) {
    let inserted = false;
    const steps = [];
    while (!inserted) {
      const ADDRESS = this.hash(value, this.globalDepth);
      this.visualizeHashCalculation(steps, value, ADDRESS, 'insert');

      // 1) Add value
      let freeElementIndex;
      for (let i = 0; i < this.MAX_BUCKET_SIZE; i += 1) {
        if (this.directory[ADDRESS].elements[i] === value) {
          freeElementIndex = i;
          inserted = true;
          break;
        }

        if (freeElementIndex === undefined && this.directory[ADDRESS].elements[i] === undefined) {
          // Remember the first free slot
          freeElementIndex = i;
        }

        // Insert value in a free slot
        if (i === (this.MAX_BUCKET_SIZE - 1) && freeElementIndex !== undefined) {
          this.directory[ADDRESS].elements[freeElementIndex] = value;
          inserted = true;
          break;
        }
      }

      if (inserted) {
        this.visualizeHighlightCell(steps, ADDRESS, 'green');
        this.visualizeHighlightCell(steps, ADDRESS, 'green', 'buckets', freeElementIndex);
        this.visualizeInsert(steps, value, ADDRESS, freeElementIndex);
        this.visualizeHighlightCell(steps, ADDRESS, 'base', 'buckets', freeElementIndex);
        this.visualizeHighlightCell(steps, ADDRESS, 'base');
        break;
      }
      this.visualizeHighlightCell(steps, ADDRESS, 'red');

      // not inserted, overflow, split!

      if (this.directory[ADDRESS].localDepth < this.globalDepth) {
        // if localDepth < globalDepth: split the Bucket
        this.visualizeHighlightBucketRow(steps, 'yellow', ADDRESS);

        let haveToShiftKeys = false;
        const KEY_TO_SPLIT = this.directory[ADDRESS].key;
        const OLD_POINTS_LIST = this.directory.map((bucket) => bucket.key);
        // Save old Bucket elements
        const copyOfOldElements = [...this.directory[ADDRESS].elements];
        const oldElements = this.directory[ADDRESS].elements;
        // increase localDepth for the bucket and its references
        this.directory[ADDRESS].localDepth += 1;

        // create a new Bucket
        this.directory[ADDRESS] = new Bucket(this.directory[ADDRESS].localDepth,
          this.MAX_BUCKET_SIZE, ADDRESS);

        // check if the pointers need to be updated
        // because a new bucket has been created at this position.
        if (KEY_TO_SPLIT === ADDRESS) {
          haveToShiftKeys = true;
        }

        // Move oldElements to new Bucket if necessary (???)
        let freePosNew = 0;
        // loop over old elements
        for (let i = 0; i < this.MAX_BUCKET_SIZE; i += 1) {
          const newAddress = this.hash(oldElements[i], this.directory[ADDRESS].localDepth);
          if (newAddress === ADDRESS) {
            this.directory[ADDRESS].elements[freePosNew] = oldElements[i];
            freePosNew += 1;
            oldElements[i] = undefined;
          }
        }

        if (haveToShiftKeys === true) {
          for (let directoryIndex = KEY_TO_SPLIT + 1; directoryIndex < this.directory.length; directoryIndex += 1) {
            if (this.directory[directoryIndex].key === KEY_TO_SPLIT) {
              this.directory[directoryIndex].key = directoryIndex;
              this.visualizeSplitting(steps, OLD_POINTS_LIST, copyOfOldElements,
                ADDRESS, directoryIndex, haveToShiftKeys);
              this.visualizeHighlightBucketRow(steps, 'base', ADDRESS, directoryIndex);
              break;
            }
          }
        } else {
          this.visualizeSplitting(steps, OLD_POINTS_LIST, copyOfOldElements,
            ADDRESS, KEY_TO_SPLIT, haveToShiftKeys);
          this.visualizeHighlightBucketRow(steps, 'base', ADDRESS, KEY_TO_SPLIT);
        }

        // expanded, try to insert in next iteration
      } else if (this.directory[ADDRESS].localDepth === this.globalDepth) {
        // if localDepth == globalDepth: Expand Index

        // Double the size of the Bucket array and point the new fields to the
        // old ones sequentially from 0 to the last field of the "old" array
        const directoryLengthOld = this.directory.length;
        for (let i = directoryLengthOld; i < 2 * directoryLengthOld; i += 1) {
          // Assign references from bucket
          this.directory[i] = this.directory[i - directoryLengthOld];
        }
        this.globalDepth += 1;
        // expanded, try to insert in next iteration
        this.visualizeIncreaseDirectoryTable(steps, ADDRESS);
      }
    }

    return steps;
  }

  /**
   * Search the given value in the hash table.
   * @param value
   */
  search(value) {
    const steps = [];
    const ADDRESS = this.hash(value, this.globalDepth);
    this.visualizeHashCalculation(steps, value, ADDRESS, 'search');
    this.visualizeHighlightCell(steps, ADDRESS, 'green');

    let found = false;
    for (let i = 0; i < this.directory[ADDRESS].elements.length; i += 1) {
      if (this.directory[ADDRESS].elements[i] === value) {
        found = true;
        this.visualizeHighlightCell(steps, ADDRESS, 'green', 'buckets', i);
        this.visualizeHighlightCell(steps, ADDRESS, 'base', 'buckets', i);
        break;
      }
      this.visualizeHighlightCell(steps, ADDRESS, 'red', 'buckets', i);
      this.visualizeHighlightCell(steps, ADDRESS, 'base', 'buckets', i);
    }

    if (found) {
      this.visualizeUpdateInfoBoxes(steps, `Value: ${value} was found`);
      this.visualizeHighlightCell(steps, ADDRESS, 'base');
    } else {
      this.visualizeUpdateInfoBoxes(steps, `Value: ${value} was not found`);
      this.visualizeHighlightCell(steps, ADDRESS, 'red');
      this.visualizeHighlightCell(steps, ADDRESS, 'base');
    }
    return steps;
  }

  /**
   * Remove the given value from the hash table.
   * @param value
   */
  remove(value) {
    const steps = [];
    const ADDRESS = this.hash(value, this.globalDepth);
    this.visualizeHashCalculation(steps, value, ADDRESS, 'remove');
    this.visualizeHighlightCell(steps, ADDRESS, 'green');

    let removed = false;
    for (let i = 0; i < this.directory[ADDRESS].elements.length; i += 1) {
      if (this.directory[ADDRESS].elements[i] === value) {
        this.directory[ADDRESS].elements[i] = undefined;
        this.visualizeHighlightCell(steps, ADDRESS, 'green', 'buckets', i);
        this.visualizeRemove(steps, value, ADDRESS, i);
        this.visualizeHighlightCell(steps, ADDRESS, 'base', 'buckets', i);
        removed = true;
        break;
      }
      this.visualizeHighlightCell(steps, ADDRESS, 'red', 'buckets', i);
      this.visualizeHighlightCell(steps, ADDRESS, 'base', 'buckets', i);
    }

    if (removed) {
      this.visualizeUpdateInfoBoxes(steps, `Value: ${value} was removed`);
      this.visualizeHighlightCell(steps, ADDRESS, 'base');
      // removed
    } else {
      this.visualizeUpdateInfoBoxes(steps, `Value: ${value} to remove was not found`);
      this.visualizeHighlightCell(steps, ADDRESS, 'red');
      this.visualizeHighlightCell(steps, ADDRESS, 'base');
    }
    return steps;
  }

  visualizeCreateTable(steps) {
    const indexes = [];
    const maxBinaryLength = this.directory.length - 1;
    const maxBits = maxBinaryLength.toString(2).length;

    for (let i = 0; i < this.directory.length; i += 1) {
      indexes.push(i.toString(2)
        .padStart(maxBits, '0'));
    }

    steps.push({
      do: [
        {
          action: Action.CREATE_LIST,
          rows: this.directory.length,
          cols: this.MAX_BUCKET_SIZE,
          globalDepth: this.globalDepth,
          pointsMap: this.directory.map((bucket) => bucket.key),
        },
        {
          action: Action.SET_ROW_VALUE,
          bucketIndexes: indexes,
        },
      ],
      undo: [
        {
          action: Action.DELETE_LIST,
        },
        {
          action: Action.SET_ROW_VALUE,
          bucketIndexes: indexes,
        },
      ],
    });
    return steps;
  }

  visualizeUpdateInfoBoxes(steps, text) {
    steps.push({
      do: [{
        action: Action.UPDATE_INFO_BOX,
        bucketSize: this.MAX_BUCKET_SIZE,
        infoText: text,
      }],
      undo: [{
        action: Action.UPDATE_INFO_BOX,
        bucketSize: this.MAX_BUCKET_SIZE,
        infoText: '',
      }],
    });
    return steps;
  }

  visualizeHashCalculation(steps, value, addressDecimal, action) {
    let text;
    switch (action) {
      case 'insert':
        text = `Inserting Value: ${value}`;
        break;
      case 'search':
        text = `Searching for Value: ${value}`;
        break;
      case 'remove':
        text = `Removing Value: ${value}`;
        break;
      default:
    }
    const binaryValue = value.toString(2);
    const addressBinary = addressDecimal.toString(2)
      .padStart(this.globalDepth, '0');
    steps.push({
      do: [
        {
          action: Action.SET_HASH_CALCULATION,
          value,
          binaryValue,
          addressBinary,
        },
        {
          action: Action.UPDATE_INFO_BOX,
          infoText: text,
          bucketSize: null,
        },
      ],
      undo: [
        {
          action: Action.SET_HASH_CALCULATION,
          value: null,
        },
        {
          action: Action.UPDATE_INFO_BOX,
          infoText: '',
          bucketSize: null,
        },
      ],
    });
    return steps;
  }

  visualizeInsert(steps, value, directoryIndex, bucketIndex) {
    steps.push({
      do: [
        {
          action: Action.INSERT_VALUE_AT_POS,
          value,
          row: this.directory[directoryIndex].key,
          col: bucketIndex,
        },
      ],
      undo: [{
        action: Action.REMOVE_VALUE_AT_POS,
        row: this.directory[directoryIndex].key,
        col: bucketIndex,
      }],
    });
    return steps;
  }

  visualizeRemove(steps, oldValue, address, bucketIndex) {
    steps.push({
      do: [{
        action: Action.REMOVE_VALUE_AT_POS,
        row: this.directory[address].key,
        col: bucketIndex,
      }],
      undo: [{
        action: Action.INSERT_VALUE_AT_POS,
        value: oldValue,
        row: this.directory[address].key,
        col: bucketIndex,
      }],
    });
    return steps;
  }

  visualizeHighlightCell(steps, directoryIndex, color, table = 'directory', column = 0) {
    let selectedColor;
    switch (color) {
      case 'red':
        selectedColor = Color.HASH_COLLISION;
        break;
      case 'green':
        selectedColor = Color.HASH_INSERT_SEARCH_COMPLETED;
        break;
      case 'base':
        selectedColor = Color.BASE_COLOR_HASH_TABLE;
        break;
      default:
    }
    steps.push({
      do: [{
        action: Action.SET_ROW_COLOR,
        rowType: table,
        row: directoryIndex,
        pointToRow: this.directory[directoryIndex].key,
        col: column,
        color: selectedColor,
      }],
      undo: [{
        action: Action.SET_ROW_COLOR,
        rowType: table,
        row: directoryIndex,
        pointToRow: this.directory[directoryIndex].key,
        col: column,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });
    return steps;
  }

  visualizeHighlightBucketRow(steps, color, directoryIndexFirst, directoryIndexSecond = null) {
    const pointToRowMap = [];
    if (directoryIndexSecond === null) {
      pointToRowMap.push(this.directory[directoryIndexFirst].key);
    } else {
      pointToRowMap.push(this.directory[directoryIndexFirst].key);
      pointToRowMap.push(this.directory[directoryIndexSecond].key);
    }
    let selectedColor;
    switch (color) {
      case 'yellow':
        selectedColor = Color.HASH_COMPARE;
        break;
      case 'base':
        selectedColor = Color.BASE_COLOR_HASH_TABLE;
        break;
      default:
    }
    steps.push({
      do: [
        {
          action: Action.HIGHLIGHT_BUCKET_ROW,
          pointToRowMap: pointToRowMap,
          color: selectedColor,
        },
        {
          action: Action.UPDATE_INFO_BOX,
          infoText: 'Splitting Bucket because localDepth < globalDepth',
          bucketSize: null,
        },
      ],
      undo: [
        {
          action: Action.HIGHLIGHT_BUCKET_ROW,
          rowType: 'directory',
          pointToRowMap: pointToRowMap,
          color: Color.BASE_COLOR_HASH_TABLE,
        },
        {
          action: Action.UPDATE_INFO_BOX,
          infoText: '',
          bucketSize: null,
        },
      ],
    });
    return steps;
  }

  visualizeIncreaseDirectoryTable(steps, directoryIndex) {
    const indexes = [];
    const shorterIndexes = [];
    const maxBinaryLength = this.directory.length - 1;
    const maxBits = maxBinaryLength.toString(2).length;

    for (let i = 0; i < this.directory.length; i += 1) {
      indexes.push(i.toString(2)
        .padStart(maxBits, '0'));
    }
    for (let i = 0; i < this.directory.length / 2; i += 1) {
      shorterIndexes.push(i.toString(2)
        .padStart(maxBits - 1, '0'));
    }
    steps[steps.length - 1].do.push({
      action: Action.UPDATE_INFO_BOX,
      infoText: 'Expanding Directory because localDepth == globalDepth',
      bucketSize: null,
    });
    steps[steps.length - 1].undo.push({
      action: Action.UPDATE_INFO_BOX,
      infoText: '',
      bucketSize: null,
    });
    steps.push({
      do: [
        {
          action: Action.ADD_DIRECTORY_ENTRY,
          rows: this.directory.length,
          globalDepth: this.globalDepth,
          pointsMap: this.directory.map((bucket) => bucket.key),
        },
        {
          action: Action.SET_ROW_COLOR,
          rowType: 'directory',
          row: directoryIndex,
          color: Color.BASE_COLOR_HASH_TABLE,
        },
        {
          action: Action.ADD_BUCKETS,
          rows: this.directory.length,
        },
        {
          action: Action.SET_ROW_VALUE,
          bucketIndexes: indexes,
        },
      ],
      undo: [
        {
          action: Action.REMOVE_DIRECTORY_ENTRY,
          globalDepth: this.globalDepth - 1,
          rows: this.directory.length / 2,
        },
        {
          action: Action.SET_ROW_COLOR,
          rowType: 'directory',
          row: directoryIndex,
          color: Color.HASH_COLLISION,
        },
        {
          action: Action.REMOVE_BUCKETS,
          rows: this.directory.length / 2,
        },
        {
          action: Action.SET_ROW_VALUE,
          bucketIndexes: shorterIndexes,
        },
      ],
    });
    return steps;
  }

  visualizeSplitting(steps, oldPointsList, oldElements,
    directoryIndex, splitDirectoryIndex, haveToShiftKeys) {
    const pointToRowMap = [];
    if (haveToShiftKeys === true) {
      pointToRowMap.push(this.directory[splitDirectoryIndex].key);
    } else {
      pointToRowMap.push(this.directory[directoryIndex].key);
    }
    let oldLocalDepth = this.directory[directoryIndex].localDepth;
    oldLocalDepth -= 1;
    steps.push({
      do: [
        {
          action: Action.UPDATE_LINK_AT_POS,
          pointsMap: this.directory.map((bucket) => bucket.key),
        },
        {
          action: Action.UPDATE_VALUES_OF_ROW,
          row: directoryIndex,
          localDepth: this.directory[directoryIndex].localDepth,
          values: [...this.directory[directoryIndex].elements],
        },
        {
          action: Action.UPDATE_VALUES_OF_ROW,
          row: splitDirectoryIndex,
          localDepth: this.directory[splitDirectoryIndex].localDepth,
          values: [...this.directory[splitDirectoryIndex].elements],
        },
        {
          action: Action.HIGHLIGHT_BUCKET_ROW,
          pointToRowMap: pointToRowMap,
          color: Color.HASH_COMPARE,
        },
        {
          action: Action.SET_ROW_COLOR,
          rowType: 'directory',
          row: directoryIndex,
          color: Color.BASE_COLOR_HASH_TABLE,
        },
      ],
      undo: [
        {
          action: Action.UPDATE_LINK_AT_POS,
          pointsMap: oldPointsList,
        },
        {
          action: Action.UPDATE_VALUES_OF_ROW,
          row: directoryIndex,
          localDepth: oldLocalDepth,
          values: oldElements,
        },
        {
          action: Action.UPDATE_VALUES_OF_ROW,
          row: splitDirectoryIndex,
          localDepth: oldLocalDepth,
          values: oldElements,
        },
        {
          action: Action.HIGHLIGHT_BUCKET_ROW,
          pointToRowMap: pointToRowMap,
          color: Color.BASE_COLOR_HASH_TABLE,
        },
        {
          action: Action.SET_ROW_COLOR,
          rowType: 'directory',
          row: directoryIndex,
          color: Color.HASH_COLLISION,
        },
      ],
    });
    return steps;
  }
}
