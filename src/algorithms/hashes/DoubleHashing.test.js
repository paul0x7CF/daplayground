import DoubleHashing from './DoubleHashing';

describe('DoubleHashing.js', () => {
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], [
    ]],
    // Reverse input
    [[5, 4, 3, 2, 1], [
    ]],
    // Only one number
    [[4], []],
    // Repeating numbers
    [[1, 2, 1], [
    ]],
    // Random input
    [[3, 8, 2, 7, 1], [
    ]],
  ])('returns the correct actions, input %#: %p', (input, expectedOutput) => {
    // const actualOutput = RadixSort(input);
    // console.log(actualOutput);
    // expect(actualOutput.length).toEqual(expectedOutput.length);
    // expectedOutput.forEach((element, index) => {
    //   expect(actualOutput[index]).toEqual(element);
    // });
  });
});
