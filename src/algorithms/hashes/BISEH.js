import Action from '../../components/hashes/HashTableVerticalActions';
import HashStructure from "@/algorithms/hashes/HashStructure";
import Color from "@/utils/Color";


export default class BISEH extends HashStructure {

  indexSize = 2;

  entrySize = 2;

  bucketSize = 2;

  indexes = [];
  lastInformationField = '';

  init(indexDepth = 3, rowsPerIndex = 3, dataBlockSize = 3) {
    const steps = [];
    this.indexSize = indexDepth;
    this.entrySize = rowsPerIndex;
    this.bucketSize = dataBlockSize;
    // Build fresh hash table
    this.indexes = [];
    for (let i = 0; i < 2 ** this.indexSize; i += 1) {
      let entriesForIndex = [];
      for (let j = 0; j < 2 ** this.entrySize; j++) {

        let binaryRepresentationEntry = j.toString(2).padStart(this.entrySize, '0');
        entriesForIndex.push({
          description: binaryRepresentationEntry, value: 0, bucketsForEntry: [],
        });
      }
      let binaryRepresentationIndex = i.toString(2).padStart(this.indexSize, '0');
      this.indexes.push({
        description: binaryRepresentationIndex, entriesForIndex: entriesForIndex,
      });
    }
    this.addCreationSteps(steps);
    return this.addUpdatedInformation(steps, indexDepth, rowsPerIndex, dataBlockSize);
  }


  getIndexAddress(value) {
    let binaryValue = value.toString(2);
    binaryValue = binaryValue.slice(-this.indexSize);
    while (binaryValue.length < this.indexSize) {
      binaryValue = '0' + binaryValue;
    }
    return binaryValue;
  }


  getRowAddress(value) {
    let binaryValue = value.toString(2);
    binaryValue = binaryValue.slice(0, -this.indexSize);
    binaryValue = binaryValue.slice(-this.entrySize);

    while (binaryValue.length < this.entrySize) {
      binaryValue = '0' + binaryValue;
    }

    return binaryValue;
  }


  insert(value) {
    const steps = [];
    this.visValueAlreadyInHashTable(steps, value, "Inserting value " + value);
    const hashedIndex = this.getIndexAddress(value);
    const hashedDepth = this.getRowAddress(value);

    const indexBlock = this.findIndexBlock(hashedIndex);
    const entryBlock = this.findEntryBlock(indexBlock, hashedDepth);
    const rowsForEntry = this.getNumberOfRowsInBucket(entryBlock);
    let hashedBucketRow = this.hashedBucketRow(entryBlock, value);
    this.addUpdatedHashCalculation(hashedIndex, hashedDepth, hashedBucketRow, steps, value, rowsForEntry);
    const copiedEntryBlock = JSON.parse(JSON.stringify(entryBlock));

    if (entryBlock.value !== 0) {
      const returnedSteps = this.search(value, false);
      steps.push(...returnedSteps);
    }
    if (this.isValueInHashTable(value)) {
      this.visValueAlreadyInHashTable(steps, value, "Value already in hash table.");
      let colDesc = this.findColForValue(entryBlock, value);
      this.visHashedCell(steps, hashedIndex, hashedDepth, colDesc, hashedBucketRow, Color.HASH_COLLISION);
      return steps;
    }

    if (this.checkIfChangedEntryValue(entryBlock, hashedBucketRow)) { //need to split bucket rows
      this.visAddColumn(steps, copiedEntryBlock, hashedIndex);
      this.visUpdatedInformation(steps, "Bucket has to split.");
      let allValuesInBucket = this.getAllValuesFromBucket(entryBlock);
      if (allValuesInBucket.length !== 0) {
        this.visUpdatedInformation(steps, "Removing all current values in bucket and reinserting them.");
      }
      this.removeAllValuesFromBucket(entryBlock, allValuesInBucket);
      this.addColumnToBucket(entryBlock);
      allValuesInBucket.push(value);
      this.insertAllValues(steps, allValuesInBucket);
      this.visResetAllColors(steps);
      return steps;
    }

    //add in last place of bucket
    let block = this.insertValueInBucket(entryBlock, value, hashedBucketRow);
    this.visInsertValue(steps, indexBlock, entryBlock, block, value);
    this.visUpdatedInformation(steps, "Value inserted in hash table.");
    return steps;
  }

  findIndexBlock(hashedIndex) {
    for (let i = 0; i < this.indexes.length; i++) {
      if (this.indexes[i].description === hashedIndex) {
        return this.indexes[i];
      }
    }
  }

  findEntryBlock(indexBlock, hashedDepth) {
    for (let i = 0; i < indexBlock.entriesForIndex.length; i++) {
      if (indexBlock.entriesForIndex[i].description === hashedDepth) {
        return indexBlock.entriesForIndex[i];
      }
    }
  }

  hashedBucketRow(entryBlock, value) {
    let rows = 0;
    if (entryBlock.bucketsForEntry.length !== 0) { //there are already values in this entry
      rows = entryBlock.bucketsForEntry[0].rowForBuckets.length;
    }

    const binaryValue = value.toString(2);
    const neededDigits = this.indexSize + this.entrySize + rows;
    const paddedBinary = binaryValue.padStart(neededDigits, '0');
    const remainingBinaryAfterIndex = paddedBinary.slice(0, -this.indexSize);
    const remainingBinaryAfterEntry = remainingBinaryAfterIndex.slice(0, -this.entrySize);

    if (rows === 1 || rows === 0) {
      return 0;
    }

    const paddedBinaryAfterEntry = remainingBinaryAfterEntry.padStart(rows, '0');
    const digitsToCut = Math.ceil(Math.log2(rows));

    return remainingBinaryAfterEntry.slice(-(digitsToCut));
  }

  search(value, showNotFoundError = true) {
    let steps = [];
    const indexAddress = this.getIndexAddress(value);
    const entryAddress = this.getRowAddress(value);
    const indexBlock = this.findIndexBlock(indexAddress);
    const entryBlock = this.findEntryBlock(indexBlock, entryAddress);
    const hashedBucketRow = this.hashedBucketRow(entryBlock, value);
    const hashedBucketRowDec = parseInt(hashedBucketRow, 2);
    this.visUpdatedInformation(steps, "Searching for value " + value);
    this.visHashedIndex(steps, indexAddress);

    this.visHashedEntry(steps, indexAddress, entryAddress);


    if (this.isValueInHashTable(value)) {
      for (let i = 0; i < entryBlock.bucketsForEntry.length; i++) {
        if (entryBlock.bucketsForEntry[i].rowForBuckets[hashedBucketRowDec].value === value) {
          this.visHashedRow(steps, indexAddress, entryAddress, hashedBucketRow, Color.HASH_COMPARE);
          setTimeout(() => {
            this.visHashedCell(steps, indexAddress, entryAddress, i, hashedBucketRow, Color.HASH_INSERT_SEARCH_COMPLETED);
          }, 1000);
          return steps;
        }
      }
    } else {
      //value not found
      if (showNotFoundError) {
        this.visHashedRow(steps, indexAddress, entryAddress, hashedBucketRow, Color.HASH_COLLISION);
      } else {
        this.visHashedRow(steps, indexAddress, entryAddress, hashedBucketRow, Color.HASH_COMPARE);
      }
      this.visUpdatedInformation(steps, "Value not found in hash table.");
    }

    this.visResetAllColors(steps);
    return steps;
  }


  remove(value) {
    let steps = [];
    const indexAddress = this.getIndexAddress(value);
    const entryAddress = this.getRowAddress(value);
    const indexBlock = this.findIndexBlock(indexAddress);
    const entryBlock = this.findEntryBlock(indexBlock, entryAddress);
    const hashedBucketRow = this.hashedBucketRow(entryBlock, value);

    this.visUpdatedInformation(steps, "Removing value " + value);
    this.addUpdatedHashCalculation(indexAddress, entryAddress, hashedBucketRow, steps, value, this.getNumberOfRowsInBucket(entryBlock));
    this.visHashedIndex(steps, indexAddress);
    this.visHashedEntry(steps, indexAddress, entryAddress);

    if (this.isValueInHashTable(value)) {
      let foundBucket =null;
      const hashedBucketRowDec = parseInt(hashedBucketRow, 2);
      for (let i = 0; i < entryBlock.bucketsForEntry.length; i++) {
        if (entryBlock.bucketsForEntry[i].rowForBuckets[hashedBucketRowDec].value === value) {
          this.visHashedRow(steps, indexAddress, entryAddress, hashedBucketRow, Color.HASH_COMPARE);
          this.visHashedCell(steps, indexAddress, entryAddress, i, hashedBucketRow, Color.HASH_INSERT_SEARCH_COMPLETED);
          this.visUpdatedInformation(steps, "Value found and removed from hash table.");
          entryBlock.bucketsForEntry[i].rowForBuckets[hashedBucketRowDec].value = null;
          this.visRemoveValue(steps, indexBlock, entryBlock, i, hashedBucketRow, value);
          foundBucket = entryBlock.bucketsForEntry[i];

        }
        if(foundBucket!==null){
          if(entryBlock.bucketsForEntry[i].rowForBuckets[hashedBucketRowDec].value!==null){
            let value = entryBlock.bucketsForEntry[i].rowForBuckets[hashedBucketRowDec].value;
            entryBlock.bucketsForEntry[i].rowForBuckets[hashedBucketRowDec].value = null;
            foundBucket.rowForBuckets[hashedBucketRowDec].value = value;
            this.visRemoveValue(steps, indexBlock, entryBlock, i, hashedBucketRow, value);
            this.visInsertValue(steps, indexBlock, entryBlock, {bucketRow: hashedBucketRow, bucketCol: i-1}, value);
            foundBucket=entryBlock.bucketsForEntry[i];
          }
        }
      }
      return steps;
    } else {
      this.visHashedRow(steps, indexAddress, entryAddress, hashedBucketRow, Color.HASH_COLLISION);
      this.visUpdatedInformation(steps, "Value not found in hash table.");
    }
    this.visResetAllColors(steps);
    return steps;
  }


  //Visiualizing

  checkIfChangedEntryValue(entryBlock, hashedBucketRow) {
    const descRow = parseInt(hashedBucketRow, 2);
    //first value
    if (entryBlock.bucketsForEntry.length === 0) {
      return true;
    }
    if (entryBlock.bucketsForEntry[entryBlock.bucketsForEntry.length - 1].rowForBuckets[descRow].value !== null) {
      return true;
    }

    for (let i = 0; i < entryBlock.bucketsForEntry.length; i++) { //iterate through cols
      if (i === (entryBlock.bucketsForEntry.length - 1)) { //last col
        if (entryBlock.bucketsForEntry[i].rowForBuckets[descRow].value !== null) {
          return true;
        } else {
          let newArray = {...entryBlock.bucketsForEntry[i].rowForBuckets[descRow]};
        }
      }

    }
    return false;
  }

  addColumnToBucket(entryBlock) {
    entryBlock.value++;
    const split = entryBlock.value;
    const newRows = Math.pow(2, split - 1);

    //iterate through columns
    for (let i = 0; i < this.bucketSize; i++) {
      //no buckets there yet -> create them
      if (entryBlock.bucketsForEntry[i] === undefined) {
        let description = entryBlock.description.slice(-1);
        entryBlock.bucketsForEntry.push({
          value: null, description: description, rowForBuckets: [],
        })
      }

      for (let l = 0; l < newRows; l++) {
        let binaryCombination = this.getBinaryCombination(l, split - 1);
        if (entryBlock.bucketsForEntry[i].rowForBuckets[l] === undefined) {
          entryBlock.bucketsForEntry[i].rowForBuckets.push({
            description: binaryCombination,
            value: null,
          })
        } else {
          entryBlock.bucketsForEntry[i].rowForBuckets[l].description = binaryCombination;
        }

      }
    }
    return;
  }

  getBinaryCombination(number, length) {
    let binary = number.toString(2);
    while (binary.length < length) {
      binary = '0' + binary;
    }
    return binary;
  }

  insertValueInBucket(entryBlock, value, row) {
    for (let i = 0; i < entryBlock.bucketsForEntry.length; i++) {
      //iterate through columns
      const decRow = parseInt(row, 2);
      if (entryBlock.bucketsForEntry[i].rowForBuckets[decRow].value === null) {
        entryBlock.bucketsForEntry[i].rowForBuckets[decRow].value = value;
        return {
          bucketRow: entryBlock.bucketsForEntry[i].rowForBuckets[decRow].description,
          bucketCol: i,
        };
      }

    }

  }


  getAllValuesFromBucket(entryBlock) {
    const allValues = [];

    for (const bucket of entryBlock.bucketsForEntry) {
      for (const row of bucket.rowForBuckets) {
        if (row.value !== null) {
          allValues.push(row.value);
        }
      }
    }
    return allValues;
  }

  removeAllValuesFromBucket(entryBlock, values) {
    for (const bucket of entryBlock.bucketsForEntry) {
      for (const row of bucket.rowForBuckets) {
        row.value = null;
      }
      bucket.value = 0;
    }
  }

  getIndex(){
    return this.indexSize;
  }
  getEntry(){
    return this.entrySize;
  }

  getBucketSize(){
    return this.bucketSize;
  }

  isValueInHashTable(value) {
    for (const index of this.indexes) {
      for (const entry of index.entriesForIndex) {
        for (const bucketCol of entry.bucketsForEntry) {
          for (const bucketRow of bucketCol.rowForBuckets) {
            if (bucketRow.value === value) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  findColForValue(entryBlock, value) {
    for (let i = 0; i < entryBlock.bucketsForEntry.length; i++) {
      for (let j = 0; j < entryBlock.bucketsForEntry[i].rowForBuckets.length; j++) {
        if (entryBlock.bucketsForEntry[i].rowForBuckets[j].value === value) {
          return i;
        }
      }
    }
  }

  //Visualization
  addCreationSteps(steps) {
    const rememberInformationField = this.lastInformationField;
    this.lastInformationField = 'Initialized with indexSize ' + this.indexSize + ', entrySize ' + this.entrySize + ' and bucketSize ' + this.bucketSize;
    steps.push({
      do: [{
        action: Action.CREATE_LIST,
        indexDepth: this.indexSize,
        entryDepth: this.entrySize,
        bucketSize: this.bucketSize,
      }, {
        action: Action.UPDATE_INFO_BOX, text: this.lastInformationField,
      },], undo: [{
        action: Action.DELETE_LIST,
      }, {
        action: Action.SET_INFORMATION_FIELD, text: rememberInformationField,
      },],
    });
  }

  visUpdatedInformation(steps, informationText) {
    steps.push({
      do: [{
        action: Action.SET_INFORMATION_FIELD,
        text: informationText,
      }],
      undo: [{
        action: Action.SET_INFORMATION_FIELD,
        text: {...this.lastInformationField}
      }]
    })
    this.lastInformationField = informationText;
  }

  addUpdatedInformation(steps, indexDepth, rowsPerIndex, dataBlockSize) {
    this.lastInformationField = "Initialized with indexSize: " + indexDepth + ", entryDepths: " + rowsPerIndex + ", bucket size: " + dataBlockSize;
    steps.push({
      do: [{
        action: Action.SET_INFORMATION_FIELD,
        text: this.lastInformationField,
      }, {
        action: Action.SET_HASH_CALCULATION,
        indexDepth: indexDepth,
        entryDepth: rowsPerIndex,
        bucketSize: dataBlockSize,
      },], undo: [{
        action: Action.DELETE_LIST,
      }, {
        action: Action.SET_INFORMATION_FIELD,
        text: '',
      }, {
        action: Action.SET_HASH_CALCULATION,
        text: null,
      },],
    });
    return steps;
  }


  addUpdatedHashCalculation(hashedIndex, hashedDepth, hashedBucketRow, steps, value, rowsForEntry) {
    const undoText = this.calc;
    const binaryRepresentation = value.toString(2);
    let text = "";

    const coloredHashedIndex = `<span style="color: red;">${hashedIndex}</span>`;
    const coloredHashedDepth = `<span style="color: dodgerblue;">${hashedDepth}</span>`;
    const coloredHashedBucketRow = `<span style="color: limegreen;">${hashedBucketRow}</span>`;

    if (rowsForEntry <= 1) {
      text = `Val: ${value} binary: ${binaryRepresentation} = y: ${coloredHashedDepth} x: ${coloredHashedIndex}`;
    } else {
      text = `Val: ${value} binary: ${binaryRepresentation} = z: ${coloredHashedBucketRow} y: ${coloredHashedDepth} x: ${coloredHashedIndex}`;
    }


    this.calc = text;


    steps.push({
      do: [{
        action: Action.SET_HASH_CALCULATION,
        text: this.calc,
      }],
      undo: [{
        action: Action.SET_HASH_CALCULATION,
        text: undoText,
      }],
    });

    return steps;

  }


  visAddColumn(steps, entryBlock, indexSize) {
    steps.push({
      do: [{
        action: Action.ADD_COLUMNS,
        entry: entryBlock.description,
        row: entryBlock.value,
        index: indexSize,
      }], undo: [{
        action: Action.REMOVE_COLUMNS,
        index: indexSize,
        entry: entryBlock.description,
        entryBlock: {...entryBlock},
      }],
    });
  }


  visInsertValue(steps, indexBlock, entryBlock, bucketInformation, value) {
    steps.push({
      do: [{
        action: Action.INSERT_VALUE_AT_POS,
        entry: entryBlock.description,
        row: bucketInformation.bucketRow,
        col: bucketInformation.bucketCol,
        index: indexBlock.description,
        value: value,
      }],
      undo: [{
        action: Action.REMOVE_VALUE_AT_POS,
        entry: entryBlock.description,
        row: bucketInformation.bucketRow,
        col: bucketInformation.bucketCol,
        index: indexBlock.description,
        value: value,
      }]
    });
  }


  insertAllValues(steps, allValuesInBucket) {
    for (const value of allValuesInBucket) {
      const returnedSteps = this.insert(value);
      steps.push(...returnedSteps);
    }
  }

  visHashedIndex(steps, indexAddress) {
    steps.push({
      do: [{
        action: Action.HIGHLIGHT_INDEX,
        index: indexAddress,
        color: Color.HASH_COMPARE,
      }],

    })
  }

  visHashedEntry(steps, indexAddress, entryAddress) {
    steps.push({
      do: [{
        action: Action.HIGHLIGHT_ENTRY,
        index: indexAddress,
        entry: entryAddress,
        color: Color.HASH_COMPARE,
      }],

    })

  }

  visHashedRow(steps, indexAddress, entryAddress, hashedBucketRow1, color) {
    steps.push({
      do: [{
        action: Action.HIGHLIGHT_BUCKET_ROW,
        index: indexAddress,
        entry: entryAddress,
        row: hashedBucketRow1,
        color: color,
      }],
      undo: [{
        action: Action.HIGHLIGHT_BUCKET_ROW,
        index: indexAddress,
        entry: entryAddress,
        row: hashedBucketRow1,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    })

  }


  visValueAlreadyInHashTable(steps, value, text) {
    steps.push({
      do: [{
        action: Action.SET_TABLE_FULL,
        text: "Value " + value + " already in hash table",
      },
        {
          action: Action.SET_INFORMATION_FIELD,
          text: text,
        },],
      undo: [{
        action: Action.SET_TABLE_FULL,
        text: "Value " + value + " already in hash table",
      }, {
        action: Action.SET_INFORMATION_FIELD,
        text: {...this.lastInformationField}
      },],
    });

    this.lastInformationField = text;
  }

  visHashedCell(steps, indexAddress, entryAddress, description, hashedBucketRow, color) {
    steps.push({
      do: [{
        action: Action.SET_CELL_COLOR,
        index: indexAddress,
        entry: entryAddress,
        col: description,
        row: hashedBucketRow,
        color: color,
      }],
      undo: [{
        action: Action.SET_CELL_COLOR,
        index: indexAddress,
        entry: entryAddress,
        col: description,
        row: hashedBucketRow,
        color: Color.BASE_COLOR_HASH_TABLE,
      }],
    });
  }

  visRemoveValue(steps, indexBlock, entryBlock, i, hashedBucketRow, value) {
    steps.push({
      do: [{
        action: Action.REMOVE_VALUE_AT_POS,
        entry: entryBlock.description,
        row: hashedBucketRow,
        col: i,
        index: indexBlock.description,
        value: value,
      }],
      undo: [{
        action: Action.INSERT_VALUE_AT_POS,
        entry: entryBlock.description,
        row: hashedBucketRow,
        col: i,
        index: indexBlock.description,
        value: value,
      }],
    });
  }


  visResetAllColors(steps) {
    steps.push({
      do: [{
        action: Action.RESET_COLORS,
      }],
      undo: [{
        action: Action.RESET_COLORS,
      }],
    });


  }

  getNumberOfRowsInBucket(entryBlock) {
    if (entryBlock.bucketsForEntry.length === 0) {
      return 0;
    }
    return entryBlock.bucketsForEntry[0].rowForBuckets.length;
  }
}
