import SeparateChaining from './SeparateChaining';

describe('SeparateChaining.js', () => {
  it('should insert and expand correctly', () => {
    const ht = new SeparateChaining();

    ht.init(2);

    ht.insert(2);
    ht.insert(1);
    ht.insert(5);

    expect(ht.numItems).toBe(3);
    expect(ht.rows.length).toBe(4);

    // level 1
    expect(ht.rows[0]).toBe(undefined);
    expect(ht.rows[1]).not.toBe(undefined);
    expect(ht.rows[2]).not.toBe(undefined);
    expect(ht.rows[3]).toBe(undefined);

    // level 1 values
    expect(ht.rows[1].value).toBe(5);
    expect(ht.rows[2].value).toBe(2);

    // level 1 next
    expect(ht.rows[1].next).not.toBe(undefined);
    expect(ht.rows[2].next).toBe(undefined);

    // level 2
    expect(ht.rows[1].next.value).toBe(1);
    expect(ht.rows[1].next.next).toBe(undefined);
  });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], [
    ]],
    // Reverse input
    [[5, 4, 3, 2, 1], [
    ]],
    // Only one number
    [[4], []],
    // Repeating numbers
    [[1, 2, 1], [
    ]],
    // Random input
    [[3, 8, 2, 7, 1], [
    ]],
  ])('returns the correct actions, input %#: %p', (input, expectedOutput) => {
    // const actualOutput = RadixSort(input);
    // console.log(actualOutput);
    // expect(actualOutput.length).toEqual(expectedOutput.length);
    // expectedOutput.forEach((element, index) => {
    //   expect(actualOutput[index]).toEqual(element);
    // });
  });
});
