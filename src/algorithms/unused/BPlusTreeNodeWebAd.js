export default class Node {
    keys;

    pointers;

    isLeaf;

    parent;

    rightPointer;

    isOrphan; // Knoten

    constructor() {
      this.keys = [];
      this.pointers = [];
      this.isLeaf = true;
      this.parent = undefined;
      this.rightPointer = undefined;
      this.isOrphan = false;
    }
}
