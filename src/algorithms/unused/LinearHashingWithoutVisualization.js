/**
 * Block. The object to hold the actual values.
 * @constructor
 */
function Block() {
  /**
   * The binary address of the block.
   * Only used for visualization.
   * @type {string}
   */
  this.index = undefined;

  /**
   * An array to store the values of this block.
   * @type {number[]}
   */
  this.values = [];

  /**
   * A nested Block object that holds the overflowing values.
   * @type {Block}
   */
  this.overflow = undefined;
}

/**
 * LinearHashing
 */
export default class LinearHashing {
  /**
   * An array of Block objects to hold the values.
   * @type {Block[]}
   */
  blocks = [];

  /**
   * Initially addressable number of Blocks (in bits).
   * Example:
   * d = 3 -> 3 bit addresses, 4 addressable blocks.
   * d = 4 -> 4 bit addresses, 8 addressable blocks.
   * @type {number}
   */
  d = 3;

  /**
   * Block Size
   * @type {number}
   */
  b = 4;

  /**
   * Next To Split.
   * @todo find out what nextToSplit means.
   * @type {number}
   */
  nextToSplit = 0;

  /**
   * Initialize an empty hash table.
   * @param {number} addressLength
   * @param {number} blockSize
   */
  init(addressLength = 2, blockSize = 2) {
    this.blocks = [];
    this.d = addressLength;
    this.b = blockSize;
    this.nextToSplit = 0;

    // fill the blocks array with as many blocks as should be initially addressable.
    for (let i = 0; i < 2 ** this.d; i += 1) {
      const block = new Block();
      // convert i to binary, only use the d rightmost bits
      block.index = i.toString(2).slice(-Math.abs(this.d));
      this.blocks.push(block);
    }
  }

  /**
   * The hash function is dependent on the length of the address space in bits (d).
   * @param {number} value
   */
  hash(value) {
    // convert value to binary, only use the d rightmost bits
    let addressBinary = value.toString(2).slice(-Math.abs(this.d));
    let addressDecimal = parseInt(addressBinary, 2);

    // todo: reason for the following if-condition?
    if (addressDecimal < this.nextToSplit) {
      // convert value to binary, only use the d-1 rightmost bits
      addressBinary = value.toString(2).slice(-Math.abs(this.d + 1));
      addressDecimal = parseInt(addressBinary, 2);
    }

    return addressDecimal;
  }

  /**
   * Insert the given value into the hash table.
   */
  insert(value) {
    if (this.blocks.length === 0) {
      // Table not created
      return;
    }

    // Calculate the address of the block where value should go
    let addressDecimal = this.hash(value);

    if (this.blocks[addressDecimal].values.length < this.b) {
      // free space in the designated bucket, put the value there, done.
      this.blocks[addressDecimal].values.push(value);
    } else {
      // designated bucket is full.

      // descend down in last overflow block
      let currentBlock = this.blocks[addressDecimal];
      while (currentBlock.overflow !== undefined) {
        currentBlock = currentBlock.overflow;
      }

      if (currentBlock.values.length < this.b) {
        // free space in currentBlock, put the value there, done.
        currentBlock.values.push(value);
      } else { // this is where I lose it:
        // not enough space in currentBlock, split.
        const newOverflow = new Block();
        newOverflow.values.push(value);
        currentBlock.overflow = newOverflow;

        // add new Block at the end and update vars
        currentBlock = this.blocks[this.nextToSplit];
        const newBlock = new Block();
        newBlock.index = `1${currentBlock.index}`;
        this.blocks.push(newBlock);

        // get all values from act row+overflows

        const vals = [];
        while (currentBlock !== undefined) {
          for (let i = 0; i < currentBlock.values.length; i += 1) {
            vals.push(currentBlock.values[i]);
          }
          currentBlock = currentBlock.overflow;
        }

        // delete all values overflows of act row
        currentBlock = this.blocks[this.nextToSplit];
        currentBlock.index = `0${currentBlock.index}`;
        currentBlock.values = [];
        currentBlock.overflow = undefined;

        // d wird erhöht, wenn splitting für ursprünglichen Primärbereich einmal durchgeführt wurde:
        this.nextToSplit += 1;
        if (this.nextToSplit === 2 ** this.d) {
          this.d += 1;
          this.nextToSplit = 0;
        }

        // redistribute
        for (let i = 0; i < vals.length; i += 1) {
          // Calculate the address/bucket/block where value should go
          addressDecimal = this.hash(vals[i]);

          // descend down in last overflow block
          currentBlock = this.blocks[addressDecimal];
          while (currentBlock.overflow !== undefined) {
            currentBlock = currentBlock.overflow;
          }

          if (currentBlock.values.length < this.b) {
            currentBlock.values.push(vals[i]);
          } else {
            // overflow again?
            const overflowBlock = new Block();
            overflowBlock.values.push(vals[i]);
            currentBlock.overflow = overflowBlock;
          }
        }
      }
    }
  }

  /**
   * Search for the given value in the hash table.
   * @param value
   */
  search(value) {
    if (this.blocks.length === 0) {
      // Table not created
      return;
    }

    const addressDecimal = this.hash(value);

    let currentBlock = this.blocks[addressDecimal];

    let found = false;
    let index = 0;

    while (!found) {
      for (let i = 0; i < this.b; i += 1) {
        if (currentBlock.values[i] === value) {
          found = true;
          index = i;
          break;
        } else if (currentBlock.values[i] === undefined) {
          break;
        }
      }
      if (!found) {
        currentBlock = currentBlock.overflow;
      }
      if (currentBlock === undefined) {
        // reached last overflow block, value not found.
        break;
      }
    }
    // if 'found', then data is at 'index'.
    // else, did not find anything.
  }

  /**
   * Remove the given value from the hash table.
   * @param value
   */
  remove(value) {
    if (this.blocks.length === 0) {
      // Table not created
      return;
    }

    const addressDecimal = this.hash(value);

    let currentBlock = this.blocks[addressDecimal];
    let previousBlock;

    let found = false;
    let index = 0;

    while (!found) {
      for (let i = 0; i < this.b; i += 1) {
        if (currentBlock.values[i] === value) {
          found = true;
          index = i;
          break;
        } else if (currentBlock.values[i] === undefined) {
          break;
        }
      }
      if (!found) {
        previousBlock = currentBlock;
        currentBlock = currentBlock.overflow;
      }
      if (currentBlock === undefined) {
        // reached last overflow block, value not found.
        return;
      }
    }

    // remove value
    currentBlock.values.splice(index, 1);

    // move all values to the left
    let { overflow } = currentBlock;
    if (overflow === undefined && currentBlock.values.length < 1 && previousBlock !== undefined) {
      previousBlock.overflow = undefined;
      return;
    }

    while (overflow !== undefined) {
      currentBlock.values.push(overflow.values[0]);
      overflow.values.splice(0, 1);

      // overflow now empty:
      if (overflow.values.length < 1) {
        if (overflow.overflow === undefined) {
          currentBlock.overflow = undefined;
        } else {
          currentBlock.overflow = overflow.overflow;
        }
      }
      currentBlock = overflow;
      overflow = currentBlock.overflow;
    }
  }
}
