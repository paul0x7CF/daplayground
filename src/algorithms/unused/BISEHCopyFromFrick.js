/* eslint-disable no-param-reassign */
import Action from '../../components/hashes/HashTableVerticalActions';

/**
 * Each Row initially holds 1 DataBlock.
 * DataBlocks store the actual values.
 * If more than <blockSize> elements hash to the same DataBlock of a row, the number of DataBlocks
 * in this row is doubled until there is no more than <blockSize> collisions.
 * @constructor
 * @param blockSize
 */
function DataBlock(blockSize) {
  this.values = new Array(blockSize);
}

/**
 * Each Index holds y=entrySize Rows.
 * Each Row initially holds 1 DataBlock.
 * If more than <blockSize> elements hash to the same DataBlock of a row, the number of DataBlocks
 * in this row is doubled until there is no more than <blockSize> collisions.
 * @constructor
 */


function Row() {
  /**
   * How often this row has been extended. i.e. how often the
   * number of Data Blocks in this Row has been doubled.
   * Also known as z.
   * @type {number}
   */
  this.extendCounter = 0;

  /**
   * DataBlocks. Holds 2^z DataBlocks.
   * @type {DataBlock[]}
   */
  this.dataBlocks = [];
}

/**
 * There are x=indexSize Index objects in BISEH.indexes.
 * An Index holds y=entrySize Rows.
 * @constructor
 * @param {number} rowsPerIndex
 */
function Index(rowsPerIndex) {
  /**
   * @type {Row[]}
   */
  this.rows = [];

  for (let i = 0; i < 2 ** rowsPerIndex; i += 1) {
    this.rows[i] = new Row();
  }
}

/**
 * BISEH - Bounded Index Size Extendible Hashing
 *
 * BISEH uses three hash functions to find the right index area, index entry and data block.
 * It is characterized by a constant index size and is a new form of Extendible Hashing.
 * If a data block reaches its limit, then the data area is going to expand.
 *
 * A BISEH hash table consists of (x=indexSize) Indexes.
 * Each Index holds (y=entrySize) Rows.
 * Each Row initially holds (1) DataBlock.
 * Each DataBlock can hold (b=blockSize) values.
 *
 * All addressing is done in binary.
 *
 * If more than <blockSize> elements hash to the same DataBlock of a row, the number of DataBlocks
 * in this row is doubled until there is no more than <blockSize> collisions.
 * The number of doubles (z) is also the length of the address of the DataBlock of a Row, in bits.
 */
export default class BISEHCopyFromFrick {
  /**
   * Number of Indexes. Also known as x.
   * Power of 2, ie. number of bits of binary index address.
   * 2^x = number of Index() objects in BISEH.indexes.
   * @type {number}
   */
  indexSize = 2;

  /**
   * Rows per Index. Also known as y.
   * Power of 2, ie. number of bits of binary row address within index.
   * 2^y = number of Row() Objects in Index.rows.
   * @type {number}
   */
  entrySize = 2;

  /**
   * DataBlock Size. Also known as b.
   * Maximum number of values a DataBlock can hold.
   * @type {number}
   */
  bucketSize = 2;

  /**
   * Consists of <indexSize> 'Index' objects.
   * @type {Index[]}
   */
  indexes = [];
  lastInformationField = '';

  /**
   * Initialize an empty hash table.
   * @param {number} indexDepth
   * @param {number} rowsPerIndex
   * @param {number} dataBlockSize
   */
  init(indexDepth = 3, rowsPerIndex = 3, dataBlockSize = 3) {
    const steps = [];
    this.indexSize = indexDepth;
    this.entrySize = rowsPerIndex;
    this.bucketSize = dataBlockSize;
    console.log("Init: received: ", this.indexSize, this.entrySize, this.bucketSize);
    // Build fresh hash table
    this.indexes = [];
    for (let i = 0; i < 2 ** this.indexSize; i += 1) {
      let entrySizes = [];
      for(let j=0; j<2**this.entrySize; j++){
        this.indexes[i] = new Index(this.entrySize);
      }

    }
    this.addCreationSteps(steps);
    return this.addUpdatedInformation(steps, indexDepth, rowsPerIndex, dataBlockSize);
  }

  /**
   * The Index is given by the rightmost <indexSize> bits of the binary representation of the given
   * value.
   * @param {number} value
   * @returns {number}
   */
  getIndexAddress(value) {
    // The bitwise & of two decimal numbers returns a decimal number.
    // eslint-disable-next-line no-bitwise
    let i = value & (2 ** this.indexSize - 1);
    return i;
  }

  /**
   * The Row is given by the rightmost <entrySize> bits to the left of the <indexSize> bits of
   * the binary representation of the given value.
   * @param {number} value
   * @returns {number}
   */
  getRowAddress(value) {
    let binaryValue = value.toString(2);
    binaryValue = binaryValue.slice(0, -1);
    binaryValue = binaryValue.slice(-this.entrySize);

    while (binaryValue.length < this.entrySize) {
      binaryValue = '0' + binaryValue;
    }

    console.log("y ", binaryValue);
    return binaryValue;
  }

  /**
   * The DataBlock is given by the rightmost <counter> bits to the left of
   * <indexSize+entrySize> bits of the binary representation of the given value.
   * The address is based on how many DataBlocks there are in a Row (counter).
   * @param {number} value
   * @param {number} counter
   * @returns {number}
   */
  getDataBlockAddress(value, counter) {
    // <counter> 1s to the left of <indexSize+entrySize> bits
    const bitMask = 2 ** (this.indexSize + this.entrySize + counter)
      - 2 ** (this.indexSize + this.entrySize);
    // eslint-disable-next-line no-bitwise
    return value & bitMask;
  }

  /**
   * Extend the available space in the given Row by doubling the number of DataBlocks in the row to
   * accommodate more values.
   * todo: function BISEH.extend() needs a sanity check!
   * @param {Row} row
   */
  extend(row) {
    const newRow = new Row();
    let dataBlockAddress;
    row.extendCounter += 1;

    // loop over indexBlock
    for (let i = 0; i < row.dataBlocks.length; i += 1) {
      newRow.dataBlocks[i] = row.dataBlocks[i];
      for (let j = 0; j < row.dataBlocks[i].values.length; j += 1) {
        if (row.dataBlocks[i].values !== undefined) {
          newRow.dataBlocks[i].values[j] = row.dataBlocks[i].values[j];
        }
      }
    }

    for (let i = 0; i < 2 ** row.extendCounter; i += 1) {
      row.dataBlocks[i] = new DataBlock(this.bucketSize);
    }

    for (let i = 0; i < newRow.dataBlocks.length; i += 1) {
      for (let j = 0; j < newRow.dataBlocks[i].values.length; j += 1) {
        if (newRow.dataBlocks[i].values[j] !== undefined) {
          dataBlockAddress = this
            .getDataBlockAddress(newRow.dataBlocks[i].values[j], row.extendCounter);
          if (dataBlockAddress !== undefined) { // why/how could this be undefined?
            for (let k = 0; k < this.bucketSize; k += 1) {
              if (row.dataBlocks[dataBlockAddress].values[k] === undefined) {
                row.dataBlocks[dataBlockAddress].values[k] = newRow.dataBlocks[i].values[j];
                break;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Insert the given value into the hash table.
   * @param value
   */
  insert(value) {
    const hashedIndex = this.getIndexAddress(value);
    const hashedDepth = this.getRowAddress(value);
    console.log("hashed depth: ", hashedDepth);
    const row = this.indexes[hashedIndex].rows[hashedDepth];

    if (row.dataBlocks[0] === undefined) {
      row.dataBlocks[0] = new DataBlock(this.bucketSize);
    }

    // check if the value is already there
    let alreadyInserted = false;
    for (let i = 0; i < this.bucketSize; i += 1) {
      const dataBlockAddress = this.getDataBlockAddress(value, row.extendCounter);
      if (row.dataBlocks[dataBlockAddress].values[i] === value) {
        alreadyInserted = true;
        break;
      }
    }

    if (alreadyInserted) {
      return;
    }

    // value is not in the hash table, insert it.
    let inserted = false;
    for (let i = 0; i < this.bucketSize; i += 1) { // loop over all DataBlocks of the given row
      if (row.extendCounter === 0) {
        // special case:
        // 'row' has never been extended, therefor the dataBlockIndex is ignored.
        if (row.dataBlocks[0].values[i] === undefined) {
          // found place, insert, done.
          row.dataBlocks[0].values[i] = value;
          inserted = true;
          break;
        }
      } else { // 'row' was already extended, need to also consider dataBlockIndex.
        const dataBlockIndex = this.getDataBlockAddress(value, row.extendCounter);
        if (
          row.dataBlocks[dataBlockIndex] !== undefined
          && row.dataBlocks[dataBlockIndex].values[i] === undefined
        ) {
          // found place, insert, done.
          row.dataBlocks[dataBlockIndex].values[i] = value;
          inserted = true;
          break;
        }
      }
    }

    if (inserted) {
      // done, return.
      return;
    }

    // value could not be inserted, extend table and insert value
    while (!inserted) {
      // extend until everything fits
      this.extend(row);
      // recalculate dataBlockIndex after extension
      const dataBlockIndex = this.getDataBlockAddress(value, row.extendCounter);
      // check all elements in the DataBlock
      for (let j = 0; j < this.bucketSize; j += 1) {
        if (
          row.dataBlocks[dataBlockIndex].values[j] === undefined
          && row.dataBlocks[dataBlockIndex] !== undefined
        ) {
          // found place, insert, done.
          row.dataBlocks[dataBlockIndex].values[j] = value;
          inserted = true;
          break;
        }
      }
    }
  }

  /**
   * Search the given value in the hash table.
   * @param value
   */
  search(value) {
    const xIndex = this.getIndexAddress(value);
    const yIndex = this.getRowAddress(value);

    const row = this.indexes[xIndex].rows[yIndex];

    const dataBlockAddress = this.getDataBlockAddress(value, row.extendCounter);

    if (row.dataBlocks[dataBlockAddress] === undefined) {
      // not found.
      return false;
    }

    for (let i = 0; i < row.dataBlocks[dataBlockAddress].values.length; i += 1) {
      if (value === row.dataBlocks[dataBlockAddress].values[i]) {
        //found
        return true;
      }
    }
    return false;
  }

  /**
   * Remove the given value from the hash table.
   * @param value
   */
  remove(value) {
    const xIndex = this.getIndexAddress(value);
    const yIndex = this.getRowAddress(value);

    const row = this.indexes[xIndex].rows[yIndex];

    const dataBlockAddress = this.getDataBlockAddress(value, row.extendCounter);

    if (row.dataBlocks[dataBlockAddress] === undefined) {
      // not found.
      return false;
    }

    for (let i = 0; i < row.dataBlocks[dataBlockAddress].values.length; i += 1) {
      if (value === row.dataBlocks[dataBlockAddress].values[i]) {
        row.dataBlocks[dataBlockAddress].values[i] = undefined;
        //removed
        return true;
      }

    }

    //not removed
    return false;
  }


  //Visiualizing
  addCreationSteps(steps) {
    const rememberInformationField = this.lastInformationField;
    this.lastInformationField = 'Initialized with indexSize ' + this.indexSize + ', entrySize ' + this.entrySize + ' and bucketSize ' + this.bucketSize;
    steps.push({
      do: [
        {
          action: Action.CREATE_LIST,
          indexDepth: this.indexSize,
          entryDepth: this.entrySize,
          bucketSize: this.bucketSize,
        },
        {
          action: Action.UPDATE_INFO_BOX,
          text: this.lastInformationField,
        },],
      undo: [
        {
          action: Action.DELETE_LIST,
        },
        {
          action: Action.SET_INFORMATION_FIELD,
          text: rememberInformationField,
        },
      ],
    });
  }

  addUpdatedInformation(steps, indexDepth, rowsPerIndex, dataBlockSize) {
    this.lastInformationField = "Initialized with indexSize: " + indexDepth + ", entryDepths: " + rowsPerIndex + ", bucket size: " + dataBlockSize;
    steps.push({
      do: [
        {
          action: Action.SET_INFORMATION_FIELD,
          text: this.lastInformationField,
        },
        {
          action: Action.SET_HASH_CALCULATION,
          indexDepth: indexDepth,
          entryDepth: rowsPerIndex,
          bucketSize: dataBlockSize,
        },
      ],
      undo: [
        {
          action: Action.DELETE_LIST,
        },
        {
          action: Action.SET_INFORMATION_FIELD,
          text: '',
        },
        {
          action: Action.SET_HASH_CALCULATION,
          text: null,
        },
      ],
    });
    return steps;
  }
}
