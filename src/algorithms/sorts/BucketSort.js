import Action from '../../components/sorts/TablesHorizontalActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm BucketSort
 *
 * Implementation based on pseudocode from Wikipedia.
 *
 * @author Bernhard Frick
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {{do: {}[], undo: {}[]}[]}
 */
export default function BucketSort(elements, testing = false) {
  const steps = [];

  if (elements.length < 2) {
    // nothing to sort
    // also, bucket mapping slope breaks if input is only 1 element.
    return testing ? Array.from(elements) : steps;
  }

  steps.push({
    do: [{
      action: Action.CLEAR,
    }],
    undo: [],
  });

  // number of buckets
  // this could be a lot more sophisticated, but is
  // easiest when using number of elements.
  const n = elements.length;

  // create inputTable, bucketsTable and outputTable
  steps.push({
    do: [
      {
        action: Action.CREATE_TABLE,
        id: 'inputTable',
        title: 'Input',
        rows: n,
        cols: 1,
      },
      {
        action: Action.CREATE_TABLE,
        id: 'bucketsTable',
        title: 'Buckets',
        rows: n,
        cols: 2,
      },
      {
        action: Action.CREATE_TABLE,
        id: 'outputTable',
        title: 'Output',
        rows: n,
        cols: 1,
      },
    ],
    undo: [
      {
        action: Action.DELETE_TABLE,
        id: 'inputTable',
      },
      {
        action: Action.DELETE_TABLE,
        id: 'bucketsTable',
      },
      {
        action: Action.DELETE_TABLE,
        id: 'outputTable',
      },
    ],
  });

  // insert input values into input table
  let doActions = [];
  let undoActions = [];
  for (let i = 0; i < elements.length; i += 1) {
    doActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'inputTable',
      row: i,
      col: 0,
      value: elements[i],
    });
    undoActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'inputTable',
      row: i,
      col: 0,
      value: null,
    });
  }
  steps.push({
    do: doActions,
    undo: undoActions,
  });

  // create Buckets array with empty Bucket arrays
  const buckets = new Array(n);
  for (let i = 0; i < n; i += 1) {
    buckets[i] = [];
  }

  // find min and max of elements to correctly map numbers to buckets
  const min = Math.min(...elements);
  const max = Math.max(...elements);

  // mapping from values to buckets
  const slope = (n - 1) / (max - min);
  

  // set bucket names
  // todo: specify value range of each bucket
  doActions = [];
  undoActions = [];
  for (let i = 0; i < n; i += 1) {
    doActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'bucketsTable',
      row: i,
      col: 0,
      value: `Bucket ${i}`,
    });
    undoActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'bucketsTable',
      row: i,
      col: 0,
      value: null,
    });
  }
  steps.push({
    do: doActions,
    undo: undoActions,
  });

  // put elements into buckets
  for (let i = 0; i < n; i += 1) {
    // highlight value
    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'inputTable',
          row: i,
          color: Color.TH_CURRENT_ELEMENT,
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'inputTable',
          row: i,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
      ],
    });

    // calculate bucket
    const index = Math.floor(slope * (elements[i] - min));

    // highlight bucket
    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: index,
          color: Color.TH_CURRENT_ELEMENT,
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: index,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
      ],
    });

    // insert value into bucket
    const undoValue = buckets[index].join(', ');
    buckets[index].push(elements[i]);
    const doValue = buckets[index].join(', ');
    steps.push({
      do: [
        {
          action: Action.SET_CELL_VALUE,
          id: 'bucketsTable',
          row: index,
          col: 1,
          value: doValue,
        },
      ],
      undo: [
        {
          action: Action.SET_CELL_VALUE,
          id: 'bucketsTable',
          row: index,
          col: 1,
          value: undoValue,
        },
      ],
    });

    // clear both highlights
    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'inputTable',
          row: i,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: index,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'inputTable',
          row: i,
          color: Color.TH_CURRENT_ELEMENT,
        },
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: index,
          color: Color.TH_CURRENT_ELEMENT,
        },
      ],
    });
  }

  // sort all buckets
  buckets.forEach((bucket, index) => {
    const undoValue = bucket.join(', ');
    // array.sort converts to string before sorting by default
    // to compare numbers, we have to supply a compare function
    // eslint-disable-next-line no-param-reassign
    bucket = bucket.sort((a, b) => a - b);
    const doValue = bucket.join(', ');
    if (doValue !== undoValue) {
      steps.push({
        do: [
          {
            action: Action.SET_ROW_COLOR,
            id: 'bucketsTable',
            row: index,
            color: Color.TH_CURRENT_ELEMENT,
          },
        ],
        undo: [
          {
            action: Action.SET_ROW_COLOR,
            id: 'bucketsTable',
            row: index,
            color: Color.BASE_COLOR_TABLE_HORIZONTAL,
          },
        ],
      });
      steps.push({
        do: [
          {
            action: Action.SET_CELL_VALUE,
            id: 'bucketsTable',
            row: index,
            col: 1,
            value: doValue,
          },
        ],
        undo: [
          {
            action: Action.SET_CELL_VALUE,
            id: 'bucketsTable',
            row: index,
            col: 1,
            value: undoValue,
          },
        ],
      });
      steps.push({
        do: [
          {
            action: Action.SET_ROW_COLOR,
            id: 'bucketsTable',
            row: index,
            color: Color.BASE_COLOR_TABLE_HORIZONTAL,
          },
        ],
        undo: [
          {
            action: Action.SET_ROW_COLOR,
            id: 'bucketsTable',
            row: index,
            color: Color.TH_CURRENT_ELEMENT,
          },
        ],
      });
    }
  });

  // the sorted list is the concatenation of all buckets
  const sorted = [];
  let i = 0;
  buckets.forEach((bucket, bucketIndex) => {
    // highlight bucket
    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: bucketIndex,
          color: Color.TH_CURRENT_ELEMENT,
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: bucketIndex,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
      ],
    });
    bucket.forEach((element) => {
      // insert elements from bucket into sorted output
      sorted[i] = element;
      steps.push({
        do: [
          {
            action: Action.SET_CELL_VALUE,
            id: 'outputTable',
            row: i,
            col: 0,
            value: element,
          },
        ],
        undo: [
          {
            action: Action.SET_CELL_VALUE,
            id: 'outputTable',
            row: i,
            col: 0,
            value: null,
          },
        ],
      });

      i += 1;
    });

    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'outputTable',
          row: bucketIndex,
          col: 0,
          color: Color.TH_ELEMENT_SORTED,
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'outputTable',
          row: bucketIndex,
          col: 0,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
      ],
    });

    // clear highlight bucket
    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: bucketIndex,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: bucketIndex,
          color: Color.TH_CURRENT_ELEMENT,
        },
      ],
    });
  });

  return testing ? sorted : steps;
}
