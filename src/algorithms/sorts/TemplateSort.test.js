import TemplateSort from './TemplateSort';
import Action from '../../components/sorts/BarChartActions';
import Color from '../../utils/Color';
import getRandomInt from '../../utils/getRandomInt';
import isSorted from '../../utils/isSorted';

const expectedStep = {
  do: [
    {
      action: Action.SET_COLORS,
      elements: [
        {
          index: 1,
          color: Color.BASE_COLOR_BAR_CHART,
        },
      ],
      text: 'Setting blue color on element 1.',
    },
  ],
  undo: [
    {
      action: Action.SET_COLORS,
      elements: [
        {
          index: 1,
          color: Color.WHITE,
        },
      ],
      text: 'Undo setting blue color on element 1.',
    },
  ],
};

describe('TemplateSort.js', () => {
  // uncomment this test
  // it('should sort the given random input', () => {
  //   const numTests = 1000;
  //   const inputSize = 50;
  //   for (let i = 0; i < numTests; i += 1) {
  //     const randomNumbers = new Array(inputSize)
  //       .fill(null)
  //       .map((_) => getRandomInt(0, 100));
  //     const sortedOutput = TemplateSort(randomNumbers, true);
  //     expect(isSorted(sortedOutput)).toBe(true);
  //   }
  // });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], [expectedStep/* put expected output here */]],
    // Reverse input
    [[5, 4, 3, 2, 1], [expectedStep/* put expected output here */]],
    // Only one number
    [[4], [expectedStep/* put expected output here */]],
    // Repeating numbers
    [[1, 2, 1], [expectedStep/* put expected output here */]],
    // Random input
    [[3, 8, 2, 7, 1], [expectedStep/* put expected output here */]],
  ])('should return the expected steps, input %#: %p', (input, expectedOutput) => {
    const actualOutput = TemplateSort(input);
    // use the code below to print the steps the algorithm returns:
    // const util = require('util');
    // console.dir(util.inspect(actualOutput, { depth: null }));
    expect(actualOutput.length).toEqual(expectedOutput.length);
    expectedOutput.forEach((element, index) => {
      expect(actualOutput[index]).toEqual(element);
    });
  });
});
