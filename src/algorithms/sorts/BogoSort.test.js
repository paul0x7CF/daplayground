import BogoSort from './BogoSort';
import Action from '../../components/sorts/BarChartActions';
import isSorted from '../../utils/isSorted';
import getRandomInt from '../../utils/getRandomInt';

describe('BogoSort.js', () => {
  it('should sort the given random input', () => {
    const numTests = 1000;
    const inputSize = 5;
    for (let i = 0; i < numTests; i += 1) {
      const randomNumbers = new Array(inputSize)
        .fill(null)
        .map((_) => getRandomInt(0, 100));
      const sortedOutput = BogoSort(randomNumbers, true);
      expect(isSorted(sortedOutput)).toBe(true);
    }
  });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], []],
    // Reverse input
    [[5, 4, 3, 2, 1], []],
    // Only one number
    [[4], []],
    // Repeating numbers
    [[1, 2, 1], []],
    // Random input
    [[3, 8, 2, 7, 1], []],
  ])('steps lead to a sorted result, input %#: %p', (input, expectedOutput) => {
    const inputCopy = Array.from(input);
    // steps cannot be tested because they are randomized.

    const steps = BogoSort(input);

    // apply exchange actions
    for (let i = 0; i < steps.length; i += 1) {
      if (steps[i].do[0].action === Action.EXCHANGE) {
        const a = steps[i].do[0].elements[0];
        const b = steps[i].do[0].elements[1];
        // exchange
        const t = inputCopy[a];
        inputCopy[a] = inputCopy[b];
        inputCopy[b] = t;
      }
    }

    // check if result is sorted
    expect(isSorted(inputCopy)).toBe(true);
  });
});
