/* eslint-disable no-param-reassign */

import Action from '../../components/sorts/TablesHorizontalActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm Binary QuickSort, a.k.a. Radix Exchange Sort
 * Due to the binary input the QuickSort algorithm is simplified.
 *
 * @author Bernhard Frick
 * @param {number[]} input An array of binary strings to be sorted
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function BinaryQuickSort(input, testing = false) {
  const steps = [];

  // filter the input
  const filteredInput = Array
    .from(input, String) // number to string
    .filter((e) => e.length > 0) // length check
    .filter((e) => e.split('').every((bit) => !Number.isNaN(parseInt(bit, 2)))); // is only 0 or 1?

  if (filteredInput.length === 0) {
    return testing
      ? filteredInput.map((e) => parseInt(e, 2))
      : steps;
  }

  steps.push({
    do: [{
      action: Action.CLEAR,
    }],
    undo: [],
  });

  // get number of values and longest value as rows and cols for table
  const rows = filteredInput.length;
  const cols = filteredInput.reduce((r, s) => (r > s.length ? r : s.length), 0);

  const elements = Array
    .from(filteredInput)
    .map((e) => e.padStart(cols, '0')) // pad to fixed length
    .map((e) => e.split('')) // split into separate digits
    .map((e) => e.map((bit) => parseInt(bit, 2))); // parse all bit strings to int

  // Create as many tables as there are bits in the longest value
  let doActions = [];
  let undoActions = [];
  for (let i = 0; i < cols; i += 1) {
    doActions.push({
      action: Action.CREATE_TABLE,
      id: `table${2 ** (cols - i - 1)}`,
      title: `Bit 2^${cols - i - 1} = ${2 ** (cols - i - 1)}`,
      rows,
      cols,
      colWidth: new Array(cols).fill(23),
      rowHeight: new Array(rows).fill(20),
    });
    undoActions.push({
      action: Action.DELETE_TABLE,
      id: `table${i}`,
    });
  }
  steps.push({
    do: doActions,
    undo: undoActions,
  });

  function bqs(matrix, col, left, right) {
    if (col === cols) {
      // all cols done
      return;
    }

    // fill the table of the current col with the values between the given left and right pointers
    doActions = [];
    undoActions = [];
    for (let i = left; i < right + 1; i += 1) {
      for (let j = 0; j < cols; j += 1) {
        doActions.push({
          action: Action.SET_CELL_VALUE,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          col: j,
          value: matrix[i][j],
        });
        undoActions.push({
          action: Action.SET_CELL_VALUE,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          col: j,
          value: null,
        });
      }
    }
    steps.push({
      do: doActions,
      undo: undoActions,
    });

    // set border of current column
    steps.push({
      do: [{
        action: Action.SET_COLUMN_STROKE,
        id: `table${2 ** (cols - col - 1)}`,
        col,
        stroke: 3,
      }],
      undo: [{
        action: Action.SET_COLUMN_STROKE,
        id: `table${2 ** (cols - col - 1)}`,
        col,
        stroke: 1,
      }],
    });

    let l = left;
    let r = right;

    // paint rows l and r
    steps.push({
      do: [
        {
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: l,
          color: matrix[l][col] === 0 ? Color.TH_CURRENT_ELEMENT : Color.TH_CURRENT_ELEMENT,
        },
        {
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: r,
          color: matrix[r][col] === 1 ? Color.TH_CURRENT_ELEMENT : Color.TH_CURRENT_ELEMENT,
        },
      ],
      undo: [
        {
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: l,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
        {
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: r,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        },
      ],
    });

    while (l < r) {
      while (matrix[l][col] === 0 && l < r) {
        l += 1;
        steps.push({
          do: [
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: l - 1,
              color: Color.BASE_COLOR_TABLE_HORIZONTAL,
            },
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: l,
              color: matrix[l][col] === 0 ? Color.TH_CURRENT_ELEMENT : Color.TH_CURRENT_ELEMENT,
            },
          ],
          undo: [
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: l - 1,
              color: matrix[l - 1][col] === 0 ? Color.TH_CURRENT_ELEMENT : Color.TH_CURRENT_ELEMENT,
            },
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: l,
              color: Color.BASE_COLOR_TABLE_HORIZONTAL,
            },
          ],
        });
      }
      while (matrix[r][col] === 1 && r > l) {
        r -= 1;
        steps.push({
          do: [
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: r + 1,
              color: Color.BASE_COLOR_TABLE_HORIZONTAL,
            },
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: r,
              color: matrix[r][col] === 1 ? Color.TH_CURRENT_ELEMENT : Color.TH_CURRENT_ELEMENT,
            },
          ],
          undo: [
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: r + 1,
              color: matrix[r + 1][col] === 1 ? Color.TH_CURRENT_ELEMENT : Color.TH_CURRENT_ELEMENT,
            },
            {
              action: Action.SET_ROW_COLOR,
              id: `table${2 ** (cols - col - 1)}`,
              row: r,
              color: Color.BASE_COLOR_TABLE_HORIZONTAL,
            },
          ],
        });
      }

      // only swap if pointers haven't met
      if (l < r) {
        const t = matrix[l];
        matrix[l] = matrix[r];
        matrix[r] = t;
        // set new values in row l and r
        doActions = [];
        undoActions = [];
        for (let i = 0; i < cols; i += 1) {
          doActions.push({
            action: Action.SET_CELL_VALUE,
            id: `table${2 ** (cols - col - 1)}`,
            row: l,
            col: i,
            value: matrix[l][i],
          });
          doActions.push({
            action: Action.SET_CELL_VALUE,
            id: `table${2 ** (cols - col - 1)}`,
            row: r,
            col: i,
            value: matrix[r][i],
          });
          undoActions.push({
            action: Action.SET_CELL_VALUE,
            id: `table${2 ** (cols - col - 1)}`,
            row: l,
            col: i,
            value: matrix[r][i],
          });
          undoActions.push({
            action: Action.SET_CELL_VALUE,
            id: `table${2 ** (cols - col - 1)}`,
            row: r,
            col: i,
            value: matrix[l][i],
          });
        }
        // paint rows l and r green
        doActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: l,
          color: Color.TH_CURRENT_ELEMENT,
        });
        doActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: r,
          color: Color.TH_CURRENT_ELEMENT,
        });
        // paint rows l and r red
        undoActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: l,
          color: Color.TH_CURRENT_ELEMENT,
        });
        undoActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: r,
          color: Color.TH_CURRENT_ELEMENT,
        });
        steps.push({
          do: doActions,
          undo: undoActions,
        });
      }
    }

    if ((r === left && matrix[left][col] === 1) || (l === right && matrix[right][col] === 0)) {
      // both pointers at start and 1 at start -> only 1s in range
      // both pointers at end and 0 at end -> only 0s in range
      // no divide
      // paint all rows in range TH_ELEMENT_SORTED
      doActions = [];
      undoActions = [];
      for (let i = left; i < right + 1; i += 1) {
        doActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          color: Color.TH_ELEMENT_SORTED,
        });
        undoActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          color: i === l ? Color.TH_CURRENT_ELEMENT : Color.BASE_COLOR_TABLE_HORIZONTAL,
        });
      }
      steps.push({
        do: doActions,
        undo: undoActions,
      });
      bqs(matrix, col + 1, left, right);
    } else {
      // divide
      console.assert(l === r, 'l = %s, r = %s, col = %s', l, r, col);
      // paint rows in first range light green
      doActions = [];
      undoActions = [];
      for (let i = left; i < l; i += 1) {
        doActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          color: Color.TH_ELEMENT_SORTED,
        });
        undoActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          color: i === l ? Color.TH_CURRENT_ELEMENT : Color.BASE_COLOR_TABLE_HORIZONTAL,
        });
      }
      steps.push({
        do: doActions,
        undo: undoActions,
      });
      bqs(matrix, col + 1, left, l - 1);
      // paint rows in second range light green
      doActions = [];
      undoActions = [];
      for (let i = l; i < right + 1; i += 1) {
        doActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          color: Color.TH_ELEMENT_SORTED,
        });
        undoActions.push({
          action: Action.SET_ROW_COLOR,
          id: `table${2 ** (cols - col - 1)}`,
          row: i,
          color: i === l ? Color.TH_CURRENT_ELEMENT : Color.BASE_COLOR_TABLE_HORIZONTAL,
        });
      }
      steps.push({
        do: doActions,
        undo: undoActions,
      });
      bqs(matrix, col + 1, l, right);
    }
  }

  bqs(elements, 0, 0, rows - 1);

  return testing
    ? elements
      .map((e) => e.join(''))
      .map((e) => parseInt(e, 2))
    : steps;
}
