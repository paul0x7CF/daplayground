/* eslint-disable no-param-reassign */

import Action from '../../components/sorts/BarChartActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm QuickSort
 * Based on ADS slides
 *
 * @author Bernhard Frick
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function QuickSort(elements, testing = false) {
  const steps = [];

  const output = Array.from(elements);

  if (output.length < 2) {
    // nothing to sort...
    return testing ? output : steps;
  }

  /**
   * @param {number[]} items
   * @param {number} a
   * @param {number} b
   */
  function swap(items, a, b) {
    const temp = items[a];
    items[a] = items[b];
    items[b] = temp;
  }

  /**
   * @param {number[]} items
   * @param {number} left
   * @param {number} right
   * @return {number}
   */
  function partition(items, left, right) {
    // partition called with elements ... log message
    steps.push({
      do: [
        {
          action: Action.SET_COLORS,
          elements: [],
          text: `partition called with items left: ${left} right: ${right}`,
        },
      ],
      undo: [
        {
          action: Action.SET_COLORS,
          elements: [],
          text: `undo: partition called with items left: ${left} right: ${right}`,
        },
      ],
    });

    // the pivot is a value - NOT an index!
    // ie. the pivot is the value 5, no matter where it is,
    // NOT the value at index 2, no matter what it is.

    // choosing the rightmost element of the given range as the pivot element
    const pivot = items[right];

    steps.push({
      do: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index: right,
              color: Color.QUICKSORT_PIVOT,
            },
          ],
          text: 'highlight pivot, the rightmost element',
        },
      ],
      undo: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index: right,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: 'highlight pivot, the rightmost element',
        },
      ],
    });

    let l = left;
    let r = right - 1; // left of the pivot

    // set labels
    const labelR = l === r ? 'L/R' : 'R';

    steps.push({
      do: [
        {
          action: Action.SET_LABELS,
          elements: [
            {
              index: l,
              text: 'L',
            },
            {
              index: r,
              text: labelR,
            },
          ],
          text: 'Set labels L and R.',
        },
      ],
      undo: [
        {
          action: Action.SET_LABELS,
          elements: [
            {
              index: l,
              text: null,
            },
            {
              index: r,
              text: null,
            },
          ],
          text: 'Undo: Set labels L and R.',
        },
      ],
    });

    for (;;) {
      // sink in from the left
      steps.push({
        do: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: l,
                color: Color.QUICKSORT_COMPARISON,
              },
            ],
            text: `LEFT_POINTER: Comparing element ${items[l]} (index ${l}) with the pivot element ${pivot} (index ${right}).`,
          },
        ],
        undo: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: l,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: `Undo: LEFT_POINTER: Comparing element ${items[l]} (index ${l}) with the pivot element ${pivot} (index ${right}).`,
          },
        ],
      });

      while (items[l] < pivot) {
        const label = l + 1 === r ? 'L/R' : 'L';

        if (l === r) {
          // Moving L by 1 to the right, keeping R label
          steps.push({
            do: [
              {
                action: Action.SET_LABELS,
                elements: [
                  {
                    index: r,
                    text: 'R',
                  },
                  {
                    index: l + 1,
                    text: label,
                  },
                ],
                text: 'Moving L by 1 to the right, keeping R label',
              },
            ],
            undo: [
              {
                action: Action.SET_LABELS,
                elements: [
                  {
                    index: r,
                    text: 'L/R',
                  },
                  {
                    index: l + 1,
                    text: null,
                  },
                ],
                text: 'Undo: Moving L by 1 to the right, keeping R label',
              },
            ],
          });
        } else {
          // move L to the right
          steps.push({
            do: [
              {
                action: Action.SET_LABELS,
                elements: [
                  {
                    index: l,
                    text: null,
                  },
                  {
                    index: l + 1,
                    text: label,
                  },
                ],
                text: 'Moving L by 1 to the right.',
              },
            ],
            undo: [
              {
                action: Action.SET_LABELS,
                elements: [
                  {
                    index: l,
                    text: label,
                  },
                  {
                    index: l + 1,
                    text: null,
                  },
                ],
                text: 'Undo: Moving L by 1 to the right.',
              },
            ],
          });
        }

        // unset yellow
        steps.push({
          do: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
              ],
              text: 'Unset comparison.',
            },
          ],
          undo: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.QUICKSORT_COMPARISON,
                },
              ],
              text: 'Undo: Unset comparison.',
            },
          ],
        });

        l += 1;

        // set l yellow, comparing elements
        steps.push({
          do: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.QUICKSORT_COMPARISON,
                },
              ],
              text: `LEFT_POINTER: Comparing element ${items[l]} (index ${l}) with the pivot element ${pivot} (index ${right}).`,
            },
          ],
          undo: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
              ],
              text: `Undo: LEFT_POINTER: Comparing element ${items[l]} (index ${l}) with the pivot element ${pivot} (index ${right}).`,
            },
          ],
        });
      }
      // still comparing, condition not true anymore -> unset yellow
      steps.push({
        do: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: l,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: 'Unset comparison. Left Greater than Pivot',
          },
        ],
        undo: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: l,
                color: Color.QUICKSORT_COMPARISON,
              },
            ],
            text: 'Undo: Unset comparison. Left Greater than Pivot',
          },
        ],
      });

      // sink in from the right
      steps.push({
        do: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: r,
                color: Color.QUICKSORT_COMPARISON,
              },
            ],
            text: `RIGHT_POINTER: Comparing element ${items[r]} (index ${r}) with the pivot element ${pivot} (index ${right}).`,
          },
        ],
        undo: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: r,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: `Undo: RIGHT_POINTER: Comparing element ${items[r]} (index ${r}) with the pivot element ${pivot} (index ${right}).`,
          },
        ],
      });
      while (items[r] > pivot && r > l) {
        const label = r - 1 === l ? 'L/R' : 'R';

        if (r === l) {
          // Moving R by 1 to the left, keeping L label
          steps.push({
            do: [
              {
                action: Action.SET_LABELS,
                elements: [
                  {
                    index: l,
                    text: 'L',
                  },
                  {
                    index: r - 1,
                    text: label,
                  },
                ],
                text: 'Moving R by 1 to the left, keeping L label',
              },
            ],
            undo: [
              {
                action: Action.SET_LABELS,
                elements: [
                  {
                    index: l,
                    text: 'L/R',
                  },
                  {
                    index: r - 1,
                    text: null,
                  },
                ],
                text: 'Undo: Moving R by 1 to the left, keeping L label',
              },
            ],
          });
        } else {
          // move R to the left
          steps.push({
            do: [
              {
                action: Action.SET_LABELS,
                elements: [
                  {
                    index: r,
                    text: null,
                  },
                  {
                    index: r - 1,
                    text: label,
                  },
                ],
                text: 'Moving R by 1 to the left.',
              },
            ],
            undo: [
              {
              action: Action.SET_LABELS,
              elements: [
                {
                  index: r,
                  text: label,
                },
                {
                  index: r - 1,
                  text: null,
                },
              ],
              text: 'Undo: Moving R by 1 to the left.',
            }
            ],
          });
        }

        // unset yellow
        steps.push({
          do: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: r,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
              ],
              text: 'Unset comparison.',
            },
          ],
          undo: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: r,
                  color: Color.QUICKSORT_COMPARISON,
                },
              ],
              text: 'Undo: Unset comparison.',
            },
          ],
        });

        r -= 1;

        // set r yellow
        steps.push({
          do: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: r,
                  color: Color.QUICKSORT_COMPARISON,
                },
              ],
              text: `RIGHT_POINTER: Comparing element ${items[r]} (index ${r}) with the pivot element ${pivot} (index ${right}).`,
            },
          ],
          undo: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: r,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
              ],
              text: `Undo: RIGHT_POINTER: Comparing element ${items[r]} (index ${r}) with the pivot element ${pivot} (index ${right}).`,
            },
          ],
        });
      }
      // still comparing, condition not true anymore -> unset yellow
      steps.push({
        do: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: r,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: 'Unset comparison. Right smaller than pivot',
          },
        ],
        undo: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: r,
                color: Color.QUICKSORT_COMPARISON,
              },
            ],
            text: 'Undo: Unset comparison. Right smaller than pivot',
          },
        ],
      });

      // pointers have not yet passed each other
      if (l < r) {
        // exchange

        // paint values to exchange
        steps.push({
          do: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.QUICKSORT_SWAP,
                },
                {
                  index: r,
                  color: Color.QUICKSORT_SWAP,
                },
              ],
              text: 'The elements L and R will be exchanged.',
            },
          ],
          undo: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
                {
                  index: r,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
              ],
              text: 'Undo: The elements L and R will be exchanged.',
            },
          ],
        });

        // exchange
        steps.push({
          do: [
            {
              action: Action.EXCHANGE,
              elements: [l, r],
              text: `Exchanging L (element ${l}) and R (element ${r}).`,
            },
          ],
          undo: [
            {
              action: Action.EXCHANGE,
              elements: [l, r],
              text: `UNDO: Exchanging L (element ${l}) and R (element ${r}).`,
            },
          ],
        });


        // clear paint of exchanged nodes
        steps.push({
          do: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
                {
                  index: r,
                  color: Color.BASE_COLOR_BAR_CHART,
                },
              ],
              text: 'Unsetting the exchange colors.',
            },
          ],
          undo: [
            {
              action: Action.SET_COLORS,
              elements: [
                {
                  index: l,
                  color: Color.QUICKSORT_SWAP,
                },
                {
                  index: r,
                  color: Color.QUICKSORT_SWAP,
                },
              ],
              text: 'Undo: Unsetting the exchange colors.',
            },
          ],
        });

        // swap items at position l and r
        swap(items, l, r);
      } else {
        // Log message (debugging)
        steps.push({
          do: [
            {
              action: Action.SET_COLORS,
              elements: [],
              text: `breaking because l < r is not true anymore. l: ${l}, r: ${r}, pivot: '${pivot}`,
            },
          ],
          undo: [
            {
              action: Action.SET_COLORS,
              elements: [],
              text: `Undo: breaking because l < r is not true anymore. l: ${l}, r: ${r}, pivot: '${pivot}`,
            },
          ],
        });
        break;
      }
    }

    // swap the pivot element into the right place

    // exchange

    // paint values to exchange
    steps.push({
      do: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index: l,
              color: Color.QUICKSORT_SWAP,
            },
            {
              index: right,
              color: Color.QUICKSORT_SWAP,
            },
          ],
          text: 'The elements L and Right will be exchanged.',
        },
      ],
      undo: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index: l,
              color: Color.BASE_COLOR_BAR_CHART,
            },
            {
              index: right,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: 'Undo: The elements L and Right will be exchanged.',
        },
      ],
    });

    // exchange
    steps.push({
      do: [
        {
          action: Action.EXCHANGE,
          elements: [l, right],
          text: `Exchanging L (element ${l}) and Right (element ${right}).`,
        },
      ],
      undo: [
        {
          action: Action.EXCHANGE,
          elements: [right, l],
          text: `Undo: Exchanging L (element ${l}) and R (element ${r}).`,
        },
      ],
    });

    // clear paint of exchanged nodes
    steps.push({
      do: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index: l,
              color: Color.BASE_COLOR_BAR_CHART,
            },
            {
              index: right,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: 'Unsetting the exchange colors.',
        },
      ],
      undo: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index: l,
              color: Color.QUICKSORT_SWAP,
            },
            {
              index: right,
              color: Color.QUICKSORT_SWAP,
            },
          ],
          text: 'Undo: Unsetting the exchange colors.',
        },
      ],
    });

    // clear L and R labels
    steps.push({
      do: [
        {
          action: Action.SET_LABELS,
          elements: [
            {
              index: r,
              text: null,
            },
            {
              index: l,
              text: null,
            },
          ],
          text: 'Removing L and R labels ',
        },
      ],
      undo: {
        action: Action.SET_LABELS,
        elements: [
          {
            index: r,
            text: 'R',
          },
          {
            index: l,
            text: 'L',
          },
        ],
        text: 'Undo: Removing L and R labels',
      },
    });

    // swap items at position l and right
    swap(items, l, right);

    // the element at position l is now at its correct place
    //  -> all items to the left are smaller
    //  -> all items to the right are bigger
    return l;
  }

  /**
   * QuickSort is a recursive sorting algorithm.
   * It partitions the input list into two parts, with the
   * pivot being the middle value that is at its correct place.
   * Then, the two resulting lists to the left and the right are
   * sorted by again applying QuickSort.
   *
   * @param {number[]} items
   * @param {number} left
   * @param {number} right
   */
  function quicksort(items, left, right) {
    if (left >= right) {
      // range has one or less elements
      // -> no need to sort
      steps.push({
        do: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: left,
                color: Color.QUICKSORT_ELEMENT_SORTED,
              },
            ],
            text: `No need to sort: Element ${items[left]} (index ${left}) is now at it's sorted position.`,
          },
        ],
        undo: [
          {
            action: Action.SET_COLORS,
            elements: [
              {
                index: left,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: `Undo: No need to sort: Element ${items[left]} (index ${left}) is now at it's sorted position.`,
          },
        ],
      });

      return;
    }

    // element items[index] is now at its correct (sorted) position.
    const index = partition(items, left, right);
    steps.push({
      do: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index,
              color: Color.QUICKSORT_ELEMENT_SORTED,
            },
          ],
          text: `Element ${items[index]} (index ${index}) is now at it's sorted position.`,
        },
      ],
      undo: [
        {
          action: Action.SET_COLORS,
          elements: [
            {
              index,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: `Undo: Element ${items[index]} (index ${index}) is now at it's sorted position.`,
        },
      ],
    });

    quicksort(items, left, index - 1);
    quicksort(items, index + 1, right);
  }

  quicksort(output, 0, output.length - 1);

  // log message (debugging)
  steps.push({
    do: [
      {
        action: Action.SET_COLORS,
        elements: [],
        text: 'quicksort finished',
      },
    ],
    undo: [
      {
        action: Action.SET_COLORS,
        elements: [],
        text: 'undo: quicksort finished',
      },
    ],
  });

  return testing ? output : steps;
}
