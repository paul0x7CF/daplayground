import BubbleSort from './BubbleSort';
import getRandomInt from '../../utils/getRandomInt';
import isSorted from '../../utils/isSorted';

describe('BubbleSort.js', () => {
  it('should sort the given random input', () => {
    const numTests = 1000;
    const inputSize = 50;
    for (let i = 0; i < numTests; i += 1) {
      const randomNumbers = new Array(inputSize)
        .fill(null)
        .map((_) => getRandomInt(0, 100));
      const sortedOutput = BubbleSort(randomNumbers, true);
      expect(isSorted(sortedOutput)).toBe(true);
    }
  });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], [
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#FFEB3B' },
              { index: 4, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#2196F3' },
              { index: 4, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#4CAF50' },
              { index: 4, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#FFEB3B' },
              { index: 4, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#2196F3' },
              { index: 4, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#4CAF50' },
              { index: 4, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFC107' }],
            text: 'Element 4 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Element 4 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFC107' }],
            text: 'Element 3 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Element 3 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFC107' }],
            text: 'Element 2 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Element 2 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFC107' }],
            text: 'Element 1 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Element 1 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFC107' }],
            text: 'Element 0 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Element 0 is sorted.',
          },
        ],
      },
    ]],
    // Reverse input
    [[5, 4, 3, 2, 1], [
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#F44336' },
              { index: 1, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [0, 1], text: '' }],
        undo: [{ action: 'exchange', elements: [0, 1], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#F44336' },
              { index: 2, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [1, 2], text: '' }],
        undo: [{ action: 'exchange', elements: [1, 2], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#F44336' },
              { index: 3, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [2, 3], text: '' }],
        undo: [{ action: 'exchange', elements: [2, 3], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#FFEB3B' },
              { index: 4, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#2196F3' },
              { index: 4, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#F44336' },
              { index: 4, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#FFEB3B' },
              { index: 4, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [3, 4], text: '' }],
        undo: [{ action: 'exchange', elements: [3, 4], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#2196F3' },
              { index: 4, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#4CAF50' },
              { index: 4, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFC107' }],
            text: 'Element 4 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Element 4 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#F44336' },
              { index: 1, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [0, 1], text: '' }],
        undo: [{ action: 'exchange', elements: [0, 1], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#F44336' },
              { index: 2, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [1, 2], text: '' }],
        undo: [{ action: 'exchange', elements: [1, 2], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#F44336' },
              { index: 3, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [2, 3], text: '' }],
        undo: [{ action: 'exchange', elements: [2, 3], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFC107' }],
            text: 'Element 3 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Element 3 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#F44336' },
              { index: 1, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [0, 1], text: '' }],
        undo: [{ action: 'exchange', elements: [0, 1], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#F44336' },
              { index: 2, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [1, 2], text: '' }],
        undo: [{ action: 'exchange', elements: [1, 2], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFC107' }],
            text: 'Element 2 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Element 2 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#F44336' },
              { index: 1, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [0, 1], text: '' }],
        undo: [{ action: 'exchange', elements: [0, 1], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFC107' }],
            text: 'Element 1 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Element 1 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFC107' }],
            text: 'Element 0 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Element 0 is sorted.',
          },
        ],
      },
    ]],
    // Only one number
    [[4], [
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFC107' }],
            text: 'Element 0 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Element 0 is sorted.',
          },
        ],
      },
    ]],
    // Repeating numbers
    [[1, 2, 1], [
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#F44336' },
              { index: 2, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [1, 2], text: '' }],
        undo: [{ action: 'exchange', elements: [1, 2], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFC107' }],
            text: 'Element 2 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Element 2 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFC107' }],
            text: 'Element 1 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Element 1 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFC107' }],
            text: 'Element 0 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Element 0 is sorted.',
          },
        ],
      },
    ]],
    // Random input
    [[3, 8, 2, 7, 1], [
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#F44336' },
              { index: 2, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [1, 2], text: '' }],
        undo: [{ action: 'exchange', elements: [1, 2], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#F44336' },
              { index: 3, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [2, 3], text: '' }],
        undo: [{ action: 'exchange', elements: [2, 3], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#FFEB3B' },
              { index: 4, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#2196F3' },
              { index: 4, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#F44336' },
              { index: 4, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#FFEB3B' },
              { index: 4, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [3, 4], text: '' }],
        undo: [{ action: 'exchange', elements: [3, 4], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#2196F3' },
              { index: 4, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 3, color: '#4CAF50' },
              { index: 4, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#FFC107' }],
            text: 'Element 4 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 4, color: '#2196F3' }],
            text: 'Element 4 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#F44336' },
              { index: 1, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [0, 1], text: '' }],
        undo: [{ action: 'exchange', elements: [0, 1], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#F44336' },
              { index: 3, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#FFEB3B' },
              { index: 3, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [2, 3], text: '' }],
        undo: [{ action: 'exchange', elements: [2, 3], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#2196F3' },
              { index: 3, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 2, color: '#4CAF50' },
              { index: 3, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#FFC107' }],
            text: 'Element 3 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 3, color: '#2196F3' }],
            text: 'Element 3 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#F44336' },
              { index: 2, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#FFEB3B' },
              { index: 2, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [1, 2], text: '' }],
        undo: [{ action: 'exchange', elements: [1, 2], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#2196F3' },
              { index: 2, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 1, color: '#4CAF50' },
              { index: 2, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#FFC107' }],
            text: 'Element 2 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 2, color: '#2196F3' }],
            text: 'Element 2 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#F44336' },
              { index: 1, color: '#F44336' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#FFEB3B' },
              { index: 1, color: '#FFEB3B' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [{ action: 'exchange', elements: [0, 1], text: '' }],
        undo: [{ action: 'exchange', elements: [0, 1], text: '' }],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#2196F3' },
              { index: 1, color: '#2196F3' },
            ],
            text: '',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [
              { index: 0, color: '#4CAF50' },
              { index: 1, color: '#4CAF50' },
            ],
            text: '',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#FFC107' }],
            text: 'Element 1 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 1, color: '#2196F3' }],
            text: 'Element 1 is sorted.',
          },
        ],
      },
      {
        do: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#FFC107' }],
            text: 'Element 0 is sorted.',
          },
        ],
        undo: [
          {
            action: 'setColors',
            elements: [{ index: 0, color: '#2196F3' }],
            text: 'Element 0 is sorted.',
          },
        ],
      },
    ]],
  ])('should return the expected steps, input %#: %p', (input, expectedOutput) => {
    const actualOutput = BubbleSort(input);
    expect(actualOutput.length).toEqual(expectedOutput.length);
    expectedOutput.forEach((element, index) => {
      expect(actualOutput[index]).toEqual(element);
    });
  });
});
