import BinaryQuickSort from './BinaryQuickSort';
import getRandomBit from '../../utils/getRandomBit';
import isSorted from '../../utils/isSorted';

describe('BinaryQuickSort.js', () => {
  it('should sort the random input correctly', () => {
    const numTests = 10000;

    for (let i = 0; i < numTests; i += 1) {
      // generate an input array that has between 1 and 20 values with each between 1 and 10 bits
      const inputArray = [];
      const numberOfValues = Math.floor(Math.random() * 20) + 1;
      for (let j = 0; j < numberOfValues; j += 1) {
        const numberOfBits = Math.floor(Math.random() * 10) + 1;
        const bits = [];
        for (let k = 0; k < numberOfBits; k += 1) {
          bits.push(getRandomBit());
        }
        inputArray.push(bits.join(''));
      }
      // sort
      const actualOutput = BinaryQuickSort(inputArray.map(Number), true);
      // check if sorted
      expect(isSorted(actualOutput)).toBe(true);
    }
  });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 10, 11, 100, 101], [
      { do: [{ action: 'clear' }], undo: [] },
      {
        do: [
          {
            action: 'createTable',
            id: 'table4',
            title: 'Bit 2^2 = 4',
            rows: 5,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table2',
            title: 'Bit 2^1 = 2',
            rows: 5,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table1',
            title: 'Bit 2^0 = 1',
            rows: 5,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'table0' },
          { action: 'deleteTable', id: 'table1' },
          { action: 'deleteTable', id: 'table2' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table4', col: 0, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table4', col: 0, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#4CAF50',
          },
        ],
      },
    ]],
    // Reverse input
    [[101, 100, 11, 10, 1], [
      { do: [{ action: 'clear' }], undo: [] },
      {
        do: [
          {
            action: 'createTable',
            id: 'table4',
            title: 'Bit 2^2 = 4',
            rows: 5,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table2',
            title: 'Bit 2^1 = 2',
            rows: 5,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table1',
            title: 'Bit 2^0 = 1',
            rows: 5,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'table0' },
          { action: 'deleteTable', id: 'table1' },
          { action: 'deleteTable', id: 'table2' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table4', col: 0, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table4', col: 0, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: 1,
          },
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: 1,
          },
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#F44336',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#F44336',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 2,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#4CAF50',
          },
        ],
      },
    ]],
    // Only one number
    [[100], [
      { do: [{ action: 'clear' }], undo: [] },
      {
        do: [
          {
            action: 'createTable',
            id: 'table4',
            title: 'Bit 2^2 = 4',
            rows: 1,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20],
          },
          {
            action: 'createTable',
            id: 'table2',
            title: 'Bit 2^1 = 2',
            rows: 1,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20],
          },
          {
            action: 'createTable',
            id: 'table1',
            title: 'Bit 2^0 = 1',
            rows: 1,
            cols: 3,
            colWidth: [23, 23, 23],
            rowHeight: [20],
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'table0' },
          { action: 'deleteTable', id: 'table1' },
          { action: 'deleteTable', id: 'table2' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table4', col: 0, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table4', col: 0, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
      },
    ]],
    // Repeating numbers
    [[1, 10, 1], [
      { do: [{ action: 'clear' }], undo: [] },
      {
        do: [
          {
            action: 'createTable',
            id: 'table2',
            title: 'Bit 2^1 = 2',
            rows: 3,
            cols: 2,
            colWidth: [23, 23],
            rowHeight: [20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table1',
            title: 'Bit 2^0 = 1',
            rows: 3,
            cols: 2,
            colWidth: [23, 23],
            rowHeight: [20, 20, 20],
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'table0' },
          { action: 'deleteTable', id: 'table1' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 0, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 0, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: 0,
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: 1,
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#F44336',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
        ],
      },
    ]],
    // Random input
    [[11, 1000, 10, 111, 1], [
      { do: [{ action: 'clear' }], undo: [] },
      {
        do: [
          {
            action: 'createTable',
            id: 'table8',
            title: 'Bit 2^3 = 8',
            rows: 5,
            cols: 4,
            colWidth: [23, 23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table4',
            title: 'Bit 2^2 = 4',
            rows: 5,
            cols: 4,
            colWidth: [23, 23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table2',
            title: 'Bit 2^1 = 2',
            rows: 5,
            cols: 4,
            colWidth: [23, 23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
          {
            action: 'createTable',
            id: 'table1',
            title: 'Bit 2^0 = 1',
            rows: 5,
            cols: 4,
            colWidth: [23, 23, 23, 23],
            rowHeight: [20, 20, 20, 20, 20],
          },
        ],
        undo: [
          { action: 'deleteTable', id: 'table0' },
          { action: 'deleteTable', id: 'table1' },
          { action: 'deleteTable', id: 'table2' },
          { action: 'deleteTable', id: 'table3' },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 3,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 3,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 3,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 0,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 2,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 3,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table8', col: 0, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table8', col: 0, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table8', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table8', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table8', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table8', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 3,
            value: 0,
          },
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 1,
            col: 3,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table8',
            row: 4,
            col: 3,
            value: 1,
          },
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#F44336',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table8', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table8', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 3, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table8', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table8', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table8', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table8', row: 3, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table8', row: 0, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table8', row: 2, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table8', row: 3, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table8', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table8', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 3,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 3,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 0,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 1,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 2,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 3,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table4', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table4', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 3,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 2,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 3,
            value: 1,
          },
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 0,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 1,
            col: 3,
            value: 1,
          },
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#F44336',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 3,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 0,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 0, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#8BC34A',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 3,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 3,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 3,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 3,
            value: 1,
          },
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 1,
            col: 3,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 2,
            col: 3,
            value: 0,
          },
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#F44336',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 1, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 2, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 3, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 3,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 3,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 3, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 0,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 1,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 2,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 3,
            value: 1,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 3,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#F44336',
          },
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#4CAF50',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 3, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table8', row: 4, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 3,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table4',
            row: 4,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table4', col: 1, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table4', col: 1, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table4', row: 4, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 3,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table2',
            row: 4,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table2', col: 2, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table2', col: 2, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table2', row: 4, color: '#4CAF50',
          },
        ],
      },
      {
        do: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 0,
            value: 1,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 1,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 2,
            value: 0,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 3,
            value: 0,
          },
        ],
        undo: [
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 0,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 1,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 2,
            value: null,
          },
          {
            action: 'setCellValue',
            id: 'table1',
            row: 4,
            col: 3,
            value: null,
          },
        ],
      },
      {
        do: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 3,
        }],
        undo: [{
          action: 'setColumnStroke', id: 'table1', col: 3, stroke: 1,
        }],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#4CAF50',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#F44336',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#ffffff',
          },
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#ffffff',
          },
        ],
      },
      {
        do: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#8BC34A',
          },
        ],
        undo: [
          {
            action: 'setRowColor', id: 'table1', row: 4, color: '#4CAF50',
          },
        ],
      },
    ]],
  ])('should return the expected steps, input %#: %p', (input, expectedOutput) => {
    const actualOutput = BinaryQuickSort(input);
    expect(actualOutput.length).toEqual(expectedOutput.length);
    expectedOutput.forEach((element, index) => {
      expect(actualOutput[index]).toEqual(element);
    });
  });
});
