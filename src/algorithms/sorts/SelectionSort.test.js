import SelectionSort from './SelectionSort';
import getRandomInt from '../../utils/getRandomInt';
import isSorted from '../../utils/isSorted';

describe('SelectionSort.js', () => {
  it('should sort the given random input', () => {
    const numTests = 1000;
    const inputSize = 50;
    for (let i = 0; i < numTests; i += 1) {
      const randomNumbers = new Array(inputSize)
        .fill(null)
        .map((_) => getRandomInt(0, 100));
      const sortedOutput = SelectionSort(randomNumbers, true);
      expect(isSorted(sortedOutput)).toBe(true);
    }
  });
  it('should sort an empty array', () => {
    const array = [];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([]);
  });

  it('should sort an array with a single element', () => {
    const array = [5];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([5]);
  });

  it('should sort an array with duplicate elements', () => {
    const array = [3, 1, 4, 1, 5, 2, 2];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([1, 1, 2, 2, 3, 4, 5]);
  });

  it('should sort a large array with descending order', () => {
    const array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  });

  it('should not modify the original array', () => {
    const array = [5, 3, 2, 4, 1];
    const sortedArray = SelectionSort(array, true);
    expect(array).toEqual([5, 3, 2, 4, 1]);
  });

  it('should sort an array with negative numbers', () => {
    const array = [-5, -3, -2, -4, -1];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([-5, -4, -3, -2, -1]);
  });
  it('should sort an already sorted array', () => {
    const array = [1, 2, 3, 4, 5];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([1, 2, 3, 4, 5]);
  });

  it('should sort an array with all identical elements', () => {
    const array = [3, 3, 3, 3, 3];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([3, 3, 3, 3, 3]);
  });

  it('should sort an array with negative and positive numbers', () => {
    const array = [-5, 3, -2, 4, -1];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([-5, -2, -1, 3, 4]);
  });

  it('should sort an array with decimal numbers', () => {
    const array = [2.5, 1.1, 3.7, 2.2, 1.8];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual([1.1, 1.8, 2.2, 2.5, 3.7]);
  });

  it('should sort an array with strings', () => {
    const array = ['apple', 'banana', 'cherry', 'date', 'elderberry'];
    const sortedArray = SelectionSort(array, true);
    expect(sortedArray).toEqual(['apple', 'banana', 'cherry', 'date', 'elderberry']);
  });
  // test.each([
  //   // No input
  //   [[], []],
  //   // Sorted input
  //   [[1, 2, 3, 4, 5], [
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#E91E63' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FFEB3B' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Element 0 is smaller than 1, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FFEB3B' }],
  //           text: 'Element 0 is smaller than 1, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Element 0 is smaller than 2, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Element 0 is smaller than 2, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 0 is smaller than 3, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Element 0 is smaller than 3, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 0 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 0 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#FF9800' }],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#E91E63' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: 'min' }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: null }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Element 1 is smaller than 2, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Element 1 is smaller than 2, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 1 is smaller than 3, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Element 1 is smaller than 3, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 1 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 1 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FF9800' }],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#E91E63' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 2 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 2 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FF9800' }],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#E91E63' }],
  //           text: 'Selecting 3 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Selecting 3 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }],
  //           text: 'Setting 3 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }],
  //           text: 'Setting 3 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FF9800' }],
  //           text: 'Element 3 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 3 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#E91E63' }],
  //           text: 'Selecting 4 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Selecting 4 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Setting 4 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Setting 4 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FF9800' }],
  //           text: 'Element 4 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 4 is now sorted.',
  //         },
  //       ],
  //     },
  //   ]],
  //   // Reverse input
  //   [[5, 4, 3, 2, 1], [
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#E91E63' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FFEB3B' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: 'min' }, { index: 0, text: null }],
  //           text: 'Setting 1 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: null }, { index: 0, text: 'min' }],
  //           text: 'Setting 1 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#2196F3' },
  //             { index: 1, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#FFEB3B' },
  //             { index: 1, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }, { index: 1, text: null }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }, { index: 1, text: 'min' }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#2196F3' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#FFEB3B' },
  //             { index: 2, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }, { index: 2, text: null }],
  //           text: 'Setting 3 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }, { index: 2, text: 'min' }],
  //           text: 'Setting 3 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 3, color: '#2196F3' },
  //             { index: 3, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 3, color: '#FFEB3B' },
  //             { index: 3, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }, { index: 3, text: null }],
  //           text: 'Setting 4 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }, { index: 3, text: 'min' }],
  //           text: 'Setting 4 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 4, color: '#2196F3' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 4, color: '#FFEB3B' },
  //             { index: 4, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#9C27B0' },
  //             { index: 4, color: '#9C27B0' },
  //           ],
  //           text: 'Elements 0 and 4 will be exchanged.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#2196F3' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Elements 0 and 4 will be exchanged.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'exchange',
  //           elements: [0, 4],
  //           text: 'Moving minimum element 4 to 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'exchange',
  //           elements: [0, 4],
  //           text: 'Moving minimum element 4 to 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#FF9800' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#9C27B0' },
  //             { index: 4, color: '#9C27B0' },
  //           ],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#E91E63' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: 'min' }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: null }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }, { index: 1, text: null }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }, { index: 1, text: 'min' }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#2196F3' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#FFEB3B' },
  //             { index: 2, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }, { index: 2, text: null }],
  //           text: 'Setting 3 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }, { index: 2, text: 'min' }],
  //           text: 'Setting 3 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 3, color: '#2196F3' },
  //             { index: 3, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 3, color: '#FFEB3B' },
  //             { index: 3, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#9C27B0' },
  //             { index: 3, color: '#9C27B0' },
  //           ],
  //           text: 'Elements 1 and 3 will be exchanged.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#2196F3' },
  //             { index: 3, color: '#2196F3' },
  //           ],
  //           text: 'Elements 1 and 3 will be exchanged.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'exchange',
  //           elements: [1, 3],
  //           text: 'Moving minimum element 3 to 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'exchange',
  //           elements: [1, 3],
  //           text: 'Moving minimum element 3 to 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#FF9800' },
  //             { index: 3, color: '#2196F3' },
  //           ],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#9C27B0' },
  //             { index: 3, color: '#9C27B0' },
  //           ],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#E91E63' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 2 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 2 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FF9800' }],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#E91E63' }],
  //           text: 'Selecting 3 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Selecting 3 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }],
  //           text: 'Setting 3 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }],
  //           text: 'Setting 3 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FF9800' }],
  //           text: 'Element 3 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 3 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#E91E63' }],
  //           text: 'Selecting 4 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Selecting 4 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Setting 4 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Setting 4 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FF9800' }],
  //           text: 'Element 4 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 4 is now sorted.',
  //         },
  //       ],
  //     },
  //   ]],
  //   // Only one number
  //   [[4], [
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#E91E63' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#FF9800' }],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //     },
  //   ]],
  //   // Repeating numbers
  //   [[1, 2, 1], [
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#E91E63' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FFEB3B' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Element 0 is smaller than 1, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FFEB3B' }],
  //           text: 'Element 0 is smaller than 1, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Element 0 is smaller than 2, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Element 0 is smaller than 2, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#FF9800' }],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#E91E63' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: 'min' }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: null }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }, { index: 1, text: null }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }, { index: 1, text: 'min' }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#2196F3' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#FFEB3B' },
  //             { index: 2, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#9C27B0' },
  //             { index: 2, color: '#9C27B0' },
  //           ],
  //           text: 'Elements 1 and 2 will be exchanged.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#2196F3' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Elements 1 and 2 will be exchanged.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'exchange',
  //           elements: [1, 2],
  //           text: 'Moving minimum element 2 to 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'exchange',
  //           elements: [1, 2],
  //           text: 'Moving minimum element 2 to 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#FF9800' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#9C27B0' },
  //             { index: 2, color: '#9C27B0' },
  //           ],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#E91E63' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FF9800' }],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //     },
  //   ]],
  //   // Random input
  //   [[3, 8, 2, 7, 1], [
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#E91E63' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 0, color: '#2196F3' }],
  //           text: 'Selecting 0 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: 'min' }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 0, text: null }],
  //           text: 'Setting 0 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FFEB3B' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Comparing elements 1 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Element 0 is smaller than 1, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#FFEB3B' }],
  //           text: 'Element 0 is smaller than 1, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }, { index: 0, text: null }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }, { index: 0, text: 'min' }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#2196F3' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#FFEB3B' },
  //             { index: 2, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }, { index: 2, text: null }],
  //           text: 'Setting 4 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }, { index: 2, text: 'min' }],
  //           text: 'Setting 4 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 4, color: '#2196F3' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 4, color: '#FFEB3B' },
  //             { index: 4, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#9C27B0' },
  //             { index: 4, color: '#9C27B0' },
  //           ],
  //           text: 'Elements 0 and 4 will be exchanged.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#2196F3' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Elements 0 and 4 will be exchanged.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'exchange',
  //           elements: [0, 4],
  //           text: 'Moving minimum element 4 to 0.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'exchange',
  //           elements: [0, 4],
  //           text: 'Moving minimum element 4 to 0.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#FF9800' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 0, color: '#9C27B0' },
  //             { index: 4, color: '#9C27B0' },
  //           ],
  //           text: 'Element 0 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#E91E63' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 1, color: '#2196F3' }],
  //           text: 'Selecting 1 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: 'min' }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 1, text: null }],
  //           text: 'Setting 1 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#FFEB3B' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Comparing elements 2 and 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }, { index: 1, text: null }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }, { index: 1, text: 'min' }],
  //           text: 'Setting 2 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#2196F3' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#FFEB3B' },
  //             { index: 2, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Element 2 is smaller than 3, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 2 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 2 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#9C27B0' },
  //             { index: 2, color: '#9C27B0' },
  //           ],
  //           text: 'Elements 1 and 2 will be exchanged.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#2196F3' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Elements 1 and 2 will be exchanged.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'exchange',
  //           elements: [1, 2],
  //           text: 'Moving minimum element 2 to 1.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'exchange',
  //           elements: [1, 2],
  //           text: 'Moving minimum element 2 to 1.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#FF9800' },
  //             { index: 2, color: '#2196F3' },
  //           ],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 1, color: '#9C27B0' },
  //             { index: 2, color: '#9C27B0' },
  //           ],
  //           text: 'Element 1 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#E91E63' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 2, color: '#2196F3' }],
  //           text: 'Selecting 2 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: 'min' }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 2, text: null }],
  //           text: 'Setting 2 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FFEB3B' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Comparing elements 3 and 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }, { index: 2, text: null }],
  //           text: 'Setting 3 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }, { index: 2, text: 'min' }],
  //           text: 'Setting 3 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 3, color: '#2196F3' },
  //             { index: 3, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 3, color: '#FFEB3B' },
  //             { index: 3, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }, { index: 3, text: null }],
  //           text: 'Setting 4 as the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }, { index: 3, text: 'min' }],
  //           text: 'Setting 4 as the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 4, color: '#2196F3' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 4, color: '#FFEB3B' },
  //             { index: 4, color: '#E91E63' },
  //           ],
  //           text: 'Finished setting the new minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#9C27B0' },
  //             { index: 4, color: '#9C27B0' },
  //           ],
  //           text: 'Elements 2 and 4 will be exchanged.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#2196F3' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Elements 2 and 4 will be exchanged.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'exchange',
  //           elements: [2, 4],
  //           text: 'Moving minimum element 4 to 2.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'exchange',
  //           elements: [2, 4],
  //           text: 'Moving minimum element 4 to 2.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#FF9800' },
  //             { index: 4, color: '#2196F3' },
  //           ],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [
  //             { index: 2, color: '#9C27B0' },
  //             { index: 4, color: '#9C27B0' },
  //           ],
  //           text: 'Element 2 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#E91E63' }],
  //           text: 'Selecting 3 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Selecting 3 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }],
  //           text: 'Setting 3 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }],
  //           text: 'Setting 3 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Comparing elements 4 and 3.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FFEB3B' }],
  //           text: 'Element 3 is smaller than 4, continuing search.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 3, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#FF9800' }],
  //           text: 'Element 3 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 3, color: '#2196F3' }],
  //           text: 'Element 3 is now sorted.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#E91E63' }],
  //           text: 'Selecting 4 as insert position.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Selecting 4 as insert position.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Setting 4 as the current minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Setting 4 as the current minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: null }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setLabels',
  //           elements: [{ index: 4, text: 'min' }],
  //           text: 'Clear minimum.',
  //         },
  //       ],
  //     },
  //     {
  //       do: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#FF9800' }],
  //           text: 'Element 4 is now sorted.',
  //         },
  //       ],
  //       undo: [
  //         {
  //           action: 'setColors',
  //           elements: [{ index: 4, color: '#2196F3' }],
  //           text: 'Element 4 is now sorted.',
  //         },
  //       ],
  //     },
  //   ]],
  // ])('should return the expected steps, input %#: %p', (input, expectedOutput) => {
  //   const actualOutput = SelectionSort(input);
  //   expect(actualOutput.length).toEqual(expectedOutput.length);
  //   expectedOutput.forEach((element, index) => {
  //     expect(actualOutput[index]).toEqual(element);
  //   });
  // });
});
