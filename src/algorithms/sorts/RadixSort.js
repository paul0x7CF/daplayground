import getMagnitude from '../../utils/getMagnitude';
import getNthDigit from '../../utils/getNthDigit';
import Action from '../../components/sorts/TablesHorizontalActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm RadixSort (LSD) a.k.a. Straight Radix Sort
 *
 * @author https://alligator.io/js/radix-sort/
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function RadixSort(elements, testing = false) {
  const steps = [];

  let doActions;
  let undoActions;

  const radix = 10;

  let output = Array
    .from(elements)
    .map((e) => Math.round(e)) // only integers
    .map((e) => Math.abs(e)); // only positive values

  if (output.length === 0) {
    // nothing to sort
    return testing ? output : steps;
  }

  steps.push({
    do: [{
      action: Action.CLEAR,
    }],
    undo: [],
  });

  const longestNumberLength = getMagnitude(Math.max(...output), true) + 1;

  // create tables
  steps.push({
    do: [
      {
        action: Action.CREATE_TABLE,
        id: 'inputTable',
        title: 'Input',
        rows: output.length,
        cols: longestNumberLength,
      },
      {
        action: Action.CREATE_TABLE,
        id: 'bucketsTable',
        title: 'Radix Buckets',
        rows: radix,
        cols: 2,
      },
      {
        action: Action.CREATE_TABLE,
        id: 'outputTable',
        title: 'Output',
        rows: output.length,
        cols: longestNumberLength,
      },
    ],
    undo: [
      {
        action: Action.DELETE_TABLE,
        id: 'inputTable',
      },
      {
        action: Action.DELETE_TABLE,
        id: 'bucketsTable',
      },
      {
        action: Action.DELETE_TABLE,
        id: 'outputTable',
      },
    ],
  });

  // fill inputTable with input
  doActions = [];
  undoActions = [];
  for (let i = 0; i < output.length; i += 1) {
    for (let j = 0; j < longestNumberLength; j += 1) {
      doActions.push({
        action: Action.SET_CELL_VALUE,
        id: 'inputTable',
        row: i,
        col: j,
        value: getNthDigit(output[i], longestNumberLength - j),
      });
      undoActions.push({
        action: Action.SET_CELL_VALUE,
        id: 'inputTable',
        row: i,
        col: j,
        value: null,
      });
    }
  }
  steps.push({
    do: doActions,
    undo: undoActions,
  });

  // fill bucketsTable with bucket numbers
  doActions = [];
  undoActions = [];
  for (let i = 0; i < radix; i += 1) {
    doActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'bucketsTable',
      row: i,
      col: 0,
      value: `Bucket ${i}`,
    });
    undoActions.push({
      action: Action.SET_CELL_VALUE,
      id: 'bucketsTable',
      row: i,
      col: 0,
      value: null,
    });
  }
  steps.push({
    do: doActions,
    undo: undoActions,
  });

  for (let i = 0; i < longestNumberLength; i += 1) {
    // highlight column i in input array
    steps.push({
      do: [{
        action: Action.SET_COLUMN_COLOR,
        id: 'inputTable',
        col: longestNumberLength - i - 1,
        color: Color.TH_CURRENT_COLUMN_SELECTED,
      }],
      undo: [{
        action: Action.SET_COLUMN_COLOR,
        id: 'inputTable',
        col: longestNumberLength - i - 1,
        color: Color.BASE_COLOR_TABLE_HORIZONTAL,
      }],
    });

    // filling with an empty array leads to all 10 elements
    // in the outer array referencing the same array.
    // inserting into one sub-array makes the value show up in each sub-array.
    // This is why after fill a map step is needed to
    // overwrite all elements with an individual array.
    const buckets = new Array(radix).fill(null).map((_) => []);

    // for each value in inputTable...
    for (let j = 0; j < output.length; j += 1) {
      // highlight value j in inputTable
      steps.push({
        do: [{
          action: Action.SET_ROW_COLOR,
          id: 'inputTable',
          row: j,
          color: Color.TH_CURRENT_ELEMENT,
        }],
        undo: [
          {
            action: Action.SET_ROW_COLOR,
            id: 'inputTable',
            row: j,
            color: Color.BASE_COLOR_TABLE_HORIZONTAL,
          },
          {
            action: Action.SET_COLUMN_COLOR,
            id: 'inputTable',
            col: longestNumberLength - i - 1,
            color: Color.TH_CURRENT_COLUMN_SELECTED,
          },
        ],
      });

      // find correct bucket for value j
      const bucket = getNthDigit(output[j], i + 1);

      // highlight bucket
      steps.push({
        do: [{
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: bucket,
          color: Color.TH_CURRENT_COLUMN_SELECTED,
        }],
        undo: [{
          action: Action.SET_ROW_COLOR,
          id: 'bucketsTable',
          row: bucket,
          color: Color.BASE_COLOR_TABLE_HORIZONTAL,
        }],
      });

      // append value to bucket
      const undoValue = buckets[bucket].join(', ');
      buckets[bucket].push(output[j]);
      const doValue = buckets[bucket].join(', ');
      steps.push({
        do: [{
          action: Action.SET_CELL_VALUE,
          id: 'bucketsTable',
          row: bucket,
          col: 1,
          value: doValue,
        }],
        undo: [{
          action: Action.SET_CELL_VALUE,
          id: 'bucketsTable',
          row: bucket,
          col: 1,
          value: undoValue,
        }],
      });

      // clear highlight for value and bucket
      // also highlight current col in input array again
      steps.push({
        do: [
          {
            action: Action.SET_ROW_COLOR,
            id: 'inputTable',
            row: j,
            color: Color.BASE_COLOR_TABLE_HORIZONTAL,
          },
          {
            action: Action.SET_COLUMN_COLOR,
            id: 'inputTable',
            col: longestNumberLength - i - 1,
            color: Color.TH_CURRENT_COLUMN_SELECTED,
          },
          {
            action: Action.SET_ROW_COLOR,
            id: 'bucketsTable',
            row: bucket,
            color: Color.BASE_COLOR_TABLE_HORIZONTAL,
          },
        ],
        undo: [
          {
            action: Action.SET_ROW_COLOR,
            id: 'inputTable',
            row: j,
            color: Color.TH_CURRENT_ELEMENT,
          },
          {
            action: Action.SET_ROW_COLOR,
            id: 'bucketsTable',
            row: bucket,
            color: Color.TH_CURRENT_COLUMN_SELECTED,
          },
        ],
      });
    }

    // write bucket to outputTable
    output = buckets.flat();
    doActions = [];
    undoActions = [];
    for (let j = 0; j < output.length; j += 1) {
      for (let k = 0; k < longestNumberLength; k += 1) {
        doActions.push({
          action: Action.SET_CELL_VALUE,
          id: 'outputTable',
          row: j,
          col: k,
          value: getNthDigit(output[j], longestNumberLength - k),
        });
        undoActions.push({
          action: Action.SET_CELL_VALUE,
          id: 'outputTable',
          row: j,
          col: k,
          value: null,
        });
      }
    }
    steps.push({
      do: doActions,
      undo: undoActions,
    });

    // copy outputTable to inputTable
    doActions = [];
    undoActions = [];
    for (let j = 0; j < output.length; j += 1) {
      for (let k = 0; k < longestNumberLength; k += 1) {
        doActions.push({
          action: Action.SET_CELL_VALUE,
          id: 'inputTable',
          row: j,
          col: k,
          value: getNthDigit(output[j], longestNumberLength - k),
        });
        undoActions.push({
          action: Action.SET_CELL_VALUE,
          id: 'inputTable',
          row: j,
          col: k,
          value: null,
        });
      }
    }
    steps.push({
      do: doActions,
      undo: undoActions,
    });

    // clear outputTable and bucketsTable
    doActions = [];
    undoActions = [];
    for (let j = 0; j < output.length; j += 1) {
      for (let k = 0; k < longestNumberLength; k += 1) {
        doActions.push({
          action: Action.SET_CELL_VALUE,
          id: 'outputTable',
          row: j,
          col: k,
          value: null,
        });
        undoActions.push({
          action: Action.SET_CELL_VALUE,
          id: 'outputTable',
          row: j,
          col: k,
          value: getNthDigit(output[j], longestNumberLength - k),
        });
      }
    }
    for (let j = 0; j < radix; j += 1) {
      doActions.push({
        action: Action.SET_CELL_VALUE,
        id: 'bucketsTable',
        row: j,
        col: 1,
        value: null,
      });
      undoActions.push({
        action: Action.SET_CELL_VALUE,
        id: 'bucketsTable',
        row: j,
        col: 1,
        value: buckets[j].join(', '),
      });
    }
    steps.push({
      do: doActions,
      undo: undoActions,
    });

    // clear highlight of col i in inputTable
    steps.push({
      do: [{
        action: Action.SET_COLUMN_COLOR,
        id: 'inputTable',
        col: longestNumberLength - i - 1,
        color: Color.BASE_COLOR_TABLE_HORIZONTAL,
      }],
      undo: [{
        action: Action.SET_COLUMN_COLOR,
        id: 'inputTable',
        col: longestNumberLength - i - 1,
        color: Color.TH_CURRENT_COLUMN_SELECTED,
      }],
    });
  }

  return testing ? output : steps;
}
