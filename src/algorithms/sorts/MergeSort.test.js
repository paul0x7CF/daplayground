import MergeSort from './MergeSort';
import getRandomInt from '../../utils/getRandomInt';
import isSorted from '../../utils/isSorted';

describe('MergeSort.js', () => {
  it('should sort the given random input', () => {
    const numTests = 1000;
    const inputSize = 50;
    for (let i = 0; i < numTests; i += 1) {
      const randomNumbers = new Array(inputSize)
        .fill(null)
        .map((_) => getRandomInt(0, 100));
      const sortedOutput = MergeSort(randomNumbers, true);
      expect(isSorted(sortedOutput)).toBe(true);
    }
  });
  test.each([
    // No input
    [[], []],
    // Sorted input
    [[1, 2, 3, 4, 5], [
    ]],
    // Reverse input
    [[5, 4, 3, 2, 1], [
    ]],
    // Only one number
    [[4], []],
    // Repeating numbers
    [[1, 2, 1], [
    ]],
    // Random input
    [[3, 8, 2, 7, 1], [
    ]],
  ])('should return the expected steps, input %#: %p', (input, expectedOutput) => {
    const actualOutput = MergeSort(input);
    // console.log(actualOutput);
    // expect(actualOutput.length).toEqual(expectedOutput.length);
    // expectedOutput.forEach((element, index) => {
    //   expect(actualOutput[index]).toEqual(element);
    // });
  });
});
