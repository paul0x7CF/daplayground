import Action from '../../components/sorts/BarChartActions';
import Color from '../../utils/Color';

/**
 * Sorting Algorithm BogoSort
 *
 * @author Bernhard Frick
 * @param {number[]} elements
 * @param {boolean} testing Set to true to test the algorithm
 * @return {number[] || {do: {}[], undo: {}[]}[]}
 */
export default function BogoSort(elements, testing = false) {
  const steps = [];

  const randomElements = Array.from(elements);

  let sorted = false;
  while (!sorted) {
    // this is BogoSort, you really thought we optimize and check first? :D

    let m = randomElements.length;
    let t;
    let i;

    // Fisher–Yates shuffle (in place):
    // While there remain elements to shuffle...
    while (m) {
      // Pick a remaining element...
      i = Math.floor(Math.random() * m);
      m -= 1;
      // And swap it with the current element
      t = randomElements[m];
      randomElements[m] = randomElements[i];
      randomElements[i] = t;
      steps.push({
        do: [{
          action: Action.EXCHANGE,
          elements: [m, i],
          text: `Exchange elements ${m} and ${i}.`,
        }],
        undo: [{
          action: Action.EXCHANGE,
          elements: [m, i],
          text: `Exchange elements ${m} and ${i}.`,
        }],
      });
    }

    // check if elements are sorted
    sorted = true;
    for (i = 0; i < randomElements.length - 1; i += 1) {
      if (randomElements[i] > randomElements[i + 1]) {
        // not sorted, highlight red
        steps.push({
          do: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BOGO_SORT_COMPARISON_WRONG,
              },
              {
                index: i + 1,
                color: Color.BOGO_SORT_COMPARISON_WRONG,
              },
            ],
            text: '',
          }],
          undo: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BASE_COLOR_BAR_CHART,
              },
              {
                index: i + 1,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: '',
          }],
        });
        steps.push({
          do: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BASE_COLOR_BAR_CHART,
              },
              {
                index: i + 1,
                color: Color.BASE_COLOR_BAR_CHART,
              },
            ],
            text: '',
          }],
          undo: [{
            action: Action.SET_COLORS,
            elements: [
              {
                index: i,
                color: Color.BOGO_SORT_COMPARISON_WRONG,
              },
              {
                index: i + 1,
                color: Color.BOGO_SORT_COMPARISON_WRONG,
              },
            ],
            text: '',
          }],
        });
        sorted = false;
        break;
      }
      // sorted, highlight green
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BOGO_SORT_COMPARISON_TRUE,
            },
            {
              index: i + 1,
              color: Color.BOGO_SORT_COMPARISON_TRUE,
            },
          ],
          text: '',
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BASE_COLOR_BAR_CHART,
            },
            {
              index: i + 1,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: '',
        }],
      });
      steps.push({
        do: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BASE_COLOR_BAR_CHART,
            },
            {
              index: i + 1,
              color: Color.BASE_COLOR_BAR_CHART,
            },
          ],
          text: '',
        }],
        undo: [{
          action: Action.SET_COLORS,
          elements: [
            {
              index: i,
              color: Color.BOGO_SORT_COMPARISON_WRONG,
            },
            {
              index: i + 1,
              color: Color.BOGO_SORT_COMPARISON_WRONG,
            },
          ],
          text: '',
        }],
      });
    }
  }

  // highlight all elements green
  for (let i = 0; i < randomElements.length; i += 1) {
    steps.push({
      do: [{
        action: Action.SET_COLORS,
        elements: [
          {
            index: i,
            color: Color.BOGO_SORT_ELEMENT_SORTED,
          },
        ],
        text: 'highlight green',
      }],
      undo: [{
        action: Action.SET_COLORS,
        elements: [
          {
            index: i,
            color: Color.BASE_COLOR_BAR_CHART,
          },
        ],
        text: '',
      }],
    });
  }
  




  return testing ? randomElements : steps;
}
