import BISEH from '../views/hashes/BISEH.vue';
import DoubleHashing from '../views/hashes/DoubleHashing.vue';
import ExtendibleHashing from '../views/hashes/ExtendibleHashing.vue';
import LinearHashing from '../views/hashes/LinearHashing.vue';
import LinearProbing from '../views/hashes/LinearProbing.vue';
import SeparateChaining from '../views/hashes/SeparateChaining.vue';
import infoMaterialData from '../utils/infoMaterialHashing';
import CoalescedHashing from '../views/hashes/CoalescedHashing.vue';

/**
 * Routes for Hashing
 */
export default [
   {
    path: '/hashing/biseh',
    name: 'BISEH',
    component: BISEH,
     meta: {
       infoMaterial: infoMaterialData.BISEH,
        group: 'Hashing',
     }
    },
  {
    path: '/hashing/coalesced-hashing',
    name: 'Coalesced Hashing',
    component: CoalescedHashing,
    meta: {
      infoMaterial: infoMaterialData.CoalescedHashing,
      group: 'Hashing',
    },
  },
  {
    path: '/hashing/double-hashing',
    name: 'Double Hashing',
    component: DoubleHashing,
    meta: {
      infoMaterial: infoMaterialData.DoubleHashing,
      group: 'Hashing',
    },
  },
   {
     path: '/hashing/extendible-hashing',
     name: 'Extendible Hashing',
    component: ExtendibleHashing,
     meta: {
       infoMaterial: infoMaterialData.ExtendibleHashing,
       group: 'Hashing',
     }
   },
   {
     path: '/hashing/linear-hashing',
    name: 'Linear Hashing',
     component: LinearHashing,
       meta: {
         infoMaterial: infoMaterialData.LinearHashing,
         group: 'Hashing',
       },
   },
  {
    path: '/hashing/linear-probing',
    name: 'Linear Probing',
    component: LinearProbing,
    meta: {
      infoMaterial: infoMaterialData.LinearProbing,
      group: 'Hashing',
    },
  },
  {
    path: '/hashing/separate-chaining',
    name: 'Separate Chaining',
    component: SeparateChaining,
    meta: {
      infoMaterial: infoMaterialData.SeparateChaining,
      group: 'Hashing',
    },
  },
];
