/**
 * Actions supported by BarChart
 */
export default {
  CLEAR: 'clear',
  SET_COLORS: 'setColors',
  SET_LABELS: 'setLabels',
  EXCHANGE: 'exchange',
  SET_POINTERS: 'setPointers',
};
