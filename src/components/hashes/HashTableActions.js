/**
 * Actions supported by HashTableBISEH
 */
export default {
  NOOP: 'noop',
  SET_FILL_FACTOR: 'setFillFactor',
  SET_HASH_CALCULATION: 'setHashCalculation',
  CREATE_LINEAR_LIST: 'createLinearList',
  DELETE_LINEAR_LIST: 'deleteLinearList',
  SET_COLOR: 'setColor',
  SET_VALUE: 'setValue',
  SET_POINTER: 'setPointer',
  UNSET_POINTER: 'unsetPointer',
  MOVE_POINTER: 'movePointer',
  SET_EXTRA_CHECK: 'setExtraCheck',
  UNSET_EXTRA_CHECK: 'unsetExtraCheck',
};
