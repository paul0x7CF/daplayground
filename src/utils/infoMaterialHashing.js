import ColorCodes from './ColorCodes';

const infoMaterialData = {
  DoubleHashing: {
    description: 'R = first prime number <=t able.size-1 g(k)= R- (k mod R) Collided and deleted buckets are marked purple, they need to be included in the collision path Double hashing is a computer programming technique used in hash tables to resolve hash collisions, cases when two different values to be searched for produce the same hash key. It is a popular collision-resolution technique in open-addressed hash tables. Double hashing is implemented in many popular libraries. Like linear probing, it uses one hash value as a starting point and then repeatedly steps forward an interval until the desired value is located, an empty location is reached, or the entire table has been searched; but this interval is decided using a second, independent hash function (hence the name double hashing). Unlike linear probing and quadratic probing, the interval depends on the data, so that even values mapping to the same location have different bucket sequences; this minimizes repeated collisions and the effects of clustering',
    colorCode: ColorCodes.HashTable,
    source: ['https://en.wikipedia.org/wiki/Double_hashing'],

  },
  LinearProbing: {
    description: 'Collided and deleted buckets are marked purple, they need to be included in the collision path Linear probing is a scheme in computer programming for resolving hash collisions of values of hash functions by sequentially searching the hash table for a free location.[20] This is accomplished using two values - one as a starting value and one as an interval between successive values in modular arithmetic. The second value, which is the same for all keys and known as the stepsize, is repeatedly added to the starting value until a free space is found, or the entire table is traversed. (In order to traverse the entire table the stepsize should be relatively prime to the arraysize, which is why the array size is often chosen to be a prime number.)  newLocation = (startingValue + stepSize) % arraySize. This algorithm, which is used in open-addressed hash tables, provides good memory caching (if stepsize is equal to one), through good locality of reference, but also results in clustering, an unfortunately high probability that where there has been one collision there will be more. The performance of linear probing is also more sensitive to input distribution when compared to double hashing, where the stepsize is determined by another hash function applied to the value instead of a fixed stepsize as in linear probing. ',
    colorCode: ColorCodes.HashTable,
    source: ['https://en.wikipedia.org/wiki/Linear_probing'],
  },
  SeparateChaining: {
    description: 'The elements are distributed in the hash table accoring to a modulo (%) operation between an element and the table size. The collision resolution allocates colliding elements into an array of buckets. In case of a dynamic hash table, a treshold fill factor of 70% shall not be exceeded. If however this is the case, the table doubles in size and reallocates the elements.',
    colorCode: ColorCodes.SeparateChaining,
    source: ['https://en.wikipedia.org/wiki/Hash_table#Separate_chaining'],
  },

   CoalescedHashing: {
      description: 'Coalesced Hashing employs modulo operations to hash values into the appropriate row. To mitigate collisions, it may utilize a cellar structure. When a collision arises, the algorithm sequentially searches from bottom to top for the next available slot, where the new value is inserted. A directional arrow is then employed to illustrate the link originating from the initially hashed cell. If a value hashes to a cell already occupied by a value hashed elsewhere, this is represented by a differently colored arrow. During searches, the algorithm initially examines the hashed row and then explores all links. During removal, subsequent links are reorganized as necessary.',
      colorCode: ColorCodes.HashTable,
      source: ['https://en.wikipedia.org/wiki/Hash_table#Coalesced_hashing'],
    },
    LinearHashing: {
      description: 'Linear hashing is a Dynamic hash function it uses a family of hash functions that change as the number of buckets increases or decreases. The hash function returns the index of the bucket that contains the record with a given key. The hash function depends on the level and the split pointer, which are part of the file state.\n' +
        'Splitting occurs when a bucket overflows ',
      colorCode: ColorCodes.LinearHashing,
      source: ['https://en.wikipedia.org/wiki/Linear_hashing'],
    },
    ExtendibleHashing: {
      description: 'Extendible Hashing is a type of dynamic hashing that cleverly grows and adapts to the data it’s handling. Imagine a directory, which is essentially a list of pointers. These pointers lead us to different buckets where our data, or hashed keys, are stored. The directory has a ‘global depth’, which tells us how many bits of the hash function we’re using to distinguish between the buckets.\n' +
        '\n' +
        'Now, each bucket also has a \'local depth’. This tells us how many bits are needed to differentiate the keys within that specific bucket.\n' +
        '\n' +
        'Here’s where Extendible Hashing shows its dynamic nature. When a bucket overflows, meaning it has no more room for new data, one of two things can happen:\n' +
        '\n' +
        '1.) If the local depth of the overflowing bucket is less than the global depth, we simply split the bucket into two. The keys are then rehashed based on the increased local depth. It’s like getting a bigger box when your old one is full.\n' +
        '2.) However, if the local depth equals the global depth, the directory itself needs to expand. It doubles in size, the global depth increases by one, and then the overflowing bucket is split. It’s like adding more drawers to your filing cabinet because one drawer is overflowing.',
      colorCode: ColorCodes.ExtendibleHashing,
      source: ['https://www.geeksforgeeks.org/extendible-hashing-dynamic-approach-to-dbms/'],
    },
    BISEH: {
          instructions: 'To change the indexsize, entrysize or datasize, please change the values in the input fields and press the "set indexsize" button. After this, the button will change to "set entrysize". You need to set all three indexes, in order to create a new hashtable. To insert a number, type it into the input field and press the "Insert" button. When typing more numbers, seperated by space you can insert more numbers.',
          description: 'BISEH (Bounded Index Size Extendible Hashing) uses three hash functions to precisely determine the correct index range, entry and data block, whereby the index range and the entry are set at the beginning and then always remain the same size.\n' +
            '\n' +
            'Insertion: When inserting a new element, BISEH uses its hash functions to determine the appropriate index range and entry. In doing so, it converts the decimal number to be inserted into a binary number.  If the target data block reaches its capacity and an extension is required, BISEH dynamically adjusts the data range to accommodate additional values. The lines of the bucket are doubled in the process. \n' +
            '\n' +
            'Search: When searching for a specific element, BISEH efficiently navigates through the index to find the corresponding entry and data block. The structured approach ensures fast retrieval, even in scenarios with extensive databases.\n' +
            '\n' +
            'Remove: This simply removes the number from the bucket. In this implementation, the number of rows is not halved again, even if it were possible. ',
          colorCode: ColorCodes.SeparateChaining,
          source: ['https://webad.univie.ac.at/webad/htmls/dictionaries/Biseh.html'],
    },
  // Define info material data for other routes
};

export default infoMaterialData;
