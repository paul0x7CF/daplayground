export default {
  INORDER: 'inorder',
  PREORDER: 'preorder',
  POSTORDER: 'postorder',
  UPDATE_TREE: 'update',
  SWAP_NODE: 'swapnodes',
  DUPLICATE_NODE: 'duplicate',
  NOOP: 'noop',
};
