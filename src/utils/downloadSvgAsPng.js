/**
 * Download the given SVG as a PNG file.
 * Compatibility: no idea.
 * @param {string} svgSelector A valid CSS selector string that identifies the SVG element
 * @param {number} width The width of the resulting PNG
 * @param {number} height The height of the resulting PNG
 * @param {string} backgroundColor A named color or hex color value
 * @param {string} fileName The name of the file to download
 */
export default function downloadSvgAsPng(svgSelector, width, height, backgroundColor = 'white', fileName = 'screenshot.png') {
  // get svg as xml string
  const svgString = new XMLSerializer().serializeToString(document.querySelector(svgSelector));
  // convert xml to blob
  const svgBlob = new Blob([svgString], { type: 'image/svg+xml;charset=utf-8' });
  // create url for svg blob
  const url = URL.createObjectURL(svgBlob);

  const img = new Image();
  img.onload = function onload() {
    // create a new canvas
    const canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;

    // draw the image onto the canvas
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);

    // set background color
    ctx.globalCompositeOperation = 'destination-over';
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // convert the canvas to a data url
    const png = canvas.toDataURL('image/png');

    // trigger a download for the canvas data url
    const a = document.createElement('a');
    a.href = png;
    a.download = fileName;
    document.body.appendChild(a);
    a.click();

    // clean up
    document.body.removeChild(a);
    URL.revokeObjectURL(png);
  };

  // trigger onload - mount svg as image
  img.src = url;
}
