/**
 * All actions used for graph algorithms
 */
export default {
  HIGHLIGHT_NODE: 'highlightNode',
  HIGHLIGHT_LINK: 'highlightLink',
  UPDATE_DIJKSTRA_TABLE: 'updateTable',
};
