export default {
  CREATE: 'create',
  INSERT: 'insert',
  REMOVE: 'remove',
  ALGORITHM: 'algorithm',
};
