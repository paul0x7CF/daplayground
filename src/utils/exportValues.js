export default function exportValues(valueWrapper) {
  const json = JSON.stringify(valueWrapper);
  const file = new Blob([json], { type: 'application/json' });
  const a = document.createElement('a');
  a.href = URL.createObjectURL(file);
  a.download = `${valueWrapper.name}.json`;
  a.click();
}
