/**
 * Returns the magnitude of the given number.
 * @param {number} n
 * @param {boolean} returnOrder If true, the order of magnitude is returned.
 * @return {number}
 */
export default function getMagnitude(n, returnOrder = true) {
  const order = Math.floor(Math.log(Math.abs(n)) / Math.LN10 + 0.000000001);
  return returnOrder ? order : 10 ** order;
}
