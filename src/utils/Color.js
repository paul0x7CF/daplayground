/**
 * All colors used for visualizations.
 * Material Design Color Palette taken from https://vuetifyjs.com/en/styles/colors/.
 */
export default {
  // GENERAL COLORS
  PRIMARY : '#379683',

  // GRAPH ALGORITHMS
  BASE_COLOR_NODE: '#3cbcc3', //  blue - used for graphs and binary trees
  GRAPH_SELECTED_NODE: '#FFFFFF', // red
  GRAPH_SELECTED_LINK: '#e40c2b', // red
  GRAPH_HIGHLIGHTED_LINK: '#8EE4AF', // yellow
  GRAPH_RESULT_TRUE: '#438945', // green
  GRAPH_HIGHLIGHTED_NODE: '#e40c2b', // yellow
  GRAPH_LINK_BASE_COLOR: '#000000', // blue


  // TREE STRUCTURES
  BASE_COLOR_TREE: '#3cbcc3', // blue
  TREE_CURRENT_NODE_SELECTED: '#e40c2b', // red
  TREE_CURRENT_NODE_RESULT_TRUE: '#438945', // green
  TREE_CURRENT_NODE_REMOVE: '#eba63f', // yellow


  // SORTING ALGORITHMS
  BASE_COLOR_BAR_CHART: '#3cbcc3', // blue
  BASE_COLOR_TABLE_HORIZONTAL: '#3cbcc3', // blue

  COMPARISON: '#eba63f', // yellow
  COMPARISON_RESULT_TRUE: '#438945', // green
  COMPARISON_RESULT_WRONG: '#F44336', //  red

  // bogo sort
  BOGO_SORT_COMPARISON_TRUE: '#438945', // green
  BOGO_SORT_COMPARISON_WRONG: '#e40c2b', // red
  BOGO_SORT_ELEMENT_SORTED: '#009688', // yellow

  // bubble sort
  BUBBLE_SORT_COMPARISON: '#eba63f', // yellow
  BUBBLE_SORT_COMPARISON_TRUE: '#438945', // green
  BUBBLE_SORT_COMPARISON_WRONG: '#e40c2b', // red
  BUBBLE_SORT_ELEMENT_SORTED: '#009688', // yellow

  // quicksort
  QUICKSORT_PIVOT: '#E91E63', // pink
  QUICKSORT_COMPARISON: '#eba63f', // yellow
  QUICKSORT_SWAP: '#673AB7', // deep purple
  QUICKSORT_ELEMENT_SORTED: '#009688', // green

  // selection sort
  SELECTION_SORT_COMPARISON: '#FF9800', // yellow
  SELECTION_SORT_COMPARE_ELEMENT: '#FFEB3B', // orange
  SELECTION_SORT_SWAP: '#673AB7', // deep purple
  SELECTION_SORT_ELEMENT_SORTED: '#009688', // red

  // table horizontal
  TH_CURRENT_ELEMENT: '#e40c2b', // red
  TH_ELEMENT_SORTED: '#009688', // green
  TH_CURRENT_COLUMN_SELECTED: '#FF7276', // light red'


  // HASH STRUCTURES
  BASE_COLOR_HASH_TABLE: '#3cbcc3', // blue
  BASE_COLOR_HASH_TABLE_CELLAR: '#95DBDF', //light-blue
  HASH_INSERT_SEARCH_COMPLETED: '#438945', // green
  HASH_COLLISION: '#e40c2b', // red
  HASH_COMPARE: '#FFEB3B', // yellow


};
