import getRandomInt from './getRandomInt';
import getMagnitude from './getMagnitude';

describe('getMagnitude.js', () => {
  it('should return the correct magnitude', () => {
    const numTests = 10000;
    for (let i = 0; i < numTests; i += 1) {
      const randInt = getRandomInt(0, 10);
      const input = 10 ** randInt;
      const magnitude = getMagnitude(input);
      expect(magnitude).toEqual(randInt);
    }
  });
  it('should return the correct order', () => {
    const numTests = 10000;
    for (let i = 0; i < numTests; i += 1) {
      const randInt = getRandomInt(0, 10);
      const input = 10 ** randInt;
      const order = getMagnitude(input, true);
      expect(order).toEqual(randInt);
    }
  });
  // todo: test negative numbers
  // todo: test decimal numbers
});
