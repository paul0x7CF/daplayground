export default class ValueWrapper {
  name;

  type;

  values;

  /*
    order/size of trees/hashes
   */
  k;
  /*
  * cellar size in coalesced hashing
  */
  cellar;
  index;
  entry;
  block;
  bucket;

  constructor() {
    this.values = [];
  }

  setType(type) {
    this.type = type;
  }

  setName(name) {
    this.name = name;
  }

  setValues(values) {
    this.values = values;
  }

  setK(k) {
    this.k = k;
  }

  setCellar(cellar) {
    this.cellar = cellar;
  }

  setIndex(index){
    this.index = index;
  }

  setBlock(block){
    this.block=block;
  }

  setEntry(entry){
    this.entry=entry;
  }

  setValuesByMutations(mutations) {
    mutations.forEach((item) => this.values.push(item.value));
  }

  setBucket(bucket) {
    this.bucket = bucket;
  }
}
