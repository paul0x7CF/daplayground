# B+Tree

A B+ tree is an m-ary tree with a variable but often large number of children per node. A B+ tree consists of a root,
internal nodes and leaves. The root may be either a leaf or a node with two or more children.

A B+ tree can be viewed as a B-tree in which each node contains only keys (not key–value pairs), and to which an
additional level is added at the bottom with linked leaves.

The primary value of a B+ tree is in storing data for efficient retrieval in a block-oriented storage context — in
particular, filesystems. This is primarily because unlike binary search trees, B+ trees have very high fanout (number of
pointers to child nodes in a node, typically on the order of 100 or more), which reduces the number of I/O operations
required to find an element in the tree.

The ReiserFS, NSS, XFS, JFS, ReFS, and BFS filesystems all use this type of tree for metadata indexing; BFS also uses B+
trees for storing directories. NTFS uses B+ trees for directory and security-related metadata indexing. EXT4 uses extent
trees (a modified B+ tree data structure) for file extent indexing. Relational database management systems such as IBM
DB2, Informix, Microsoft SQL Server, Oracle 8, Sybase ASE, and SQLite support this type of tree for table indices.
Key–value database management systems such as CouchDB and Tokyo Cabinet support this type of tree for data access.

![B+Tree Schema](https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Bplustree.png/600px-Bplustree.png "B+Tree Schema")

[Wikipedia](https://en.wikipedia.org/wiki/B%2B_tree)
