# BISEH - Bounded Index Size Extendible Hashing

BISEH uses three hash functions to find the right index area, index entry and datablock.
It is characterized by a constant index size and is a new form of [Extendible Hashing](ExtendibleHashing).

If a datablock reaches his limit, then the data area is going to expand.
