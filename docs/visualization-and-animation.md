# Visualization and Animation

Each Page holds an array of StepGroups.

todo: Wrapper for StepGroups array that defines the data structure / algorithm that the StepGroups are about.

## Level 1: StepGroup

Defines a group of steps that change a data structure.

Each StepGroup has...
 * An array of Steps
 * A type (create, insert, search, delete, sort, graph)
 * One or more parameters required for the respective type

Possible parameters:
 * create: size of the ds to create
 * insert: the value to insert
 * sort: the array of values to sort

 **Example**

 ```javascript
const stepGroup = {
    steps: [],
    type: '',
    value: '',
    dataStructure: {},
};
```

## Level 2: Step

Defines one moment in time, broadcast by the controlHandler via the EventBus.

 * Type: Object
 * has a way to define what should happen in this step (actions)
 * could be only one thing, or multiple things

**Example**

```javascript
const step = {
    do: [/* list of actions */],
    undo: [/* list of actions */],
};
```

## Level 3: Actions

Actual changes on the drawing area.

Defined Per visualization type: each viz component can interpret only some of the actions.

Examples of actions that could happen in the same step:
 * set/change color (of bar, cell, node, edge, etc.)
 * set/change value (of bar, cell, node, edge, etc.)
 * exchange values (sorting)
 * create / move / delete pointers (hashing)
 * create / delete symbols (on bars, cells, etc.)

**Example: exchange two values in a bar chart**

```javascript
const action = {
    action: 'exchange',
    elements: [4, 1],
    text: '',
};
```

`action` is one of the strings defined in `src/utils/SortAction.js`.

`elements` is an array of objects that specify the elements the action should be applied to.

The following keys can be set inside an `element`:

```javascript
const element = {
    index: 3,
    color: '#ff0000',
    label: 'Pivot',
};
```

`index` is required to identify the element in the visualization.
`color` is used in combination with the `SET_COLORS` action. It can be any hex color definition as a string but for
consistency only colors from `src/utils/Color.js` should be used.
`label` is a text that is displayed in the center of the specified bar. It is removable by setting `label: ''`.
``

`text` is a string that describes what is currently happening and is displayed in the User Interface.
