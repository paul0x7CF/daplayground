# Offline-Installation

DA Playground is a Progressive Web App (PWA) - that means it can be installed as an application on your
Desktop/Tablet/Smartphone, allowing it to be run like any other installed app.

After installing the app, it can also be used offline without an active internet connection.
