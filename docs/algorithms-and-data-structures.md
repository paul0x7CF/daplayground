# Algorithms and Data Structures

## Graph Algorithms

> Graph theory is the study of graphs, which are mathematical structures used to model pairwise relations between
> objects. A graph in this context is made up of vertices (also called nodes or points) which are connected by edges
> (also called links or lines). A distinction is made between undirected graphs, where edges link two vertices
> symmetrically, and directed graphs, where edges link two vertices asymmetrically. [[Wikipedia](https://en.wikipedia.org/wiki/Graph_theory)]

Learn more about graph algorithms [here](algorithms/graphs/).

 * [Breadth-first search (BFS)](algorithms/graphs/BreadthFirstSearch.md)
 * [Depth-first search (DFS)](algorithms/graphs/DepthFirstSearch.md)
 * [Dijkstra's Algorithm (Shortest Path Problem)](algorithms/graphs/DijkstrasAlgorithm.md)
 * [Kruskal's Algorithm (Minimum Spanning Tree)](algorithms/graphs/KruskalsAlgorithm.md)
 * [Prim's Algorithm (Minimum Spanning Tree)](algorithms/graphs/PrimsAlgorithm.md)
 * [Topological Sort](algorithms/graphs/TopologicalSort.md)

## Sorting algorithms

> A sorting algorithm is an algorithm that puts elements of a list in a certain order. [[Wikipedia](https://en.wikipedia.org/wiki/Sorting_algorithm)]

Learn more about sorting algorithms [here](algorithms/sorts/).

 * [Bogo Sort](algorithms/sorts/BogoSort.md)
 * [Bubble Sort](algorithms/sorts/BubbleSort.md)
 * [Bucket Sort](algorithms/sorts/BucketSort.md)
 * [Counting Sort](algorithms/sorts/CountingSort.md)
 * [Quicksort](algorithms/sorts/QuickSort.md)
 * [Binary Quicksort](algorithms/sorts/BinaryQuickSort.md)
 * [Radix Sort (LSD)](algorithms/sorts/RadixSort.md)
 * [Selection Sort](algorithms/sorts/SelectionSort.md)

## Hash Tables

> A hash table (hash map) is a data structure that implements an associative array abstract data type, a structure that
can map keys to values. A hash table uses a hash function to compute an index, also called a hash code, into an array
of buckets or slots, from which the desired value can be found. [[Wikipedia](https://en.wikipedia.org/wiki/Hash_table)]

Learn more about hash tables [here](algorithms/hashes/).

 * [Double Hashing](algorithms/hashes/DoubleHashing.md)
 * [Extendible Hashing](algorithms/hashes/ExtendibleHashing.md)
 * [Linear Hashing](algorithms/hashes/LinearHashing.md)
 * [Linear Probing](algorithms/hashes/LinearProbing.md)
 * [Separate Chaining](algorithms/hashes/SeparateChaining.md)

## Tree Data Structures

> A tree is a widely used abstract data type (ADT) that simulates a hierarchical tree structure, with a root value and
> subtrees of children with a parent node, represented as a set of linked nodes. [[Wikipedia](https://en.wikipedia.org/wiki/Tree_(data_structure))]

Learn more about trees [here](algorithms/trees/).

 * [Binary Search Tree](algorithms/trees/BinarySearchTree.md)
 * [B+Tree](algorithms/trees/BPlusTree.md)
 * [2-3-4 Tree](algorithms/trees/234Tree.md)
 * [Min Heap](algorithms/trees/Heap.md)
 * [Max Heap](algorithms/trees/Heap.md)
 * [Prefix Tree (Trie, DeLaBriandais-Tree, Patricia-Tree)](algorithms/trees/PrefixTree.md)
