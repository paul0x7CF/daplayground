module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  collectCoverage: true,
  collectCoverageFrom: [
    'src/algorithms/**/*.js',
  ],
  testMatch: [
    '**/tests/unit/**/*.test.js',
    '**/*.test.js',
  ],
  testPathIgnorePatterns: [
    '<rootDir>/src/algorithms/',
    '<rootDir>/node_modules/',
    /* '<rootDir>/src/algorithms/trees', */
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/algorithms/trekhleb',
    '<rootDir>/node_modules/',
  ],
};
