FROM node:14-alpine as builder

COPY . /home/node/app
WORKDIR /home/node/app

RUN yarn install

ENV NODE_ENV production
RUN yarn build

# add static files
RUN cp -r static dist/static

# add docs
RUN yarn build:docs && cp -r docs/.vuepress/dist dist/docs

# add coverage
RUN yarn test:unit && cp -r coverage/lcov-report dist/coverage

FROM nginx:alpine

COPY --from=builder /home/node/app/dist /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
