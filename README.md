<div align="center">

[<img src="http://wwwlab.cs.univie.ac.at/~frickb95/daplayground/static/img/logo.png" width="600">](http://wwwlab.cs.univie.ac.at/~frickb95/daplayground/#/)

<br/>

[![Build Status](https://gitlab.com/daplayground/daplayground/badges/master/pipeline.svg)](https://gitlab.com/daplayground/daplayground/-/commits/master)
[![Coverage Report](https://gitlab.com/daplayground/daplayground/badges/master/coverage.svg)](https://gitlab.com/daplayground/daplayground/-/commits/master)

#### An Algorithm and Data Structures Visualization tool

</div>

DA Playground is a web-based visualization and training platform for algorithms and data structures, developed at the
Faculty of Computer Science at the University of Vienna. The tool is intended to be used as a means of teaching and
training for the Algorithms and Data Structures lecture.

## Contributors

**Core Functionality**

 * Schikuta, Erich (academic lead)
 * Begy, Volodimir (initial development, architecture and data structures)
 * Elashkr, Mohamed (responsive redesign for mobile users)
 * Wegscheider, Lukas (import/export-functionality)
 * Frick, Bernhard (import/export-functionality)

**Data Structures**

 * Hirsch, Bernhard (Merge Sort)
 * Aumüller, Steve (Bucket Sort)
 * Habetinek, Robert (LSD Radix Sort)
 * Tomic, David (Separate Chaining)
 * Fagagnini, Laura (Traversal)
 * Kronfuß, Kathrin (Counting Sort)
 * TarasiewiczMary, Yasmin (Trie)
 * Mangat, Amolkirat Singh (Extendible Hashing)
 * Slapal, Christopher (Binary Quicksort)
 * Wittich, Lukas (Prim's algorithm)
 * Berger, Markus (Topological Sort)
 * Biber, Michael (BISEH)
 * Kunz, Martin (2-3-4 Tree)

## Contributing

To contribute to the project, make sure that you are running an up-to-date version of `node`.

## Getting started & helpful commands

Install all dependencies:
```
yarn install
```

Start a development server: within daplayground folder
```
yarn serve
```

Compile and minify for production:
```
yarn build
```

Lint and fix all files:
```
yarn lint
```

Run all unit tests:
```
yarn test:unit
```

If there is an openssl error (code: ERR_OSSL_EVP_UNSUPPORTED), try to run the following command: (windows)
```
$env:NODE_OPTIONS = "--openssl-legacy-provider" 
```
